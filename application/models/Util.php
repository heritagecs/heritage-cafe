<?php
/**
 * A PHP Class for processing meal plans and their balances for student accounts.
 */


class Util extends CI_Model {

	public function __construct() {
		// Call the parent constructor
		parent::__construct();

		// Load the appropriate models
		$this->load->model('Account');
		$this->load->model('CoffeeshopTab');
    }






    /**
     * A utility that is typically run ahead of the September start up to reset all staff accounts.
     * Ideally, this is accomplished in consultation with the Finance Director to ensure all balances have been captured.
     * 
     * Zero out each account and create a report detailing the zero'd out balance.
     */
    public function zeroStaffAccounts() {
        log_message('debug', 'MODEL: Util | Zero out staff and faculty credit acccounts.');
        $report = '<h1>Report</h1>';

        // Get a list of staff account numbers
        $accounts = $this->Account->getAccountsByType('STAFF', 'ALL');

        // Loop over the list of accounts
        // Call the Zero function, passing it a new memo
        $count = 0;
        foreach ($accounts as $acc) {
            $pId = $acc->person_id;
            // Try to Zero out the account
            $memo = 'Zero out balance of staff account no. ' . $pId;
            log_message('debug', 'MODEL: Util | ' . $count . '. ' . $memo);
            // Go ahead.
            $try = $this->Account->zeroCafeAccount($pId, null, $memo);
            // Log the result
            if ($try) {
                $tId = $try;
                $msg = 'Account ' . $pId . ' balance reset successful.  Reset balance of $' . $acc->mp_balance . ' with transaction # ' . $tId . '.';
                log_message('debug', 'MODEL: Util | ' . $msg);
                $count++;
            }
            if (!$try) log_message('error', 'MODEL: Util | Zero out balance for ' . $pId . ' FAILED.');
        }
        log_message('debug', 'MODEL: Util | SUMMARY: Reset balances for ' . $count . ' staff accounts.');

        return $report;   
    }






    public function zeroCoffeeshopBalancesForAllAccounts() {
        log_message('debug', 'MODEL: Util | Zero out coffee shop balances for all accounts.');
        
        // Create a results object
        $results = new stdClass();
        $results->result = false;
        $results->success = 0;
        $results->fail = 0;
        $results->count = 0;

        // Get a list of coffeeshop accounts
        $accounts = $this->CoffeeshopTab->getAccounts();

        log_message('debug', 'MODEL: Util | Zeroing out ' . sizeof($accounts) . ' Coffeeshop accounts.');

        // Loop over the list of accounts and zero each of them out.
        foreach ($accounts as $acc) {
            // Increment the counter and setup some useful variables.
            $results->count++;
            $fullName = $acc->first . ' ' . $acc->last;
            $pId = $acc->person_id;
            
            // Start of the process
            log_message('debug', 'MODEL: Util | ' . $results->count . '. Zero Coffeeshop balance for ' . $pId);

            // Process the transaction and get the result
            $try = false;
            $try = $this->Account->zeroCoffeeshopBalance($pId);
            if ($try) {
                log_message('debug', 'MODEL: Util | Zero Coffeeshop balance for ' . $pId . ' SUCCESS');
                $results->success++;
            } else {
                log_message('error', 'MODEL: Util | Zero Coffeeshop balance for ' . $pId . ' FAIL');
                $results->fail++;
            }
        }
        // Add a result entry to the log.
        $message = 'In attempting to zero out ' . sizeof($accounts) . ' accounts, we successfully zero\'d out ';
        $message .= $results->success . ' accounts.';
        log_message('debug', 'MODEL: Util | ' . $message);

        // If there were no failures, set the result boolean to true.
        if ($results->fail == 0) $results->result = true;

        // Send back the result object.
        return $results;
    }





















}