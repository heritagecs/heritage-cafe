<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support {
  protected $CI;

    // We'll use a constructor, as you can't directly call a function
  // from a property definition.
  public function __construct()
  {
    // Assign the CodeIgniter super-object
    $this->CI =& get_instance();
  }



  /**
   * Loads a view with the default header and footer.
   */
  public function loadView($view, $data = null) {
    // Get an instance of the Codeigniter object.
    $CI =& get_instance();

    // Load the default header view.
    $this->CI->load->view('parts/header');
    // Load the user's view.
    $this->CI->load->view($view, $data);
    // Load the default footer view.
    $this->CI->load->view('parts/footer');
  }








}
