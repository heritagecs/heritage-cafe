<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {

	/**
	 * [Inventory object constructor]
	 */
	function __construct() {
		parent::__construct();

		// Load up the inventory models.
		$this->load->model('InventoryTable');
		$this->load->model('InventoryCategory');
		$this->load->model('TaxCode');
		$this->load->model('User');
		$this->load->model('Role');

		// Check if the user is logged in.
		$this->User->is_logged_in();
	}


	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
		// Lets have a look at the query string.
		$show_inactive = (boolean)$this->input->get('show_inactive');
		
		// Go get the full inventory data from the model and get the list of categories.
		if (!$show_inactive) {
			$inventory = $this->InventoryTable->getItems(1);
		} else {
			$inventory = $this->InventoryTable->getItems(0);
		}
		$cats = $this->InventoryCategory->getCategories();

		// Build the data array.
		$data = array(
			'inventory' 	=> $inventory,
			'categories'	=> $cats,
		);

		// Finally, load the inventory view.
		$this->load->view('parts/header');
		$this->load->view('nav/manage');
		$this->load->view('manage/inventory', $data);
		$this->load->view('parts/footer');
	}




	public function newCategory() {
		$this->load->view('inventory/new-category');
	}




	public function newItem() {
		// Load up the inventory model.
		$this->load->model('InventoryCategory');
		$taxCodes = $this->TaxCode->getTaxCodes();


		// Get the list of categories.
		$data = array (
			'categories' => $this->InventoryCategory->getCategoryNames(),
			'tax_codes'	=> $taxCodes,
		);

		$this->load->view('inventory/new-item', $data);
	}





	public function import() {
		$this->load->view('inventory/import');
	}





	public function editCategory() {



		$this->load->view('inventory/edit-category');
	}




	public function editItem($id=1) {
		// Get data from the models.
		$item = $this->InventoryTable->getItem($id);
		$cats = $this->InventoryCategory->getCategories();
		$taxCodes = $this->TaxCode->getTaxCodes();

		// Put the item object, categories, and tax codes into a data array.
		$data = array(
			'item'			=> $item,
			'cats'			=> $cats,
			'tax_codes'	=> $taxCodes,
		);

		// Load the view
		$this->load->view('inventory/edit-item', $data);
	}




	/**
	 * Provides a ways to get an item's info externally (i.e. with javascript).
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getItem($id) {
		// Use the InventoryTable model to look the item up in the database.
		$item = $this->InventoryTable->getItem($id);

		// Return a json string containing the item's data.
		echo json_encode($item);
	}

	/**
	 * Provides a ways to get an item's info externally (i.e. with javascript).
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getInventory() {
		// Use the InventoryTable model to look the item up in the database.
		$item = $this->InventoryTable->getItems();

		// Return a json string containing the item's data.
		echo json_encode($item);
	}


	/**
	 * Returns a json string of the inventory in the category specified by the parameter.
	 * @param  [type] $id The id of a category.
	 * @return [type]     A json string of the inventory data.
	 */
	public function getInventoryByCategory($id, $is_active) {
		echo json_encode($this->InventoryTable->getItemsByCategoryId($id, (int)$is_active));
	}


	/**
	 * Return the name of the category.
	 * @param  [type] $id The id of the category in question.
	 * @return [type]     The name/description of the category.
	 */
	public function getCategoryName($id) {
		// Create a variable to hold the name.
		$name = null;

		// Query the categories table for the id of this category and get the first row.
		$query = $this->db->get_where('inventory_category', array('id' => $id));
		$row = $query->row();

		// If the category exists, update the id variable with the id of the category.
		if (isset($row)) {
			$name = $row->category;
		}

		// Return the id of the category.
		echo json_encode($name);
	}
}
