<?php
$is_logged_in = $this->session->is_logged_in;

// Check if the session is set.
if (isset($is_logged_in) || $is_logged_in == true) {
?>


<nav id="hcs-user-navigation" class="hcs-user-navigation hcs-navigation multibuttons" role="navigation">
  <ul class="unstyled-list">
    <?php
    // Check if the user is an admin.  If so, display the admin link.
    if ($this->Role->validateAccess(array('admin'))) { ?>
      <li><a href="<?php echo base_url(); ?>admin/settings"><i class="fas fa-cog"></i></a></li>
    <?php } ?>
		<li><a href="https://heritage.populiweb.com/internal/people/person.php"><i class="fas fa-user"></i><?php echo ucwords($this->session->username); ?></a></li>
    <li><a href="<?php echo base_url(); ?>auth/logout"><i class="fal fa-sign-out"></i></a></li>
  </ul>
</nav>


<?php
}
?>
