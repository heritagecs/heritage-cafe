<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Model for the Users api controller.  Gets users from the database
 */

class InventoryCategory extends CI_Model {

	public function __construct() {

		// Call the parent constructor
		parent::__construct();
	}




	/**
	 * [getCategories Return a list of inventory categories and their id's]
	 * @return [type] [Returns a list of inventory categories and their information]
	 */
	public function getCategories($getActiveCategories = true) {
		// Build the query.
		$this->db->select('id, category, description, storefront');
		if ($getActiveCategories) {
			// If we want only active categories
			$this->db->where('active', 1);
		}
		$this->db->order_by('category', 'ASC');
		$query = $this->db->get('inventory_category');

		$result = $query->result();

		return $result;
	}






	/**
	 * [getCategories Return a list of inventory category names]
	 * @return [type] [description]
	 */
	public function getCategoryNames($getActiveCategories = true) {
		$this->db->select('category');
		if ($getActiveCategories) {
			// If we want only active categories
			$this->db->where('active', 1);
		}
		$this->db->order_by('category', 'ASC');
		$query = $this->db->get('inventory_category');

		$result = $query->result();

		return $result;
	}




	/**
	 * [getCategoryId description]
	 * @param  [type] $name [the name of a category]
	 * @return [type]       [the id of the category]
	 */
	public function getCategoryId($name) {
		// Create a variable to hold the id.
		$id = false;

		// Query the categories table for the name of this category and get the first row.
		$query = $this->db->get_where('inventory_category', array('category' => $name));
		$row = $query->row();

		// If the category exists, update the id variable with the id of the category.
		if (isset($row)) {
			$id = $row->id;
		}

		// Return the id of the category.
		return $id;
	}








	/**
	 * Insert a new category into the database.
	 *
	 */
	public function addNewCategory($category, $description="") {
		// Get the date and time.
		$dateTime = getSqlDateTime();

		// Prepare the data
		$data = array(
			'category' => $category,
			'description' => $description,
			'created_date' => $dateTime,
		);

		// Insert the data into the database.
		$this->db->insert('inventory_category', $data);
	}



}
