<?php

// Load the header
$this->load->view('parts/header');



echo '<h1>Set Storefront</h1>';
echo '<p>Select a storefront to use when loading the Store.  This setting can be changed at any time.</p>';

echo '<div>';
echo anchor('store/set_storefront?sf=CAFETERIA', 'Cafeteria (Lily Ruth)', 'class="btn"');
echo anchor('store/set_storefront?sf=COFFEESHOP', 'The Bean', 'class="btn"');
echo '</div>';






echo '<!-- End of the individual view -->';

$this->load->view('parts/footer'); ?>