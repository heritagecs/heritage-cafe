/**
 * Heritage College and Seminary
 * script.js
 * Supporting JavaScript functions.
 *
 */








/**
 *
 * When the document is loaded, run this stuff.
 *
 */
$( document ).ready( function() {
	console.log( "Document loaded" );

	// Show me the variables
	console.log("BASE_URL: " + BASE_URL);
	console.log("CURRENT_URL: " + CURRENT_URL);


	// Detect and highlight the current page's navigation link.
	highlightNavLinks();


	// Select the username input field.
	$("input[type='text']").on("click", function() {
		$(this).select();
	});


	/*
	 * Event handlers
	 */
	$(".account-details").click( function() {
		if ($(this).next().is(':visible')) {
			$(this).next().hide();
		} else {
			$(this).next().show();
		}
	});

	$("#update-populi-account-balances").click( function(event) {
		deactivateButton('#' + event.target.id);
	})





});







/*
 * A more generic function that returns the last URL segement for the current page.
 */
function currentPage() {
  // Split the current url, limiting it to 4 segments and returning the fourth segment.
  var currentPage = currentURL.split("/",4)[3];

	// If no currentPage, assume we are at the root, make currentPage equal to 'store'.
	if (!currentPage) {
		currentPage = "store";
	}

  // Make a note in the log.
  console.log("Current page: " + currentPage);

  // Return the current page.
  return currentPage;
}





/**
 * Marks navigational links as active to two levels deep.
 */
function highlightNavLinks() {
	// Always find the main nav's selector.
	var selectorSiteNav = '.hcs-navigation li a[href="' + BASE_URL + controller + '"]';
	// Show the log what we found
	//console.log("highlightNavLinks: Main Nav = " + selectorSiteNav);
	// Add the active class to the parent li of the site nav.
	$(selectorSiteNav).parent().addClass('active');

	// If there is a method, define the anchor tag selector using that additional data.
	if (method) {
		var selectorSecondaryNav = '.hcs-navigation li a[href*="/' + controller + '/' + method + '"]';
		// Tell the log.
		//console.log("highlightNavLinks: Secondary Nav = " + selectorSecondaryNav);
		// Add the active class to the parent li of the site nav.
		$(selectorSecondaryNav).parent().addClass('active');
	}
}





/**
 * toggleNextRow, does just what it says.  Toggles the next row based on the row you just
 * clicked.
 * @return {[type]} [description]
 */
function toggleNextRow() {

}






/**
 * Deactivates the button specified.
 * @param  {[type]} selector A text string of the selector for the button you want to
 * deactivate.
 */
function deactivateButton(selector) {
	// Deactive the button.
	$(selector).attr('disabled','disabled').addClass('btn-disabled');
}
