<?php
//include('Support.php');
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Model {



  /**
   * Retrieves the specified role from the data.
   * @return object Returns a role object containing: id, name, description, ts_update, ts_create.
   */
  public function getRole($term) {
    log_message('info', 'Role->getRole() $term = ' . $term);

    // Query the database for a record for this person.
    $this->db->or_where('id', $term);
    $this->db->or_where('name', $term);
    $query = $this->db->get('role');
    $row = $query->row();

    // If the person exists, get the data.
    if (isset($row)) {
      log_message('info', 'The role already exists.');
      $result = $row;
    } else {
      log_message('info', 'TThe role does not exist.');
      $result = false;
    }

    // return
    return $result;
  }


  public function setRole($name, $description = null) {
    log_message('info', 'Role->setRole().');

    // Prepare the data.
    $data = array(
      'name'        => $name,
      'description' => $description,
    );

    // Check if this role already exists.
    $role = $this->getRole($name);
    if ($role) {
      log_message('debug', 'The role already exists, update the record.');
      // Update the user record.
      $this->db->where('id', $role->id);
      $this->db->update('role', $data);
    } else {
      log_message('debug', 'Create role: ' . $name);
      // Add a ts_create variable.
      $data['ts_create'] = getSqlDateTime();
      // Insert the data into the database.
      $this->db->insert('role', $data);
      // Insert it into the database.
    }
  }




  /**
   * Retrieve all the roles from the database.
   * @return array    An array of role objects.
   */
  public function getRoles() {
    log_message('info', 'Role->getRoles()');

    // Generate the results.
    $query = $this->db->get('role');

		// Put the results into an array.
		$result = $query->result();

    // Return the array.
    return $result;
  }





























  /**
   * Checks if the user has the role in question.  Returns true if they do and false if they don't.
   * @param  array or string  $roles        an array containing the list of roles we are trying to validate or a single
   *                                       string containing the role.
   * @param  string  $username    alphanumeric username.  Optional.  Session variable will be used if not supplied with a username.
   * @return boolean              true if the person is granted one of the roles, false otherwise.
   */
  public function validateAccess($roles, $username = null) {
    // If we have not been passed a username, use the session variable by default.
    if (empty($username)) {
      // Check if first to make sure there is something in the username session variable.
      if (isset($this->session->username)) {
        // Get the username from session.
        $username = $this->session->username;
      } else {
        throw new Exception('No one is logged in.');
      }
    }

    // Some messaging
    log_message('info', 'MODEL: Role | Checking if ' . $username . ' has been granted one of these roles: ' . csv($roles));

    // If the $roles list is only a single string and does not contain a comma, turn it into an array.
    if (is_string($roles)) {
      if (!strpos($roles, ',')) {
        log_message('info', 'MODEL: Role | The supplied $roles parameter is a single string, converting to an array.');
        $roles = array($roles);
      } else {
        throw new Exception('$role parameter must only contain lowercase letters.');
      }
    }

    // Get the list of roles for the user is question.
    $acl = $this->User->getAcl($username);

    // Loop over the caller supplied ACL to see if one of these is assigned to the user.
    // If the list of roles contains the role we are looking for return true.
    $valid = false;
    foreach ($roles as $role) {
      if (in_array($role, $acl)) {
        log_message('info', 'MODEL: Role | ' . $username . ' has been granted the ' . $role . ' role.');
        $valid = true;
        break;  // Stop checking the others.
      }
    }

    if (!$valid) {
      log_message('error', 'MODEL: Role | ' . $username . ' has NOT been granted the ' . $role . ' role.');
    }

    return $valid;
  }






}
