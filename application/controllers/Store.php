<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

	public function __construct() {
		// Call the parent constructor
		parent::__construct();
		log_message('debug', 'CONTROLLER: Store | __construct()');

		log_message('debug', 'CONTROLLER: Auth | $_SESSION (id=' . session_id() . '): ' . json_encode($_SESSION));

		// Load up the needed models.
		$this->load->model('InventoryTable');
		$this->load->model('InventoryCategory');
		$this->load->model('User');
		$this->load->model('Role');
		$this->load->model('Transaction');
		$this->load->model('Account');

		// Helpers
		$this->load->helper('cookie');

		// Check if the user is logged in.
		$this->User->is_logged_in();
	}


	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
		log_message('debug', 'CONTROLLER: Store | index()');

		// Making preparations for implementing a decision on which store front to display.
		// Retried the store front cookie.
		$storefront = get_cookie('storefront');
		if (!$storefront || $storefront == '1') redirect('/store/set_storefront');

		$inventory = $this->InventoryTable->getItems();
		$cats = $this->InventoryCategory->getCategories();
		$accounts = $this->Account->getAccounts('first');
		$featuredItems = $this->InventoryTable->getFeaturedItems();

		// Send data appropriate to the storefront.
		if ($storefront == 'CAFETERIA') {
			// Build the data array.
			$data = array(
				'inventory' 	=> $inventory,
				'categories'	=> $cats,
				'accounts'		=> $accounts,
				'featured'		=> $featuredItems,
			);
		} else if ($storefront == 'COFFEESHOP') {
			// Build the data array.
			$data = array(
				'inventory' 	=> $inventory,
				'categories'	=> $cats,
				'accounts'		=> $accounts,
				'featured'		=> $featuredItems,
			);
		}

		// Load the store front.
		$this->load->view('store', $data);
	}




	public function completeTransaction() {
		log_message('debug', 'CONTROLLER: Store | Receiving transaction data from the Store.');

		// Some handy variables
		$discount = 0;
		$paymentType = -1;
		$accountId = -1;

		// Decode the raw input from json in to objects.
		$jsonInput = json_decode($this->input->raw_input_stream);

		// Create an array of items.
		$itemsArray = array();
		$items = json_decode($jsonInput->items);
		foreach ($items as $i) {
			array_push($itemsArray, array('id' => $i->id, 'cost' => $i->price));
		}

		// Set up the transactions variables.
		$subtotal = $jsonInput->subtotal;
		$tax = $jsonInput->tax;
		$total = $jsonInput->total;
		$paymentType = $jsonInput->paymentType;
		$accountId = $jsonInput->account;

		// Get the storefront cookie
		$storefront = $_COOKIE['storefront'];

		// Log info about the transaction.
		$message = 'Items: ' . json_encode($items) . ', Total: ' . $total . ', PaymentType: ' . $paymentType;
		$message = $message . ', AccountId: ' . $accountId . ', Storefront: ' . $storefront;
		log_message('debug', 'CONTROLLER: Store | ' . $message);

		// Push the transaction data to the model.
		$object = (object)array(
			'total'             => $total,
			'payment_type'      => $paymentType,
			'storefront'        => $storefront,
			'transaction_type'  => 0,
			'items'             => $itemsArray,
			'account_id'        => $accountId,
			'tax'               => $tax,
			'discount'          => $discount,
		  );
		$result = $this->Transaction->addTransaction($object);
		if ($paymentType == 1 && $result == true) {
			// If this was on account, update the balance.
			$this->Account->updateBalance($accountId, $total, 0);
		} else if ($storefront == 'COFFEESHOP') {
			// If storefront is the Coffee shop, we will alter the Coffee Shop Tab.
			$this->Account->updateCoffeeShopTab($accountId, $total, 0);
		}
		log_message('debug', 'CONTROLLER: Store | Transaction added to the database.');

		// Echo some data.
		echo 'Transaction posted successfully. ' . json_encode($items) . ', ' . $subtotal . ', ' . $tax . ', ' . $total;
	}



	public function set_storefront() {
		// See if a query string is set.
		$storefront = $this->input->get('sf');

		// We are about to set some cookies, lets make up an expiry
		$years = 1;
		$exp = time() + ($years * 365 * 24 * 60 * 60);

		// Create the two cookies.
		if ($storefront)	{
			set_cookie('storefront', $storefront, $exp);
			redirect ('store');
		}
	
		// If we are not setting a cookie, load the view.
		$this->load->view('store/set-storefront');	
	}

}
