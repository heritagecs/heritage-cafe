<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Model {

  public function __construct() {
    // Call the parent constructor
		parent::__construct();

    // Load up the appropriate models.
    $this->load->model('Account');
  }



  /**
   * Grabs one row for one transaction from the database and returns an object.
   * @param  int $id    A numerical transaction id.
   * @return object     An object containing data about the transaction.
   */
  public function getTransaction($id) {
    log_message('info', 'MODEL: Transaction | getTransaction()');

    // Query the database
    $this->db->where('id', $id);
    $query = $this->db->get('transaction');


    

		// If the first row return by the query has data, return that data.
    ($query->row()) ? $row = $query->row() : $row = false;
		if (!$row) log_message('error', 'MODEL: Transaction | A transaction with ID ' . $id . ' does not exist.');
		return $row;
  }













  /**
   * Responsible for actually pushing the transaction into the database.
   * @param [Object] $trans
   * 
   * $trans object contains the following properties:
   * - total
   * - payment_type
   * - storefront
   * - date (NULL)
   * - transaction_type (0)
   * - items (NULL)
   * - account_id (-1)
   * - tax (0)
   * - discount (0)
   * - memo (NULL)
   * - gl_account (NULL)
   * 
   * $object = (object)array(
   *   'total'             => FLOAT,
   *   'payment_type'      => INT (1 - 5),
   *   'storefront'        => STRING,
   *   'date'              => null,
   *   'transaction_type'  => INT (0 or 1),
   *   'items'             => null,
   *   'account_id'        => $personId,
   *   'tax'               => 0,
   *   'discount'          => 0,
   *   'memo'              => null,
   *   'gl_account'        => null,        
   * );
   */
  public function addTransaction($trans) {
    // Evaluate the properties in the object
    // These properties are required.
    if ($trans->total)          $total = $trans->total;
    if ($trans->payment_type)   $paymentType = $trans->payment_type;
    if ($trans->storefront)     $storefront = $trans->storefront;
    // Default values for these properties.
    (isset($trans->date))              ? $date = $trans->date : $date = NULL;
    (isset($trans->transaction_type))  ? $transactionType = $trans->transaction_type : $transactionType = 0;
    (isset($trans->items))             ? $items = $trans->items : $items = NULL;
    (isset($trans->account_id))        ? $accountId = $trans->account_id : $accountId = -1;
    (isset($trans->tax))               ? $tax = $trans->tax : $tax = 0;
    (isset($trans->discount))          ? $discount = $trans->discount : $discount = 0;
    (isset($trans->memo))              ? $memo = $trans->memo : $memo = NULL;
    (isset($trans->gl_account))        ? $glAccount = $trans->gl_account : $glAccount = NULL;
    
    // Put a note in the logs about what we received.
    log_message('debug', 'MODEL: Transaction | Transaction data received: Total: ' . $total . ', TransactionType: ' . $transactionType . ', PaymentType: ' . $paymentType . ', AccountId: ' . $accountId . ', Storefront: ' . $storefront);

    /* Checks */
    if (!isset($items)) {
      log_message('debug', 'MODEL: Transaction | No items were sold.');
    }

    if ($paymentType == 1) {
      if ($accountId == -1) {
        log_message('error', 'MODEL: Transaction | An account id was not set, this transaction is invalid.');
        return;
      }
    }

    /* Date */
    if (isset($date)) {
      $transactionDate = getSqlDateTime($date);
    } else {
      $transactionDate = getSqlDateTime();
    }
    
    /* Calculate the subtotal */
    $subtotal = 0;
    if (isset($items)) {
      foreach ($items as $item) {
        $subtotal += $item['cost'];
      }
    } else {
      $subtotal = $total;
    }

    // Build an array of data.
    $data = array(
      'date'              => $transactionDate,
      'storefront'        => $storefront,
      'transaction_type'  => $transactionType,
      'subtotal'          => $subtotal,
      'tax'               => $tax,
      'discount'          => $discount,
      'total'             => $total,
      'payment_type'      => $paymentType,
      'account_id'        => $accountId,
      'memo'              => $memo,
      'gl_account'        => $glAccount,
      'ts_create'         => getSqlDateTime(),
    );

    // Query the database and insert a record.
    log_message('debug', 'MODEL: Transaction | Inserting the transaction...');
    $this->db->insert('transaction', $data);

    // Verify the transaction was created.
    if ($this->verifyTransaction($transactionDate, $total, $paymentType)) {
      // Create the records of items sold using the new transaction id.
      $transId = $this->getTransactionId($transactionDate, $total, $paymentType);
      log_message('debug', 'MODEL: Transaction | Transaction #' . $transId . ' was created successfully.');
      $result = $transId;
    } else {
      log_message('error', 'MODEL: Transaction | The transaction was not created: ' . $transactionDate . ', total:' . $total . ', type:' . $paymentType);
      $result = false;
    }

    // Go home if the transaction wasen't created successfully.
    if (!$result) return $result;

    // Each of the items that were purchased if items are specified.
    if (isset($items)) {
      foreach ($items as $item) {
        $data = array(
          'transaction_id'  => $transId,
          'inventory_id'    => $item['id'],
          'price'           => ($transactionType == 0) ? $item['cost'] : $item['cost'] * -1,
          'date'            => $transactionDate,
        );
        log_message('debug', 'MODEL: Transaction | Inserting the items for transaction #' . $transId);
        $this->db->insert('items_sold', $data);
      }
    }

    // Return true or false depending on if the transactions was created.
    return $result;
  }



  /**
   * Verifies that the transaction specified by the parameters exists.
   * @param  [type] $date        [description]
   * @param  [type] $total       [description]
   * @param  [type] $paymentType [description]
   * @return [type]              [description]
   */
  public function verifyTransaction($date, $total, $paymentType) {
    // Variables
    $verified = false;

    // Build and query the database.
    $this->db->where('date', $date);
    $this->db->where('total', $total);
    $this->db->where('payment_type', $paymentType);
    $query = $this->db->get('transaction');

    // Get the first row.
    $result = $query->row();

    // See if there is data in the first row.
    if (isset($result)) {
      $verified = true;
    }

    // Return the result.
    log_message('debug', 'MODEL: Transaction | Transaction #' . $result->id . ' verified.');
    return $verified;
  }









  /**
   * Looks up in the transaction table and returns the id for the transaction that matches
   * these parameters exactly.
   * @param  [type] $date        [description]
   * @param  [type] $total       [description]
   * @param  [type] $paymentType [description]
   * @return [type]              [description]
   */
  public function getTransactionId($date, $total, $paymentType) {
    // Variables
    $id = -1;

    // Build and query the database.
    $this->db->where('date', $date);
    $this->db->where('total', $total);
    $this->db->where('payment_type', $paymentType);
    $query = $this->db->get('transaction', 'id');

    // Get the first row.
    $result = $query->row();

    // See if there is data in the first row.
    if (isset($result)) {
      $id = $result->id;
    }

    // Return the id.
    return $id;
  }














  /**
   * Determines the items sold in a particular transaction.
   * @param  [type] $transId The id of the transaction in question.
   * @return [type]          An array of item objects.
   */
  public function getItemsSoldInTransaction($transId) {
		// The list of fields to query.
		$fields = 'items_sold.id, items_sold.inventory_id, items_sold.price, inventory.description';

		// Query the database for all active inventory items.
		// Put the query into an array.
		$this->db->select($fields);
		$this->db->from('items_sold');
    $this->db->where('items_sold.transaction_id', $transId);
		$this->db->join('inventory', 'items_sold.inventory_id = inventory.id');
		$this->db->order_by('items_sold.id', 'ASC');
		$query = $this->db->get();

		// If there is data, return it otherwise return false.
		return ($query->result()) ? $query->result() : false;
  }




















  /**
   * Voiding a transaction will reverse the effects of the transaction but leave
   * the original transaction's data intact.
   * @param  int $transId     A numerical transaction id.
   * @return boolean          True if the void was successful, false if there was a failure.
   */
  public function voidTransaction($transId) {
    log_message('debug', 'MODEL: Transaction | voidTransaction()');

    // Make sure the transaction id is an int.
    $transId = (int)$transId;

    // Go get the original transaction data from the database and the items sold.
    $trans = $this->Transaction->getTransaction($transId);
    // Add one second to the original transaction's date.
    $newDate = date('Y-m-d H:i:s', strtotime('+1 second', strtotime($trans->date)));

    // Check to see if this account is already voided.
    if ($trans->is_voided) {
      log_message('debug', 'MODEL: Transaction | Tranaction ' . $trans->id . ' is already voided.  ABORT!');
      return false;
    }

    // Empty variable to hold the transaction id that will be generated for the voiding transaction.
    $voidingTransId = null;

    /*
      When we reverse a transaction, we need to know a few things, like.
       - what was the transaction type (0 or 1; debit or credit)?
       - what was the payment type?
     */
    if ($trans->transaction_type == 0) {
      /* This transaction was a debit, so we need to do a credit. */

      // Since this was a debit transaction type, get the items sold.
      log_message('debug', 'MODEL: Transaction | Transaction ' . $transId . ' is a debit transation, getting the items sold.');
      $itemsSold = $this->Transaction->getItemsSoldInTransaction($transId);

      /* Decisions based on the payment type */
      switch ($trans->payment_type) {
        case (1):
          // The payment type was 1 (on account).

          // Fix the items sold.
          $itemsArray = array();
          foreach ($itemsSold as $i) {
            array_push($itemsArray, array('id' => $i->inventory_id, 'cost' => $i->price));
          }

          // Important data for the transaction.
          $tot = $trans->total;
          $pType = $trans->payment_type;
          $date = $newDate;
          $tType = 1;
          $items = $itemsArray;
          $account = $trans->account_id;
          $tax = $trans->tax;
          $memo = 'VOID #' . $transId;

          // Create the new credit transaction.
          $object = (object)array(
            'total'             => $tot,
            'payment_type'      => $pType,
            'storefront'        => $_COOKIE['storefront'],
            'date'              => $date,
            'transaction_type'  => $tType,
            'items'             => $items,
            'account_id'        => $account,
            'tax'               => $tax,
            'discount'          => 0,
            'memo'              => $memo,
          );
          $voidingTransId = $this->Transaction->addTransaction($object);

          // Update the balance.
          $this->Account->updateBalance($account, $tot, 1);

          break;
        default:
          log_message('debug', 'MODEL: Transaction | We cannot yet reverse payment types of ' . $trans->payment_type . '.');
      } // End of switch ($trans->payment_type)

    } else {
      /* This transaction was a credit, so we need to do a debit. */
      log_message('debug', 'MODEL: Transaction | Transaction ' . $transId . ' is a credit transation, getting the items sold.');
      $itemsSold = $this->Transaction->getItemsSoldInTransaction($transId);
      if (!$itemsSold) $itemsSold = null;

      // Create the new debit transaction object.
      $object = (object)array(
        'total'             => $trans->total,
        'payment_type'      => $trans->payment_type,
        'storefront'        => $_COOKIE['storefront'],
        'date'              => $newDate,
        'transaction_type'  => 0,
        'items'             => $itemsSold,
        'account_id'        => $trans->account_id,
        'tax'               => $trans->tax,
        'discount'          => 0,
        'memo'              => 'VOID # ' . $transId,         
      );
      $voidingTransId = $this->Transaction->addTransaction($object);

      // Update the balance
      $this->Account->updateBalance($trans->account_id, $trans->total, 0);
    }


    /*
      Update the original transaction to mark it as voided.
     */
    $isVoided = $this->Transaction->setIsVoided($transId, $voidingTransId);

    // Check that the voiding transaction was created successfully and the references are set.
    if ($this->Transaction->getTransaction($voidingTransId) && $isVoided) {
      log_message('debug', 'MODEL: Transaction | Transaction ' . $transId . ' was successfully voided by transaction ' . $voidingTransId);
      return $voidingTransId;
    } else {
      log_message('debug', 'MODEL: Transaction | Oops, something bad happened and voiding ' . $transId . ' failed.');
      return false;
    }


  }















  /**
   * A function that marks a transaction voided and notes the relationship between it and the
   * transaction that voided it.
   * @param int $transId          A numerical transaction id.
   * @param int $voidingTransId   Another numerical transaction id for which the first is voided as a result.
   */
  public function setIsVoided($transId, $voidingTransId) {
    log_message('debug', 'MODEL: Transaction | setIsVoided()');

    // Make sure both numbers are integers.
    $transId = (int)$transId;
    $voidingTransId = (int)$voidingTransId;

    /*
      Update the voided transaction; set is_voided = 1 and mark the voiding transaction id.
     */
    $data = array(
      'is_voided'               => 1,
      'voiding_transaction_id'  => $voidingTransId,
    );
    $this->db->where('id', $transId);
    $this->db->update('transaction', $data);

    /*
      Update the voiding transaction so it refers to the transaction it is voiding.
     */
    $this->db->where('id', $voidingTransId);
    $this->db->update('transaction', array('voided_transaction_id' => $transId));

    // Verify that we voided it on record.
    $isVoided = $this->Transaction->getTransaction($transId);
    if ($isVoided->is_voided && $isVoided->voiding_transaction_id == $voidingTransId) {
      return (int)$isVoided->is_voided;
    } else {
      return false;
    }

  }






}
