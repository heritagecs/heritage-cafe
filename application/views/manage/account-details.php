<?php $this->load->view('nav/manage'); ?>

<!-- Start of the individual view -->

<div id="" class="site-content-inner">

<?php
  $fullName = $account->first . ' ' . $account->last;
  log_message('debug', 'VIEW: account-details | Showing account details for ' . $fullName . ' (' . $account->person_id . ').');

  // Get the friendly display name for the storefront and also set which
  // stored balanced we are using for this view based on that Storefront.
  switch (STOREFRONT) {
    case 'CAFETERIA':
      $storefrontDisplay = 'Cafeteria Account';
      $balanceStored = $account->mp_balance;
      break;
    case 'COFFEESHOP':
      $storefrontDisplay = 'The Bean Tab';
      $balanceStored = $account->coffeeshop_balance;
      break;
  }
  // Display the person's name and their person_id/account number.
  echo '<h1>' . $fullName . ' | ' . $storefrontDisplay . '</h1>';

  // Breadcrumbs
  echo '<p>Back to <a href="' . base_url('manage/accounts') . '">Accounts</a></p>';

  // Basic balance info.
  echo '<h6>Basic Info</h6>';
  echo '<ul class="unstyled-list">';
  echo '<li>' . $storefrontDisplay . ' Balance: <span class="price text-small">$</span>';
  echo '<span class="price text-large">' . number_format($balanceCalculated, 2) . '</span></li>';
  echo '<li>Stored Balance: <span class="price">$' . number_format($balanceStored, 2) . '</span></li>';
  $url = 'https://heritage.populiweb.com/router/contacts/people/' . $account->person_id . '/financial';
  echo '<li>Account Number: <a target="_blank" href="' . $url . '">' . $account->person_id . '</a></li>';
  echo '</ul>';

  // If the balance and the calculated balance are different generate a button that will allow the
  // manager to update the balance to properly reflect the accurate calculated balance.
  $balanceStoredRounded = round(floatval($balanceStored), 2);
  $balanceCalculatedRounded = round(floatval($balanceCalculated), 2);

  if ($balanceStoredRounded != $balanceCalculatedRounded) {
    // I used abs() to verify the difference between the two float values.
    log_message('debug' , $balanceStored . ' | ' . $balanceCalculated);
    echo '<p><strong>WARNING</strong>, there is a discrepancy between the balance as stored with the Account and the balance as attained by calculating the debits and credits for this account throughout the school year.  Correct the balance manually by doing a ';
    echo anchor('Manage/oneTimeBalanceUpdate/' . $account->person_id . '/' . $balanceCalculated, 'one time balance update');
    echo '.</p>';
  }


  // Detailed meal plan info.
  echo '<h6>Account Details</h6>';
  
  /* Feature to allow the user to check Populi for meal plan data for this account */
  $uri = 'manage/updateAccount/' . $account->person_id . '?action=get-populi-remote-meal-plan';
  echo anchor($uri, 'Sync with myHeritage', 'class="btn btn-small"');

  /* Create a button that will allow the user to quickly update this students
  remote account balance in Populi. */
  if (STOREFRONT === 'CAFETERIA') {
    $uri = 'manage/updateAccount/' . $account->person_id . '?action=update-populi-remote-balance';
    echo anchor($uri, 'Push updated account balance to Populi', 'class="btn btn-small"');
  } else if (STOREFRONT === 'COFFEESHOP') {
    $uri = 'manage/updateAccount/' . $account->person_id . '?action=update-coffeeshop-tab-remote-balance';
    echo anchor($uri, 'Push The Bean tab balance to Populi', 'class="btn btn-small"');
  }



  echo '<ul class="unstyled-list">';
  if (STOREFRONT === 'CAFETERIA') {
    if ($account->type == 'MEAL_PLAN') {
      echo '<li>' . $account->first . ' is using a <strong>Heritage Meal Plan</strong> account</li><br />';
      echo '<li><strong>' . $account->mp_name;
      if ($account->mp_starting_balance != null) {
        echo ' (<span class="price">$' . number_format($account->mp_starting_balance, 2) . '</span>)</strong>';
      }
      echo '</li>';
    } else if ($account->type == 'PRE_PAID') {
      echo '<li>' . $account->first . ' is using a <strong>Lily Ruth Pre-Paid</strong> account</li>';
    } else if ($account->type == 'STAFF') {
      echo '<li>' . $account->first . ' is using a <strong>Staff & Faculty</strong> account</li>';
    } else {
      echo '<li>' . $account->first . ' is using a <strong>Credit Account</strong></li>';
    }
    echo '<li>Account Balance <span class="text-small">(visible on the Student tab of myProfile in myHeritage)</span>: <span class="price">$' . number_format($account->mp_balance_populi, 2) . '</span></li>'; 
  } else if (STOREFRONT === 'COFFEESHOP') {
    echo '<li>The Bean Tab <span class="text-small">(visible on the Financial tab of myProfile in myHeritage)</span>: <span class="price">$' . number_format($account->coffeeshop_balance_populi, 2) . '</span></li>'; 
  }
  echo '</ul>';


  /*
    Account transaction actions.
   */
  echo '<aside>';
  echo '<h6 id="account-actions">Account Actions</h6>';
  echo '<ul class="unstyled-list">';
  // Add a Transaction.
  echo '<li>';
  echo '<a href="javascript:void(0);" onclick="toggleFormAddTransaction();" class="btn-add-transaction">Add Transaction</a>';
  echo '</li>';
  // Transfer a balance
  if (STOREFRONT == 'CAFETERIA') {
    echo '<li>';
    echo '<a href="javascript:void(0);" onclick="toggleFormTransferBalance();" class="btn-transfer-balance">Transfer Balance</a>';
    echo '</li>';
    echo '</ul>';
  }
  echo '</aside>';


  echo '<div class="account-action-forms">';
  $this->load->view('manage/account-add-transaction');
  $this->load->view('manage/account-transfer-balance');
  echo '</div>';

  echo '</ul>';

  /* Show a table of transactions, categorized by semester. */

  // Generate the heading.
  $this->table->set_template(array('table_open' => '<table id="account-details-transaction-history" class="tbl-accordion account-details-transaction-history"'));
  $this->table->set_heading(array('ID', 'Date', 'Memo', 'Debit', 'Credit', 'Balance'));
  $data = array();

  foreach ($accountHistory as $t) {
    // Format the transaction id/link.
    ($t->is_voided) ? $class = array('class' => 'text-strikethrough') : $class = '';
    $tUrl = anchor('manage/transaction/' . $t->id, $t->id, $class);

    // Figure out if this is a debit or credit transaction.
    if ($t->transaction_type == 0) {
      $amount = number_format($t->total, 2);
      $credit = NULL;
    } else {
      $amount = NULL;
      $credit = number_format($t->total, 2);
    }

    // Format the balance.
    $balanceCalculated = number_format($t->account_balance, 2);

    log_message('info', 'VIEW: account-details | Displaying transaction #' . $t->id);

    $date = date('M jS Y', strtotime(sanitizeDate($t->date)));
    $memo = $t->memo;

    // Add this row to the array of data.
    array_push($data, array($tUrl, $date, $memo, $amount, $credit, $balanceCalculated));
  }


  // Display the table.
  echo $this->table->generate(array_reverse($data));


?>

</div>

<!-- Load the manage.js script -->
<script src="<?php echo base_url(); ?>assets/js/manage.js"></script>
