<?php
/**
 * A PHP Class for negotiating sessions and data with the Populi database.
 *
 * Specifically, this a local implementation of some of the Populi Developer API methods from their
 * API Reference (https://support.populiweb.com/hc/en-us/articles/223798747-API-Reference)
 *
 * Most of these local methods interact wtih the PopuliConnect class to get data out of the database
 * and return it to the caller in JSON format or an array of JSON objects.
 */


class PopuliApi extends CI_Model {

	public function __construct() {
		// Call the parent constructor
		parent::__construct();

    // Load up the appropriate models.
    $this->load->model('populi/PopuliConnect');

	}











	/**
	 * Queries Populi for a list of academic terms and returns the result as a JSON object.
	 * @return object  JSON object containing a list of academic terms.
	 */
	public function getAcademicTerms() {
		$data = $this->PopuliConnect->doTask('getAcademicTerms');
		$data = json_decode($data)->academic_term;
		return $data;
	}









	public function getAcademicYears() {
		$data = $this->PopuliConnect->doTask('getAcademicYears');
		$data = json_decode($data)->academic_year;
		return $data;
	}





	/**
	 * Implements the API call for the getAllCustomFields which returns all custom fields our college has defined.
	 * @return array 				An array of custom field objects.
	 */
	public function getAllCustomFields() {
		log_message('info', 'MODEL: PopuliApi | getAllCustomFields()');

		// Make the call.
		$raw = $this->PopuliConnect->doTask('getAllCustomFields');

		// Return the data.
		return json_decode($raw)->custom_field;
	}






	public function getCurrentAcademicTerm() {
		$data = $this->PopuliConnect->doTask('getCurrentAcademicTerm');
		$data = json_decode($data);
		return $data;
	}







	/**
	 * Implement the getCustomFields API call.  Must collect a person id from the call.  The type is optional.
	 * @param  int $personId 		A numerical student id.
	 * @param  string $type     The name of the type of custom field (all (default), application, faculty, person, prospect, student)
	 * @return array           	An array of custom field objects or false if failure.
	 */
	public function getCustomFields($personId, $type = NULL) {
		log_message('info', 'MODEL: PopuliApi | getCustomFields(' . $personId . '[, ' . $type . '])');

		$customFields = false;

		// Ensure the person id is used to call teh doTask function.
		$args['person_id'] = $personId;

		// If a type was specified, add it to the arguments.
		if ($type) $args['type'] = $type;

		// Tell the log what we are passing.
		log_message('info', 'MODEL: PopuliApi | Passing arguments to doTask->getCustomFields: ' . json_encode($args));

		// Go get the data from Populi.
		try {
			// Collect data from Populi and convert the returned data into a json object.
			$data = $this->PopuliConnect->doTask('getCustomFields', $args);
			$dataJson = json_decode($data);
		} catch (Exception $e) {
			// Throw a new exception back to the caller with more pertinent data and return false.
			throw new PopuliException('MODEL: PopuliApi | Exception: ' . $e->getMessage());
			return false;
		}

		// Check that the custom field property exists, if not return false.
		if (!property_exists($dataJson, 'custom_field')) {
			log_message('error', 'MODEL: PopuliApi | No custom fields were returned for person ' . $personId . '.');
			return false;
		}

		// Determine if only one record was returned or not.
		$singleResult = strpos($data, 'custom_field":{"');
		// If the result was a single custom field.
		if ($singleResult) {
			log_message('info', 'MODEL: PopuliApi | There was only one custom field for person ' . $personId);
			// Create an empty array.
			$customFields = array();
			// Add the single custom field to the array.
			array_push($customFields, $dataJson->custom_field);
		} else {
			log_message('info', 'MODEL: PopuliApi | Person ' . $personId . ' has multiple of custom fields.');
			$customFields = $dataJson->custom_field;
		}

		// Return the array of one or more custom field objects.
		return $customFields;
	}

	/**
	 * An implementation of the setCustomField api call.
	 * @param int $personId     			A numerical person id.
	 * @param int $custom_field_id		A numerical custom field id.
	 * @param float $value        		A decimal price.
	 * @return boolean								Returns a value of true if successful.
	 */
	public function setCustomField($person_id, $custom_field_id, $value) {
		log_message('info', 'MODEL: PopuliApi | setCustomField()');

		// Setup the arguments we will pass to Populi.
		$args = array(
			'person_id'				=> $person_id,
			'custom_field_id'	=> $custom_field_id,
			'value'						=> $value,
		);

		// Punch the query.
		$response = $this->PopuliConnect->doTask('setCustomField', $args);

		// Evaluate the response.
		if (json_decode($response)->result == 'SUCCESS') {
			return true;
		} else {
			log_message('error', 'MODEL: PopuliApi | We failed to set the custom field with id ' . $custom_field_id);
			return false;
		}
	}









	/**
	 * Returns information about each degrees offered by the school.
	 * @return [array] An array of degree objects.
	 */
	public function getDegrees() {
		log_message('debug', 'MODEL: PopuliApi | getDegrees()');

		$data = $this->PopuliConnect->doTask('getDegrees');

		return json_decode($data)->degree;
	}











	/**
	 * Gathers all information related to meal plans configured for the institution.
	 * @return array 					An array of meal plan objects.
	 */
	public function getMealPlans() {
		log_message('info', 'MODEL: PopuliApi | getMealPlans()');

		// Get the data from Populi.
		$raw = $this->PopuliConnect->doTask('getMealPlans');

		// Return it as an array of json objects.
		return json_decode($raw)->meal_plan;
	}











	public function getPendingCharges($term_id = null, $type = null) {
		log_message('info', 'MODEL: PopuliApi | getPendingCharges()');

		// Create an array for the arguments to be passed to Populi.
		$args = array(
			'term_id'			=> $term_id,
			'type'				=> $type,
		);

		// Get all the pending charges based on the arguments.
		$raw = $this->PopuliConnect->doTask('getPendingCharges', $args);

		// Convert the data into a couple formats
		$obj = json_decode($raw);
		$array = (array)(json_decode($raw));

		// If the result was a single record.
		if ($array['@attributes']->num_results == 1) {
			log_message('debug', 'MODEL: PopuliApi | Only one record was returned.');
			// Create an empty array.
			$pendingCharges = array();
			// Add the single custom field to the array.
			array_push($pendingCharges, $obj->pending_charge);
		} else {
			log_message('debug', 'MODEL: PopuliApi | Multiple records were returned.');
			$pendingCharges = $obj->pending_charge;
		}

		// Return the array.
		return $pendingCharges;
	}














	/**
	 * Returns basic profile data about a person.
	 * @param  [type] $id The numberic ID of the person in question.
	 * @return [type]     An object containing name, age, gender, tags, and
	 * contact information (address, phone, email).
	 */
	public function getPerson($id) {
		log_message('info', 'MODEL: PopuliApi | getPerson(): $id = ' . $id);

		// Check that the ID is numberic.
		if (!is_int($id)) {
			log_message('error', 'MODEL: PopuliApi | getPerson(). The parameter, person_id, is not an integer.');
			return;
		}

		// Get data on this person from Populi.
		$person = $this->PopuliConnect->doTask('getPerson', array('person_id' => $id));

		// Format the data as a json object.
		return json_decode($person);
	}








	/**
	 * Returns member of a particular role
	 * @param  [int] $roleId   		Numeric ID of the role.  Must specify one of either $roleId or $roleName.
	 * @param  [string] $roleName Name of the role.
	 * @param  [string] $status   Possible values: ACTIVE (default), INACTIVE, or ALL.
	 * @param  [int] $page				If you're using a limit, which page of results would you like.
	 * @param  [boolean]			By default, only ships back one page of results, say true to return all results.
	 * @return [array]           An array of person objects (including personId, first, last, inactive)
	 */
	public function getRoleMembers($roleName = null, $roleId = null, $status = null, $page = null, $combinedResults = false) {
		log_message('info', 'MODEL: PopuliApi | getRoleMembers()');

		// Placeholder.
		$roleMembers = false;

		/*
     * Validate the input.
		 */
		if (!$roleId && !$roleName) {
			log_message('error', 'MODEL: PopuliApi | Please specify either a roleId or a roleName.');
		}

		// Setup the array that will be used to interact with Populi.
		$args = array();
		if ($roleId) 		$args['roleID'] = $roleId;
		if ($roleName) 		$args['roleName'] = $roleName;
		if ($status) 		$args['status'] = $status;
		if ($page) 			$args['page'] = $page;
		if ($page === null)	$args['page'] = 1;

		// Tell the log what we are about to do.
		log_message('info', 'MODEL: PopuliApi | Asking Populi for information about a role: ' . implode(',', $args));

		// Go get the data from Populi.
		$data = $this->PopuliConnect->doTask('getRoleMembers', $args);
		// Encode the whole thing into a json object.
		$json = json_decode($data);
		// Turn the json object into an array and access the @attributes element.
		$array = (array)$json;
		$numResults = $array['@attributes']->num_results;

		// If there were results.
		if (property_exists($json, 'person')) $roleMembers = $json->person;

		// If the number of results is over 1000, we need to ask additional pages.
		if ($numResults > 1000 && $combinedResults == true) {
			// Figure out the number of pages we need to get.
			$noPages = ceil($numResults / 1000);
			log_message('debug', 'MODEL: PopuliApi | Page 1 of ' . $noPages . ' returned more than 1000 results.');

			// For loop for each page in the number of results getting the results for each page.
			for ($i = 2; $i <= $noPages; $i++) {
				// Update the page value that we are asking for.
				$args['page'] = $i;
				// Go get the data from Populi, encode it into objects then make an array.
				$data = (array)json_decode($this->PopuliConnect->doTask('getRoleMembers', $args));
				// Log a message.
				log_message('debug', 'MODEL: PopuliApi | Page ' . $i . ' of ' . $noPages . ' returned ' . sizeof($data['person']) . ' more.');
				// Add the new data to the existing array.
				$roleMembers = array_merge($roleMembers, $data['person']);
			}
		}

		// Go through the list and replace the name of personID with person_id
		foreach ($roleMembers as $row) {
			$row->person_id = $row->personID;
			unset($row->personID);
		}

		$message = sizeof($roleMembers) . ' results from a set of ' . $numResults . ' total for the role ' . strtoupper($roleName);
		log_message('info', 'MODEL: PopuliApi | getRoleMembers returning ' . $message);

		// Return only the person object (which contains the array of people objects).
		return $roleMembers;
	}










	/**
	 * Get basic student information from the Populi database including name, birthdate, student_id, etc.
	 * @param  [type] $id The numberic ID of the person in question.
	 * @return [type]     [description]
	 */
	public function getStudentInfo($id) {
		log_message('info', 'PopuliApi->getStudentInfo()');

		// Check that the ID is numberic.
		if (!is_int($id)) {
			log_message('error', 'PopuliApi->getStudentInfo(). The parameter, person_id, is not an integer.');
			return;
		}

		// Get data on this person from Populi.
		$person = $this->PopuliConnect->doTask('getStudentInfo', array('person_id' => $id));

		// Format the data as a json object.
		return json_decode($person);
	}













  /**
   * Generates a list of programs the student is, or has been, enrolled in.
   * @param  [type] $personId The numberic person id.
   * @return [type]           An array of json objects containing $programId, $name, and $degrees.
   *                             $degrees is an array of degree objects.
   */
  public function getStudentPrograms($personId) {
		log_message('info', 'MODEL: PopuliApi | getStudentPrograms()');
    // Get the programs for this person.
    $raw = $this->PopuliConnect->doTask('getStudentPrograms', array('person_id' => $personId));

    // Encode it as a json object.
		$raw = json_decode($raw);

		if (isset($raw->programs->program)) {
			// If programs are set, then they are in a College or Seminary program.
			$rawArray = $raw->programs->program;
		} else {
			// If the program is not set, they are just taking individual courses.
			log_message('error', 'MODEL: PopuliApi | Person ' . $personId . ' is not in a program.');
			return false;
		}

		/*
	 If the data is not an array, then there is only one element, so, make it an
	 array to match what happens when there are multiple programs.  We can now
	 iterate over the $rawArray with a foreach.
		 */
		if (!is_array($rawArray)) $rawArray = array($rawArray);
		/* Data is now prepared from Populi in an array regardless of the number of programs. */


		/*
		Now for each element of the raw array aquired from Populi, prepare a
		program object and add it to a new array of programs.
		Create an empty array.
		 */
		$programs = array();
		$countDegrees = 0;
		foreach ($rawArray as $pr) {
			log_message('info', 'MODEL: PopuliApi | Parsing program ' . $pr->program_id . ' for person ' . $personId);

			// Update the properties of $pr that contain empty objects.
			if (isset($pr->started_on) && !is_string($pr->started_on)) $pr->started_on = null;
			if (isset($pr->inactive) && !is_string($pr->inactive)) $pr->inactive = null;

			/*
			If this program has a degree, extract the raw degree info.  Next, determine
			if that raw degree info is a single degree (not an array) or multiple degrees
			and make them the same.  As in, make a single degree a single element array
			like the mulitple degrees.  Now we can iterate over the degrees using foreach.
			 */
			if (isset($pr->degrees->degree)) {
				$rawDegrees = $pr->degrees->degree;
				// Also double check if the list of degrees is not an array, and if not
				// make a single element array.
				if (!is_array($rawDegrees)) $rawDegrees = array($rawDegrees);
			} else {
				// This program has no degree.  Probably a person taking single courses.
				$rawDegrees = null;
			}

			/*
			Iterate over the degrees array and parse out the specializations.  But only
			if there are degrees.
			Create an empty array for the degrees.
			 */
			$degrees = array();
			if ($rawDegrees) {
				foreach ($rawDegrees as $degree) {
					$countDegrees++;
					log_message('info', 'MODEL: PopuliApi | Parsing degree ' . $degree->degree_id . ' for ' . $personId);

					// Update the properties of $degree that contain empty objects.
					if (!is_string($degree->inactive_date))		$degree->inactive_date = null;
					if (!is_string($degree->graduation_date))		$degree->graduation_date = null;

					// If the degree name contains ' - ' then the specilization is determined using the degree name.
					if (strpos($degree->name, ' - ')) {
						log_message('info', 'MODEL: PopuliApi | Extracing the specialization from the name field for degree ' . $degree->degree_id);

						/*
						Determine the degree name and specialization.
						 */
						// Explode the degree using the delimeter.
						$degreeNameArray = explode(' - ', $degree->name);

						// Build a specializations array that contains specializations objects.
						// Since we are building the only specialization from the degree's name, we
						// don't need to loop over an array.
						$specializationsArray = array(json_decode(json_encode(array(
							'specialization_id'	=> null,
							// The last element is always the specialization.
							'name'							=> $degreeNameArray[count($degreeNameArray) - 1],
							'type'							=> 'MAJOR',
							'status'						=> $degree->status,
							'granted_date'			=> $degree->graduation_date,
						))));
						//
						$degree->specializations = $specializationsArray;

						// If the array is bigger than one element, chop off the last item
						if (count($degreeNameArray) > 1) unset($degreeNameArray[count($degreeNameArray) -1]);
						// Put the string back together to create the final degree name.
						$degree->name = join(' - ', $degreeNameArray);

					} else if (isset($degree->specializations->specialization)) {
						// If not in the degree name, check if there is a specialization noted in the Populi-defined fields.
						log_message('info', 'MODEL: PopuliApi | Extracing the specialization from the Populi-defined specialization for degree ' . $degree->degree_id);

						// Get the specialization info out of the degree object.
						// At this point, we assume there is only one specialization, although Populi will allow
						// for multiple specializations.
						$specialization = $degree->specializations->specialization;

						// Tidy up some data.
						if (!is_string($specialization->granted_date))		$specialization->granted_date = null;
						// Tidy up the specialization name if needed.
						if (strpos($specialization->name, ' - ')) {
							$specName = explode(' - ', $specialization->name);
							// Take the last element as the specialization name
							$specialization->name = $specName[count($specName) - 1];
						}

						// Put the updated specialization info into an array and add it back to the degree object.
						$degree->specializations = array($specialization);

					} else {
						// If none of the conditions above are present, we won't be tracking specializations.
						log_message('info', 'MODEL: PopuliApi | Degree ' . $degree->degree_id . ' for ' . $personId . ' has NO specialization.');
						$degree->specializations = null;
					}

					// Push this parsed out degree object onto the new degrees array.
					array_push($degrees, $degree);
				}
			} else {
				// In this case rawDegrees doesn't have any data so make sure the property is correct.
				$degrees = null;
			}

			// Update the program object with the new degrees array.
			$pr->degrees = $degrees;

			/*
			Averngers Assemble!  Add the final parsed object to the array of programs.
			 */
			array_push($programs, $pr);
		}

  	/*
		Phew.  That was a lot of work.  Now, return the array of program objects.
  	 */
  	log_message('debug', 'MODEL: PopuliApi | ' . $personId . ' has enrolled in ' . $countDegrees . ' degree(s) as part of ' . count($programs) . ' program(s).');
    return $programs;
  }




















	/**
	 * Implementation of the addTag api call.
	 * @param int $person_id			A numerical person id.
	 * @param int $tag_id    			A numerical tag id.
	 * @param String $tag       	The name of the tag you want to add.
	 */
	public function addTag($person_id, $tag_id = null, $tag = null) {
		log_message('info', 'MODEL: PopuliApi | addTag()');

		// One of $tag_id or $tag must be set.
		if (is_null($tag_id) && is_null($tag)) {
			log_message('debug', 'MODEL: PopuliApi | Either tag_id or tag must be set.');
			return false;
		}

		// Set the arguments.
		$args['person_id']	= $person_id;
		if (isset($tag_id)) 	$args['tag_id'] = $tag_id;
		if (isset($tag)) 			$args['tag'] = $tag;

		// Punch the call to the api.
		$response = $this->PopuliConnect->doTask('addTag', $args);

		// Evaluate the response.
		if (json_decode($response)->result == 'SUCCESS') {
			log_message('info', 'MODEL: PopuliApi | Successfully added the tag to the person ' . $person_id);
			return true;
		} else {
			log_message('error', 'MODEL: PopuliApi | We failed to add the tag.');
			return false;
		}
	}


















	/**
	 * Local implementation of getTaggedPeople API call.
	 * @param  [int] $tagId   		Numeric ID of the tag.  Must specify one of either tagId or tagName.
	 * @param  [string] $tagName  Name of the tag.
	 * @param  [int] $page				If you're using a limit, which page of results would you like.
	 * @return [array]           An array of person objects (including personId, first, last)
	 */
	public function getTaggedPeople($tagId = NULL, $tagName = NULL, $page = NULL) {
		log_message('info', 'MODEL: PopuliApi | getTaggedPeople()');

		/*
		 * Validate the input.
		 */
		if (!$tagId && !$tagName) {
			log_message('error', 'MODEL: PopuliApi | Please specify either a tagId or a tagName.');
		}

		// Setup the array that will be used to interact with Populi.
		$args = array();
		if ($tagId) 		$args['tagID'] = $tagId;
		if ($tagName) 	$args['tagName'] = $tagName;
		if ($page) 			$args['page'] = $page;

		// Tell the log what we are about to do.
		log_message('debug', 'MODEL: PopuliApi | Asking Populi for a list of people on tag list: ' . implode(',', $args));

		// Go get the data from Populi.
		$data = $this->PopuliConnect->doTask('getTaggedPeople', $args);
		// Encode the whole thing into a json object.
		$json = json_decode($data);
		// Turn the json object into an array and access the @attributes element.
		$array = (array)$json;
		$attributes = $array['@attributes'];
		log_message('debug', 'MODEL: PopuliApi | getTaggedPeople returned ' . $attributes->num_results . ' results.');

		// Return the array of people objects.
		return $json->person;
	}









	/**
	 * Returns a list of available tags.
	 */
	public function getTags() {
		log_message('info', 'MODEL: PopuliApi | getTags()');
		$data = $this->PopuliConnect->doTask('getTags');
		$data = json_decode($data);
		return $data->tags->tag;
	}









	/**
	 * Implements Populi's getTermBilling info API call.
	 * @param  int $termId		A numerical term id.
	 * @return array     			An array of term billing objects.
	 */
	public function getTermBillingInfo($termId) {
		log_message('info', 'MODEL: PopuliApi | getTermBillingInfo()');

		if (!is_int($termId)) {
			log_message('debug', 'MODEL: PopuliApi | Please supply a numerical term id.');
			return false;
		}

		// Get the raw data from Populi.  This will need to be massaged into an array of objects.
		$raw = $this->PopuliConnect->doTask('getTermBillingInfo', array('academic_term_id' => $termId));

		// Get the array of person objects.
		$raw = json_decode($raw)->people->person;

		// Return that array.
		return $raw;
	}











	/**
	 * Generates a list of students enrolled in classes for a particular academic term.
	 *
	 */
	public function getTermEnrollment($term_id) {
		$data = $this->PopuliConnect->doTask('getTermEnrollment', array('term_id' => $term_id));
		$data = json_decode($data);
		return $data;
	}






	/**
	 * Generates a list of students enrolled in class for a particular semester.  If no
	 * input is specified, it uses the current semester.
	 * @param  integer $term
	 * @return array        An array of json objects.
	 */
	public function getTermStudents($termId = null) {
		log_message('info', 'MODEL: PopuliApi | getTermStudents()');

		if ($termId) {
			$args = array('term_id' => $termId);
			$message = 'term id ' . $termId;
		} else {
			$args = array();
			$message = 'the current term';
		}

		log_message('debug', 'MODEL: PopuliApi | Getting student data from Populi for ' . $message);
		$data = $this->PopuliConnect->doTask('getTermStudents', $args);
	  // Encode it as a json object.
	  $data = json_decode($data)->student;

		log_message('info', 'MODEL: PopuliApi | Found ' . sizeof($data) . ' student records.');

		// Return the object.
		return $data;
	}










	public function getUpdatedEnrollment($startDate = SYSTEM_ALIVE_DATE) {
		log_message('info', 'MODEL: PopuliApi | getUpdatedEnrollment()');

		$args = array( 'start_date'	=> $startDate);
		$data = $this->PopuliConnect->doTask('getUpdatedEnrollment', $args);
		// Encode the whole thing into a json object.
		$json = json_decode($data)->enrollment;
		log_message('debug', 'MODEL: PopuliApi | getUpdatedEnrollment returned ' . sizeOf($json) . ' enrollments changed since ' . $startDate . ' (limit 200 returned)');
		// If there were changes in enrollment.
		if (sizeOf($json) > 0) return $json;
		return false;
	}
	public function getUpdatedPeople($startDate = SYSTEM_ALIVE_DATE) {
		log_message('info', 'MODEL: PopuliApi | getUpdatedPeople()');

		$args = array( 'start_time'	=> $startDate );
		$data = $this->PopuliConnect->doTask('getUpdatedPeople', $args);
		// Encode the whole thing into a json object.
		$json = json_decode($data);
		// Turn the json object into an array and access the @attributes element.
		$array = (array)$json;
		$numResults = (int)$array['@attributes']->num_results;
		log_message('debug', 'MODEL: PopuliApi | getUpdatedPeople returned ' . $numResults . ' people updated since ' . $startDate . ' (limit 200 returned)');

		// If there are people, return them.
		if ($numResults > 0) return $json->person;
		// If we got this far there were no records.
		return false;
	}





















}
?>
