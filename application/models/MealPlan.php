<?php
/**
 * A PHP Class for processing meal plans and their balances for student accounts.
 */


class MealPlan extends CI_Model {

	public function __construct() {
		// Call the parent constructor
		parent::__construct();

		// Load the appropriate models
		$this->load->model('populi/PopuliConnect');
		$this->load->model('populi/PopuliApi');
		$this->load->model('populi/PopuliApiExtended');
	}









	/**
	 * [getMealPlans Gets a list of students with meal plans.]
	 * @return array 			[Returns and associcative array of person_id => $mealPlanAmount]
	 */
	public function getCurrentStudents() {
		log_message('info', 'MODEL: MealPlan | getCurrentStudents()');

		// Get all pending charges from Populi for this current term.
		$pendingCharges = $this->PopuliApi->getPendingCharges($this->PopuliApiExtended->getCurrentTermId(), 'MEAL_PLAN');

		// Create an empty array.
		$mealPlanData = array();

		// Add data to the array.
		// Only add data for pending charge type of MEAL_PLAN.
		foreach ($pendingCharges as $c) {
			array_push($mealPlanData, array(
				'person_id'		=> $c->person_id,
				'first'				=> $c->first,
				'last'				=> $c->last,
				'term_id'			=> $c->term_id,
				'term_name'		=> $c->term_name,
				'type'				=> $c->type,
				'charge'			=> $c->charge,
				'amount'			=> $c->amount,
			));
		}

		// Return the array of students and their meal plans.
		return $mealPlanData;
	}










	/**
	 * Returns a list of people on a meal plan and the name and id of that meal plan.
	 * @return [type] An array of objects containing personId, termId, mpId, mpName.
	 */
	public function getPeopleOnMealDEPRECATED($termId = null) {
		log_message('debug', 'MODEL: MealPlan | getPeopleOnMealPlans()');

		// Go get the bulk data from Populi.
		if (!$termId) {
			$termId = $this->PopuliApiExtended->getCurrentTermId();
		}
		$people = $this->PopuliApi->getTermBillingInfo($termId);

		// Create an empty array.
		$mealPlanData = array();

		// Add data to the array.
		// Only add data for if there is a meal plan for this person.
		$countMealPlanPeople = 0;
		foreach ($people as $p) {
			// Convert the meal plan id into an array and evaluate if it's empty.
			if (!empty((array)$p->meal_plan_id)) {

				// Count this person as being on a meal plan.
				$countMealPlanPeople++;

				// Prepare variable data
				$personId = (int)$p->person_id;
				$mpId = (int)$p->meal_plan_id;
				$mpName = (string)$p->meal_plan_name;

				// Create an array for this person's meal plan data.
				$pData = array(
					'personId'		=> $personId,
					'termId'		=> $termId,
					'mpId'			=> $mpId,
					'mpName'		=> $mpName,
				);

				// Prepare the object and add it to the array.
				$pObject = json_decode(json_encode($pData));
				array_push($mealPlanData, $pObject);
			}
		}

		log_message('debug', 'MODEL: MealPlan | There are ' . $countMealPlanPeople . ' people on meal plans for term ' . $termId);

		// Return the list of folks on a meal plan.
		return $mealPlanData;
	}

	/**
	 * New implementation of this function.  Simplified version that gets the meal plan data for each person directly out of the cache.
	 */
	public function getPeopleOnMealPlans($updateCache = false) {
		// If requested, update the cache first.
		if ($updateCache) $this->PopuliApiCache->cachePeopleOnMealPlans();

		// Get the current term ID from settings.
		$termId = $this->PopuliApiCache->getCurrentTermId();
		// Get the data out of the database for this term.
		$this->db->where('term_id', $termId);
		$query = $this->db->get('cache_term_meal_plans');
		$result = $query->result();

		// If there is data, return it.
		return ($query->result()) ? $query->result() : false;
	}








	/**
	 * Gathers updated meal plan data from Populi and adds it to the local database.
	 */
	public function setCachedMealPlans() {
		log_message('debug', 'MODEL: MealPlan | setCachedMealPlans()');

		// Fetch the data from Populi and convert it to a JSON string.
		$data = $this->PopuliApi->getMealPlans();

		foreach($data as $plan) {
			// Prepare the data
			$data = array(
				'id'							=> $plan->id,
				'name'						=> $plan->name,
				'amount'					=> $plan->amount,
				'status'					=> $plan->status,
				'account_name'		=> $plan->account_name,
				'account_number'	=> $plan->account_number,
			);

			if (!$this->getCachedMealPlan($plan->id)) {
				// Insert.
				log_message('debug', 'MODEL: MealPlan | setCachedMealPlans() Inserting new meal plan with id ' . $plan->id);
	      // Insert the data into the database.
	      $this->db->insert('meal_plans', $data);
			} else {
				// Update.
				log_message('info', 'MODEL: MealPlan | setCachedMealPlans() Updating existing meal plan with id ' . $plan->id);
        // Perform the update.
        $this->db->where('id', $plan->id);
        $this->db->update('meal_plans', $data);
			}
		}
	}

















	/**
	 * Returns all information related to meal plans configured from our local database.
	 * @return array 				An array of mealPlan objects.
	 */
	public function getMealPlans() {
		log_message('info', 'MODEL: MealPlan | getMealPlans()');

		// Query the database
		$query = $this->db->get('meal_plans');

    // Prepare the query results to be returned to the caller.
    ($query->result()) ? $result = $query->result() : $result = false;
    return $result;
	}




	/**
	 * Gets and returns a json object for a single meal plan based on $id.
	 * @param  int $id		A numerical meal plan id.
	 * @return object     A json object containing information for a single meal plan.
	 */
	public function getMealPlan($id) {
		log_message('info', 'MODEL: MealPlan | getMealPlan()');

    // Query the database
    $this->db->where('id', $id);
    $query = $this->db->get('meal_plans');

		// If the first row returned by the query has data, return that data.
    ($query->row()) ? $row = $query->row() : $row = false;
		if (!$row) log_message('error', 'MODEL: MealPlan | A meal plan with ID ' . $id . ' does not exist.');
		return $row;
	}




	/**
	 * This is just a renamed getMealPlan() function.
	 */
	public function getCachedMealPlan($id) {
		log_message('info', 'MODEL: MealPlan | getCachedMealPlan()');
		return $this->MealPlan->getMealPlan($id);
	}



	/**
	 * Get a single persons entry out of the cached table of meal plans.
	 */
	public function getCachedMealPlanByPerson($personId) {
		$this->db->where('person_id', $personId);
		$query = $this->db->get('cache_term_meal_plans');

		($query->row()) ? $result = $query->row() : $result = false;
		return $result;
	}
	/**
	 * Function to update the cached meal plan record for this person for a current semester.
	 * 
	 * Expects one parameter, and object containing 'person_id', 'term_id', 'mp_id', and 'mp_name'
	 */
	public function setCachedMealPlanByPerson($mealPlanPersonObject) {
		// Extract the person id cause we'll use it a lot.
		$pId = $mealPlanPersonObject->person_id;
		// See if there is an existing record.
		$existingRecord = $this->getCachedMealPlanByPerson($pId);

		// If this person has a meal plan in the term meal plan list, just update it.
		if ($existingRecord) {
			$this->db->where('person_id', $pId);
			// Update an existing record
			$result = $this->db->update('cache_term_meal_plans', $mealPlanPersonObject);
			if ($result) log_message('info', 'MODEL: MealPlan | Meal plan metadata UPDATED for ' . $pId . ' in the cache.');
		} else {
			// Insert the data
			$result = $this->db->insert('cache_term_meal_plans', $mealPlanPersonObject);
			if ($result) log_message('info', 'MODEL: MealPlan | Meal plan metadata for ' . $pId . ' ADDED to the cache.');
		}
		
		// In case of an error, log it.
		if (!$result) log_message('error', 'MODEL: MealPlan | Metadata FAILED to update. pId=' . $pId);

		//if ($result) { return $this->getCachedMealPlanByPerson($pId); } else { return false; }
		return ($result) ? $this->getCachedMealPlanByPerson($pId) : false;
	}









	/**
	 * [getBalance Gets the custom Meal Plan Balance field]
	 * @param  [type] $personId [description]
	 * @return [type]           [description]
	 */
	 public function getBalance($personId) {
		 $mealPlanBalance = NULL;

		 // Get the custom fields
		 $customFields = $this->PopuliApi->getCustomFields($personId, 'STUDENT');

		 // Loop through each of the custom fields looking for the meal plan one.
		 foreach ($customFields as $c) {
			 if ($c->name == "Meal Plan Balance") {
				 // Return the value of this field.
				 $mealPlanBalance = $c->value;
			 }
		 }

		 // Return the balance or NULL if a balance was not found.
		 return $mealPlanBalance;
	 }





	/**
	 * [setBalance loads the PopuliApi and adds a custom fields based on the input parameters.]
	 * @param int $personId     	A numerical person id.
	 * @param float $value        A decimal price.
	 */
	public function setBalance($personId, $value) {
		log_message('info', 'MODEL: MealPlan | setBalance()');

		// Go get the custom field id.
		$customFieldId = $this->MealPlan->getBalanceCustomFieldId();

		// Punch the query.
		$result = $this->PopuliApi->setCustomField($personId, $customFieldId, $value);

    // Evaluate the result.
		if ($result) {
			log_message('debug', 'MODEL: MealPlan | Successfully updated the cafeteria balance in Populi for ' . $personId . ' to $' . $value);
			return true;
		} else {
			log_message('error', 'MODEL: MealPlan | There was an error updating the balance for ' . $personId);
			return false;
		}
	}




	/**
	 * Sets the cached balance in the local database for this person.
	 * @param [type] $personId A person_id of an account in the local
	 * @param [type] $value    The amount of the balance.
	 */
	public function setCachedBalance($personId, $value) {
		log_message('info', 'MealPlan->setCachedBalance(): $personId = ' . $personId);

    // Perform the update.
    $this->db->where('person_id', $personId);
    $this->db->update('account', array('mp_balance_populi' => $value));
	}

	/**
	 * Gets the current value of the cache mp_balance_populi field.
	 * @param  [type] $personId The numberic ID of the person.
	 * @return [type]           The balance.
	 */
	public function getCachedBalance($personId) {
		log_message('info', 'MealPlan->getCachedBalance()');

    // Query the database
    $this->db->where('person_id', $personId);
    $query = $this->db->get('account', 'mp_balance_populi');

    // Get just the balance.
    return $query->row()->mp_balance_populi;
	}









	/**
	 * Updates the remote account balance stored in Populi using the value
	 * in the local database for a specific account.
	 * @param  int $accountId		A numerical account id.
	 * @return boolean          Was the update successful or not.
	 */
	public function updatePopuliAccountBalance($accountId) {
		log_message('debug', 'MODEL: MealPlan | updatePopuliAccountBalance()');

		// Get the account balance.
		$account = $this->Account->getAccount($accountId);
		$balance = $account->mp_balance;
		$localPopuliBalance = $account->mp_balance_populi;

		// Set the balance in Populi.
		$result = $this->MealPlan->setBalance($accountId, $balance);

		// If that was successful then set the cached balance in our local database to this same value.
		if ($result) $this->MealPlan->setCachedBalance($accountId, $balance);

		// Make notes in the log about what happened when we tried to update the balance.
		if ($result) {
			log_message('debug', 'MODEL: MealPlan | The remote balance in Populi was updated successfully from ' . $localPopuliBalance . ' to ' . $balance . '.');
			return true;
		} else {
			log_message('error', 'MODEL: MealPlan | There was an error updating the remote balance in Populi.');
			return false;
		}
	}












	public function addTag($personId) {
		// Call the PopuliApi function.
		$result = $this->PopuliApi->addTag($personId, null, 'MEAL PLANS');
		return $result;
	}






	/**
	 * [getBalanceCustomFieldId Figure out the id of the custom Meal Plan Balance field]
	 * @return [type] [description]
	 */
	public function getBalanceCustomFieldId() {
		// Default value returned.  -1 in case we don't find anything.
		$MealPlanBalanceId = -1;

		// Get a list of all custom fields.
		$allCustomFields = $this->PopuliApi->getAllCustomFields();

		// Loop through each of the returned fields.
		foreach ($allCustomFields as $c) {
			if ($c->name == "Meal Plan Balance") {
				// Get the id of this field.
				$MealPlanBalanceId = (int)$c->id;
				break;
			}
		}

		return $MealPlanBalanceId;
	}







	/**
	 * Function to determine if the current semester is Winter.
	 */
	public function isCurrentSemesterWinter() {
    // TO DO: add logic to determine which semester we're adding data from.
    // Rules.  Fall -> Winter, balances carry over.
    //          Winter -> Spring,Fall, balance do not carry over.
    //
    // If the current semester is Winter and the user has a balance from the
    // previous semester, which is fall, add the new meal plan to the previous balance.
    // Otherwise, throw the balance away and start again.
    // For now we are hard coded for winter because in Winter we will not throw balances away.
		// Set the return value to false by default.
		$isCurrentSemesterWinter = false;
		$currentSemester = $this->PopuliApiExtended->getCurrentTermName();

		// Check and set to true if the current semester is winter.
		if ($currentSemester == 'winter') { $isCurrentSemesterWinter = true; }

		return $isCurrentSemesterWinter;
	}







}
