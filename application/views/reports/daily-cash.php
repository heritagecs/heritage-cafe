<?php

// Set useful variables
$start = $dates->start;
$end = $dates->end;
$kind = $dates->kind;
$today = date('Y-m-d');
// Set the previous and next parameters.
$pStart = $dates->pStart;
$pEnd = $dates->pEnd;
$nStart = $dates->nStart;
$nEnd = $dates->nEnd;

// An array for the different kinds to loop over.
$kinds = array('day', 'week', 'month', 'year');
// Prepare the dates as unix timestamps.
$timestampStart = nice_date($start);
$timestampEnd = nice_date($end);

switch ($kind) {
	case 'day':
		$kindDisplayDate = date('l d M', $timestampStart);
		break;
	case 'month':
		$kindDisplayDate = date('F Y', $timestampStart);
		break;
	case 'year':
		$kindDisplayDate = date('Y', $timestampStart);
		break;
	default:
		$kindDisplayDate = $start . ' to ' . $end;
}

?>


<div id="" class="site-content-inner">

<h1>Daily Cash Totals</h1>

<div id="page-top" class="reports-top">
	<div class="reports-timeframe">
		<?php
		$attr = array(
			'class'	=> 'btn btn-timeframe btn-backwards',
		);
		$url = base_url() . 'reports/dailycash/' . $pStart . '/' . $pEnd . '/' . $kind;
		echo anchor($url, 'Previous', $attr);

		$attr = array(
			'class'	=> 'btn btn-timeframe btn-forwards',
		);
		$url = base_url() . 'reports/dailycash/' . $nStart . '/' . $nEnd . '/' . $kind;
		echo anchor($url, 'Next', $attr);
		?>
		<span><?php echo '<span class="kind-heading">' . $kind . '</span>: ' . $kindDisplayDate; ?></span>
	</div>
</div>

<section class="reports-daily-cash tbl-accordion">
  <table>
    <thead>
      <tr><th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Thursday</th><th>Friday</th><th>Saturday</th><th>Sunday</th><th>Weekly Total</th></tr>
    </thead>
    <tbody>
      <tr><?php
        // Setup the first date.
        $date = $dates->start;
        $weeklyTotal = 0;
        // For each day of the week, loop through the transactions and calculate the total.
        for ($x = 1; $x <= 7; $x++) {
          log_message('info', 'DAY ' . $x . ' TOTALS | ' . $date);
          // Reset the daily total.
          $dailyTotal = 0;
          // Loop through the transactions looking for transactions from $date.
          foreach ($cashTransactions as $t) {
            $transactionDate = $this->Sales->sanitizeDate($t->date);
            log_message('info', 'Looking at transaction # ' . $t->id . ' with the date of ' . $transactionDate);
            if ($date == $transactionDate) {
              // Add the transaction total to the daily total.
              $dailyTotal += $t->total;
              log_message('debug', 'Trans #' . $t->id . ' is from ' . $transactionDate . ' | Daily Total: ' . $dailyTotal);
            }
          }
          // Now that we have a proper daily total, print a table cell.
          echo '<td class="price">' . number_format($dailyTotal, 2) . '</td>';
          $weeklyTotal += $dailyTotal;
          // Increment the date.
          $date = $this->Sales->addToDate($date, 1);
        }
        // Print out the weekly total
        echo '<td class="price"><strong>' . number_format($weeklyTotal, 2) . '</strong></td>';
      ?></tr>
      <!--<tr><td>Account Deposits</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
      <tr class="table-summary"><td>Totals</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>-->
    </tbody>
  </table>
</section>

</div><!-- .site-content-inner DIV -->
