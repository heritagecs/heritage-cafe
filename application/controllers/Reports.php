<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	/**
	 * Constructor
	 */
	public function __construct() {
		// Call the parent constructor
		parent::__construct();

		// Load up the appropriate models.
		$this->load->model('User');
		$this->load->model('Role');
		$this->load->model('Account');
		$this->load->model('Transaction');
		$this->load->model('InventoryTable');
		$this->load->model('InventoryCategory');
		$this->load->model('Sales');


		// Check if the user is logged in.
		$this->User->is_logged_in();


	}


	/**
	 * Index Page for this controller.
	 *
	 */
	public function index() {
		$today = date('Y-m-d');

		redirect('reports/accounts');
	}




	public function sales($userStart = -1, $userEnd = -1, $kind = 'day') {
    log_message('debug', '-----> CONTROLLER: Reports->sales().');

		/* Validate the users input. */
    // Sanitize the input dates.
		$userStart = $this->Sales->sanitizeDate($userStart);
		$userEnd = $this->Sales->sanitizeDate($userEnd);

		log_message('debug', 'Reports->sales().  Sanitized Dates: $startDate = ' . $userStart . ' | $endDate = ' . $userEnd);

		// Calculate what the previous and next segments are based on the kind.
		$dates = $this->Sales->calculateReportDates($userStart, $userEnd, $kind);

		// Create a json string of the date information.
		$jsonDates = json_encode(array(
			'kind'		=> $dates->kind,
			'start'		=> $dates->start,
			'end'			=> $dates->end,
			'pStart'	=> $dates->pStart,
			'pEnd'		=> $dates->pEnd,
			'nStart'	=> $dates->nStart,
			'nEnd'		=> $dates->nEnd,
		));

		// Tell the log what's up.
		log_message('debug', 'Dates to the view: ' . $jsonDates);

		// Generate the data from the query functions in the model.
		// Convert the $dates object into a json string.
		$data['salesData'] = $this->Sales->getSalesData($dates->start, $dates->end);
		$data['dates'] = json_decode($jsonDates);

		$this->load->view('parts/header');
		$this->load->view('nav/reports');
		$this->load->view('reports/sales', $data);
		$this->load->view('parts/footer');
	}







	public function accounts() {
		// Get the accounts data.
		$data['accounts'] = $this->Account->getAccounts('first');
		$data['countMealPlanAccounts'] = sizeof($this->Account->getAccountsMealPlan());
		$data['filter_type'] = $this->input->get('type');

		// Load the view.
		$this->load->view('parts/header');
		$this->load->view('nav/reports');
		$this->load->view('reports/accounts', $data);
		$this->load->view('parts/footer');
	}


	public function dailyCash($userStart = -1, $userEnd = -1, $kind = 'week') {
		// Calculate what the previous and next segments are based on the kind.
		$dates = $this->Sales->calculateReportDates($userStart, $userEnd, $kind);

		// Create a json string of the date information.
		$jsonDates = json_encode(array(
			'kind'		=> $dates->kind,
			'start'		=> $dates->start,
			'end'			=> $dates->end,
			'pStart'	=> $dates->pStart,
			'pEnd'		=> $dates->pEnd,
			'nStart'	=> $dates->nStart,
			'nEnd'		=> $dates->nEnd,
		));

	  // Get the Cash transaction data for this week.
	  $data['cashTransactions'] = $this->Sales->getCashSalesData($dates->start, $dates->end);
		$data['dates'] = json_decode($jsonDates);

	  // Load the view.
		$this->load->view('parts/header');
		$this->load->view('nav/reports');
		$this->load->view('reports/daily-cash', $data);
		$this->load->view('parts/footer');
	}




}
