<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Unit extends CI_Controller {

  public function __construct() {
    // Call the parent constructor
		parent::__construct();

    // Load up the appropriate models.
    $this->load->model('User');
    $this->load->model('Role');
    $this->load->model('MealPlan');
    $this->load->model('populi/PopuliApi');
    $this->load->model('Account');
    $this->load->model('Transaction');
    $this->load->model('Sync');
    $this->load->model('InventoryTable');
    $this->load->model('Sales');
    $this->load->model('MealPlan');
    $this->load->model('AcademicTerms');
    $this->load->model('PaymentType');
    $this->load->model('ItemsSold');
    $this->load->model('common/Settings');
    $this->load->model('CoffeeshopTab');
    $this->load->model('Util');
  }

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
    log_message('debug', '-----> CONTROLLER: Unit Test');
    $this->load->view('parts/header-test');


    $personId = 11291918;
    $stu = $personId;
    $cindy = 11291930;
    $amount = 4;
    $date = '2017-12-13';
    $termId = 171230;
    $mpId = 1484;
    $mpName = 'Custom';
    $debitTransId = 20015;
    $creditTransId = 23656;
    $cashTransId = 8472;
    $storefront = $_COOKIE['storefront'];
    $trent = 21150703;
    $currentTermId = $this->PopuliApiCache->getCurrentTermId();

    $mpData = (Object)array(
      'personId'  => $personId,
      'termId'    => $termId,
      'mpId'      => $mpId,
      'mpName'    => $mpName,
    );

    $object = (object)array(
      'total'             => $amount,
      'payment_type'      => 1,
      'storefront'        => $_COOKIE['storefront'],
      'date'              => null,
      'transaction_type'  => 0,
      'items'             => null,
      'account_id'        => $personId,
      'tax'               => 0,
      'discount'          => 0,
      'memo'              => null,
      'gl_account'        => null,        
    );
    

    



    //$data = $this->Util->zeroStaffAccounts();
    //$data = $this->Account->zeroCafeAccount(11283967);
    //$data = $this->Account->getAccountsByType('/STAFF', 'ALL');
    //$data = $this->Sync->updateSupportingData();
    $data = $this->Account->removeMealPlan(19283000);
  
    
      
    

    
    
    
    
  

    




    /*
      Display the data returned by the test function.
     */
    echo 'RETURNED TYPE: ' . gettype($data) . '<br />';
    if (is_array($data)) {
      echo 'Yep, that was an array... ' . count($data) . ' records returned.';
      echo '<br />';
      echo '<br />';
      foreach ($data as $d) {
        echo '<pre>' . var_export($d, true) . '</pre>';
        echo '<br />';
        echo '<br />';
      }
    } else {
      echo 'Only one element was returned.';
      echo '<br />';
      echo '<br />';
      echo '<pre>' . var_export($data, true) . '</pre>';
    }
















		$this->load->view('parts/footer');
	}



















  public function listLocalAccounts() {
    $accounts = $this->Account->getAccounts();

    echo '<h2>Local Accounts</h2>';
    echo '<table>';
    foreach ($accounts as $a) {
      echo '<tr>';
      echo '<td>' . $a->person_id . '</td>';
      echo '<td>' . $a->first . ' ' . $a->last . '</td>';
      echo '<td>' . $a->mp_name . '</td>';
      echo '<td>' . $a->mp_balance . '</td>';
      echo '</tr>';
    }
    echo '</table>';
  }








  public function listPopuliMealPlanStudents() {
    // Get the meal plans for each person.
    $people = $this->MealPlan->getCurrentStudents();

    // Loop through each student, listing their balance.
    echo '<h2>Meal Plan Students from Populi</h2>';
    echo '<table>';
    foreach ($people as $p) {
      // Variables for this person
      $id = $p['person_id'];
      $balance = $this->MealPlan->getBalance($id);

      // List the balances for each person on a meal plan.
      echo '<tr>';
      echo '<td>' . $p['person_id'] . '</td>';
      echo '<td>' . $p['first'] . ' ' . $p['last'] . '</td>';
      echo '<td>' . $p['charge'] . '</td>';
      echo '<td>' . $balance . '</td>';
      echo '</tr>';
    }
    echo '</table>';
  }

}
