<?php

// Conditions for adding the logo.
$logo = base_url() . "assets/img/Heritage_CandS_logo.png";
$logoHTML = '<img class="banner-logo" src="' . $logo . '" />';

// Get the logged in flag
$is_logged_in = $this->session->is_logged_in;

// Check if the session is set.
if (isset($is_logged_in) || $is_logged_in == true) {
  echo $logoHTML;
}

?>
