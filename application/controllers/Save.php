<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Save extends CI_Controller {

	public function __construct() {
		// Call the parent constructor
		parent::__construct();

		// Load up the appropriate models.
		$this->load->model('InventoryTable');
		$this->load->model('InventoryCategory');
		$this->load->model('TaxCode');
		$this->load->model('User');
		$this->load->model('populi/PopuliApi');

		// Check if the user is logged in.
		$this->User->is_logged_in();
	}



	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{

	}











	public function inventory() {
		log_message('debug', 'CONTROLLER: Save | inventory()');

		// When this controller loads for the first time, check for POST data.
		// If there is, determine which form submitted data and process the data.
		if ($this->input->post('item_description') != null) {
			log_message('debug', 'CONTROLLER: Save | There is item data: ' . json_encode($this->input->post()));
			$description	= $this->input->post('item_description');
			$taxCode			= 1;
			$price 				= $this->input->post('item_price');
			$category			= $this->input->post('item_category');

			log_message('debug', 'CONTROLLER: Save | Category: ' . $category);

			// Push the data to the model for processing.
			$this->InventoryTable->addNewItem($category, $description, $taxCode, $price);
		} else {
			log_message('debug', 'CONTROLLER: Save | We are not adding a new item.');
		}


		// Check for data from the category form and process it.
		if ($this->input->post('category_name') != null) {
			log_message('debug', 'CONTROLLER: Save | There is category data: ' . json_encode($this->input->post()));
			// New variables for the category info.
			$name					= $this->input->post('category_name');
			$description	= $this->input->post('category_description');

			// Push the data to the model for processing.
			$this->InventoryCategory->addNewCategory($name, $description);
		} else {
			log_message('debug', 'CONTROLLER: Save | We are not adding a new category.');
		}



		// Check for data from the import utility and process it into an array
		if ($this->input->post('import_csv') != null) {
			log_message('debug', 'CONTROLLER: Save | There was CSV data: ' . json_encode($this->input->post()));
			// New variables for the csv data.
			$csvData = $this->input->post('import_csv');

			// Parse the data into an array.
			// Explode the data splitting at each new line.
			$csvArray = explode("\n", trim($csvData));

			foreach ($csvArray as $csv) {
				$line = str_getcsv($csv, ',');

				$description	= $line[0];
				$category			= $line[1];
				$taxCode			= $line[3];
				$price 				= $line[2];

				// Add this item to the database
				$this->InventoryTable->addNewItem($category, $description, $taxCode, $price);
			}

			// Push the data to the model for processing.
			//echolog("csvData: " . $csvData);
		} else {
			log_message('debug', 'CONTROLLER: Save | We are not adding inventory items via CSV upload.');
		}

		// Unset POST data
		unset($_POST);

		// Redirect to the Inventory controller
		redirect('/inventory', 'refresh');
	}




	/**
	 * [editItem description]
	 * @return [type] [description]
	 */
	public function item($item_id) {
		// Load a link to the inventory page
		log_message('debug', 'CONTROLLER: Save | items()');

		var_dump($this->input->post('item_active'));

		$active = 1;
		if ($this->input->post('item_active') == null) $active = 0;

		// Check for POST data
		if ($this->input->post('item_description') != null) {
			// Create the item object.
			$itemObject = (object)array(
				'id'			=> $item_id,
				'category'		=> $this->input->post('item_category'),
				'description'	=> $this->input->post('item_description'),
				'taxCode'		=> $this->input->post('item_tax_code'),
				'price'			=> $this->input->post('item_price'),
				'is_active'		=> $active,
			);

			// Push the data to the model for processing.
			$this->InventoryTable->updateItem($itemObject);
		} else {
			log_message('debug', 'CONTROLLER: Save | No item data.');
		}

		// Unset POST data
		unset($_POST);

		// Redirect to the Inventory controller
		redirect('/inventory', 'refresh');
	}



	/**
	 * [api Method for saving API settings from the settings form]
	 * @return [type] [description]
	 */
	public function api() {
		// Check the user entered something into the password confirmation box.
		if ($this->input->post('settings_password_confirm') != null) {
			// Put the post data into variables
			$username			= $this->input->post('settings_username');
			$password			= $this->input->post('settings_password');
			$pass_confirm	= $this->input->post('settings_password_confirm');

			// Check that the passwords are identical.
			if ($password == $pass_confirm) {
				$this->PopuliApi->updateApiConfig($username, $password);
				log_message('debug', 'CONTROLLER: Save | api().  Settings updated.');
			} else {
				log_message('debug', 'CONTROLLER: Save | api().  The passwords were not identical');
			}
		} else {
			log_message('debug', 'CONTROLLER: Save | Please enter a new password before saving.');
		}

		// Redirect to the Inventory controller
		redirect('/inventory', 'refresh');

	}

}
