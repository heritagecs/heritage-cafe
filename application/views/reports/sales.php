<div id="" class="site-content-inner">

<?php
	// Set useful variables
	$start = $dates->start;
	$end = $dates->end;
	$kind = $dates->kind;
	$today = date('Y-m-d');
	// Set the previous and next parameters.
	$pStart = $dates->pStart;
	$pEnd = $dates->pEnd;
	$nStart = $dates->nStart;
	$nEnd = $dates->nEnd;
	// An array for the different kinds to loop over.
	$kinds = array('day', 'week', 'month', 'year');
	// Prepare the dates as unix timestamps.
	$timestampStart = nice_date($start);
	$timestampEnd = nice_date($end);

	switch ($kind) {
		case 'day':
			$kindDisplayDate = date('l d M', $timestampStart);
			break;
		case 'month':
			$kindDisplayDate = date('F Y', $timestampStart);
			break;
		case 'year':
			$kindDisplayDate = date('Y', $timestampStart);
			break;
		default:
			$kindDisplayDate = $start . ' to ' . $end;
	}


	// Log it.
	log_message('info', 'Sales report view, parameters: ' . $start . '/' . $end . '/' . $kind);

?>

<h1>Cafeteria Sales Report</h1>

<div id="page-top" class="reports-top">
	<div class="reports-timeframe">
		<?php
		$attr = array(
			'class'	=> 'btn btn-timeframe btn-backwards',
		);
		$url = base_url() . 'reports/sales/' . $pStart . '/' . $pEnd . '/' . $kind;
		echo anchor($url, 'Previous', $attr);

		$attr = array(
			'class'	=> 'btn btn-timeframe btn-forwards',
		);
		$url = base_url() . 'reports/sales/' . $nStart . '/' . $nEnd . '/' . $kind;
		echo anchor($url, 'Next', $attr);
		?>
		<span><?php echo '<span class="kind-heading">' . $kind . '</span>: ' . $kindDisplayDate; ?></span>
	</div>

	<div class="reports-kind">
		<select class="drop-down-kind btn" name="kind" onchange="location = this.value;">
			<?php
			// Loop through each kind and create an option item.  Also, figure out which is the current
			// one and select it.
			foreach ($kinds as $k) {
				// Calculate dates based on the kind.
				$dates = $this->Sales->calculateReportDates($today, $end, $k);

				// Open the Option list and loop through each kind.
				echo '<option ';
				if ($k == $kind) {
					echo 'selected="selected" ';
				}

				echo 'value="' . site_url() . 'reports/sales/' . $dates->start . '/' . $dates->end;
				echo '/' . $k . '">' . $k . '</option>';
			}

			?>
		</select>
	</div>
</div>


<?php

// Loop through the categories.
$categories = $this->InventoryCategory->getCategories();
foreach ($categories as $cat) {
  $categoryHasSales = false;
  // Loop the data once to see if this category has data.
  foreach ($salesData as $item) {
    if ($cat->id == $item->category_id && $item->qty > 0) {
      // This item belongs to this category, set the flag
      $categoryHasSales = true;
      break;
    }
  }

  // If the current category does not have sales skip to the next category.
  if ($categoryHasSales) {
    // This item belongs to this category, write the section header and the opening table tags
    echo '<section class="sales-data"><h2>' . $cat->category . '</h2>';
    echo '<table><thead><tr><th>Item</th><th>Qty Sold</th><th>Total</th><tr><thead><tbody>';
  } else {
    continue;
  }

	// Reset the category metrics.
	$categoryQty = 0;
	$categoryTotal = 0;
	log_message('info', 'Reset category (' . $cat->category . ') metrics to 0.');

  // Loop through the sales data, writing the items for this category.
  foreach ($salesData as $item) {
		// If this item's quantity is zero then don't process this item.
		if ($item->qty == 0) continue;

    $id = $item->inventory_id;
    $desc = $item->description;
    $catId = $item->category_id;
    $qty = $item->qty;
    $price = $this->Sales->getItemPrice($item->inventory_id);
    $total = $qty * $price;

    if ($cat->id == $catId) {
      echo '<tr>';
      echo '<td>' . $desc . '</td>';
      echo '<td>' . $qty . '</td>';
      echo '<td class="price">$' . $total . '</td>';
      echo '</tr>';

			// Sum up the quantity in this category.
			$categoryQty += $qty;
			$categoryTotal += $total;
			log_message('info', 'Item quantity = ' . $qty . ', $categoryQty = ' . $categoryQty);
			log_message('info', 'Item total = ' . $total . ', $categoryTotal = ' . $categoryTotal);
    }


  }

	echo '<tr class="table-summary"><td>Summary</td>';
	echo '<td>' . $categoryQty . '</td><td class="price">$' . number_format($categoryTotal, 2) . '</td></tr>';
  echo '</tbody>';
	echo '</table>';
  echo '</section>';
}

?>
</div><!-- .site-content-inner DIV -->
