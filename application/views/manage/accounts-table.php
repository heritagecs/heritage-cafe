<table>
  <thead>
    <tr>
      <th>Account</th>
      <th>Name</th>
      <?php
        if (STOREFRONT === 'CAFETERIA') echo '<th>Meal Plan</th>';
        echo '<th>Balance</th>';
      ?>     
      <th>myHeritage Balance</th>
    </tr>
  </thead>
  <tbody>
    <?php
      // Apply filters
      if ($filter_type) {
        $accountsFiltered = [];

        if (STOREFRONT === 'CAFETERIA') {
          foreach ($accounts as $a) {
            if ($a->type == $filter_type) array_push($accountsFiltered, $a);
          }
        } else if (STOREFRONT === 'COFFEESHOP') {
          foreach ($accounts as $a) {
            if ($a->coffeeshop_balance != 0) $accountsFiltered[] = $a;
          }
        }
          // Now replace the accounts list with the list of filtered accounts.
          $accounts = $accountsFiltered;
      }


      // Loop through each account.
      $count = 0;
      foreach ($accounts as $a) {
        // See if we are on the Reports view.
        $is_reports_view = strpos(uri_string(), 'reports');
        $is_mp = $a->is_mp_added;
        $type = $a->type;

        /*
          What accounts are we displaying?
          If the storefront is cafeteria and this is the reports view, don't show credit account, otherwise show accounts.
          If the storefront is coffeeshop and this is the report view, show all active accounts.
        */

        // If we are on the Reports tab and this account a credit account, don't display it.
        if (STOREFRONT === 'CAFETERIA') {
          if ($is_reports_view !== false) {
            if ($type == 'CREDIT') continue;
          }
        }


        // Get the balance of this account from Populi.
        //$populiBalance = number_format((double)$this->MealPlan->getBalance($a->person_id), 2);

        echo '<tr id="' . $a->person_id . '" class=account-details">';

        /*
          If we are under the Reports tab.
         */
        $icon_meal_plan = '<i style="margin-right: 0.0625em; font-size: 0.825em;" class="fas fa-utensils"></i>';
        $icon_pre_paid = '<i style="margin-right: 0.0625em; font-size: 0.825em;" class="fas fa-wine-glass-alt"></i>';
        $icon_staff = '<i style="margin-right: 0.0625em; font-size: 0.825em;" class="fas fa-user-friends"></i>';
        if ($is_reports_view !== false) {
          echo '<td>' . $a->person_id . '</td>';
          echo '<td>' . $a->first . ' ' . $a->last . '</td>';
        } else {
          // If we are under the Manage tab, create a link out of the ID to get further details about the account.
          echo '<td>';
          echo '<a href="' . site_url() . 'manage/accountdetails/' . $a->person_id . '">' . $a->person_id . '</a>';
          echo '</td>';
          // Active and Inactive checks.
          ($a->is_active) ? $isActiveIcon = 'fa-check-circle' : $isActiveIcon = 'fa-circle-thin';
          echo '<td>';
          echo '<i class="fa '. $isActiveIcon . '" aria-hidden="true"></i>&nbsp;' . $a->first . ' ' . $a->last . '</td>';
        }

        /*
          Prep and show the balance data which is different depending on the storefront.
         */
        // Show the meal plan data
        if (STOREFRONT === 'CAFETERIA') {
          // Prep the Meal Plan info.
          echo '<td>';
          if ($type == 'MEAL_PLAN') {
            echo $icon_meal_plan;
          } elseif ($type == 'PRE_PAID') {
            echo $icon_pre_paid;
          } elseif ($type == 'STAFF') {
            echo $icon_staff;
          }
          echo ' ' . $a->mp_name;
          if ($a->mp_name != 'None') {
            echo ' (<span class="price">$' . number_format($a->mp_starting_balance, 2) . '</span>)';
          }
          echo '</td>';

          // Prep and display the current balance and the Populi balance.
          echo '<td class="price">$' . number_format($a->mp_balance, 2) . '</td>';
          echo '<td class="price">$' . number_format($a->mp_balance_populi, 2) . '</td>';
        } else if (STOREFRONT === 'COFFEESHOP') {
          echo '<td class="price">$' . number_format($a->coffeeshop_balance, 2) . '</td>';
          echo '<td class="price">$' . number_format($a->coffeeshop_balance_populi, 2) . '</td>';
        }


        // Close the row.
        echo '</tr>';

        $count++;
      }

      log_message('debug', 'VIEW: accounts-table | Loaded ' . $count . ' meal plan, pre-paid, and staff accounts.');
    ?>
  </tbody>
</table>




<?php


?>