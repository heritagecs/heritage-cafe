<?php $this->load->view('parts/header'); ?>
<?php $this->load->view('nav/manage'); ?>

<!-- Start of the individual view -->

	<div id="" class="site-content-inner">

    <h1>Featured Items</h1>

    <section class="choose-featured-items">
      <table><thead><tr><th>Item Description</th><th></th></tr></thead>
        <tbody>
        <?php
        foreach ($featuredItems as $f) {
          $id = $f->id;
          $invId = $f->inventory_id;
          $desc = $f->description;

          echo '<tr>';
          echo '<td>' . $desc . '</td>';
          echo '<td class="featured-items-actions">';

					// Open the form.
					echo '<form class="action-buttons" action="' . base_url() . 'manage/changeFeaturedItem/' . $id . '" method="post">';
          // Create a drop down.
          $this->InventoryTable->createInventoryDropDown();
					// Submit button.
					echo form_submit('change-item', 'Change Item', array('class' => 'btn btn-primary btn-edit-featured-item'));

					// Edit an item's order in the list.
					if ($id != 1) {
						echo '<a class="btn btn-item-edit btn-edit-featured-item"';
						echo 'href="' . base_url() . 'manage/moveFeaturedItemUp/' . $id . '">&uarr;</a>';
					}

					if ($id != 8) {
						echo '<a class="btn btn-item-edit btn-edit-featured-item"';
						echo 'href="' . base_url() . 'manage/moveFeaturedItemDown/' . $id . '">&darr;</a>';
					}

					echo '</form>';
				  echo '</td>';
          echo '</tr>';

        }
        ?>
        </tbody>
      </table>
    </section>

	</div><!-- #view-dashboard DIV -->

<!-- End of the individual view -->

<?php $this->load->view('parts/footer'); ?>
