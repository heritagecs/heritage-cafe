<?php
/**
 * A PHP Class for negotiating sessions and data with the Populi database.
 */


class PopuliConnect extends CI_Model {

	public function __construct() {
		// Call the parent constructor
		parent::__construct();

		$this->load->model('common/Settings');

		// When the object is initialized, set the apiRateLimitCount and apiRequestCount to zero.
		$this->apiRateLimitCount = 0;
		$this->apiRequestCountTotal = 0;
	}


	/**
	 * Instance variables
	 */

	// This is the URL for the College site.
	protected $api_url = 'https://heritage.populiweb.com/api/index.php';



	/* A variables to hold the counts for numbers of requests to Populi. */
	public $apiRateLimitCount;
	public $apiRequestCount;

	/**
	 * Getters and setters for hte $apiRateLimitCount instance variable.
	 */
	public function getApiRateLimitCount() {
		return $this->apiRateLimitCount;
	}
	public function setApiRateLimitCount($count) {
		$this->apiRateLimitCount = $count;
		return $this->apiRateLimitCount;
	}
	public function increaseApiRateLimitCount($increase) {
		$this->apiRateLimitCount = $this->apiRateLimitCount + $increase;
		return $this->apiRateLimitCount;
	}

	/* Total request counts */
	public function getApiRequestCountTotal() {
		return $this->apiRequestCountTotal;
	}
	public function setApiRequestCountTotal($count) {
		$this->apiRequestCountTotal = $count;
		return $this->apiRequestCountTotal;
	}
	public function increaseApiRequestCountTotal($increase) {
		$this->apiRequestCountTotal = $this->apiRequestCountTotal + $increase;
		return $this->apiRequestCountTotal;
	}
	public function decreaseApiRequestCountTotal($decrease) {
		$this->apiRequestCountTotal = $this->apiRequestCountTotal - $decrease;
		return $this->apiRequestCountTotal;
	}






	/**
	* Goes out to Populi using the username and password specified in the apiConfig object and gather's a new API token.
	*
	* @param  [type] $username [Populi username, usually an email address]
	* @param  [type] $password [The user's password]
	* @return	[type]		Returns a valid api key or -1 if there was a failure.
	*/
	public function getApiToken($username, $password) {
		log_message('debug', 'MODEL: PopuliConnect | Getting a new API token from Populi.');

		// Url encode the username and password parameters.
		$params = 'username=' . urlencode($username) . '&password=' . urlencode($password);

		// Set the api key to a default value.
		$apiToken = -1;

		// Initialize Curl
		// Place the results into an XML string. We can't use file_get_contents
		// since it randomly fails... so we now use curl
		$curl = curl_init();
		// Set curl options
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_DNS_USE_GLOBAL_CACHE,false);
		curl_setopt($curl, CURLOPT_FRESH_CONNECT,true);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
		curl_setopt($curl, CURLOPT_LOW_SPEED_LIMIT, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_VERBOSE, 1);
		curl_setopt($curl, CURLOPT_LOW_SPEED_TIME, 60);
		curl_setopt($curl, CURLOPT_URL, $this->api_url);
		// Execute and retrieve data.
		$response = curl_exec($curl);
		// Close the handle
		curl_close($curl);

		// If there was a positive response.
		if ($response !== false) {
			// Use SimpleXML to put results into Simple XML object (requires PHP5)
			$xml = new SimpleXMLElement($response);
			if (isset($xml->access_key)) {
				// Update the api_token to the new value.
				$apiToken = (string)$xml->access_key;
			} else {
				throw new PopuliException("Oops! Please enter a valid username/password.", 'AUTHENTICATION_ERROR');
			}
		} else {
			// If there was no response from the server.
			throw new PopuliException("Oops! We're having trouble connecting to Populi right now... please try again later.", 'CONNECTION_ERROR');
		}

		// Also return the token to the caller.
		return $apiToken;
	}























	// By default, we'll attempt to parse the response into a SimpleXML object
	// and return that.
	// For certain tasks, though, (like downloadFile), you'll want the raw
	// response and will need to set return_raw to true.
	/**
	 * [doTask performs a function/task from the Populi Developer API
	 * (https://support.populiweb.com/entries/20218516-API-Reference).]
	 * @param  [type] $task   [One of the task names listed in the developer API.]
	 * @param  array  $params [The paramters for that task.]
	 * @param  string $returnType		[specify either 'json' or 'xml'.  The default is json]
	 * @return [type]         [A SimpleXMLElement object if gather data was successful.]
	 */
	public function doTask($task, $params = array(), $return_raw = false, $returnType = 'json') {
		log_message('info', 'MODEL: PopuliConnect | Connecting to Populi to run ' . $task);

		// Get the value of thee api token out of the database.
		$apiToken = $this->Settings->getSettingValue('api_token');

		// Put the task and access key together to begin forming parameters.
		$post = 'task=' . urlencode($task) . '&access_key=' . $apiToken;

		// Add other parameter items.
		foreach ($params as $param => $value) {
			if (is_array($value)) {
				// If the value of this parameter is itself an array.
				foreach ($value as $array_value) {
					$post .= '&' . $param . '[]=' . urlencode($array_value);
				}
			} else {
				$post .= "&$param=" . urlencode($value);
			}
		}

		// Place the results into an XML string. We can't use file_get_contents
		// since it randomly fails... so we now use curl
		// Initialize Curl
		log_message('info', 'MODEL: PopuliConnect | XML post string we are sending to Populi: ' . $post);
		$curl = curl_init();
		// Set curl options
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_URL, $this->api_url);
		// Execute and retrieve data.
		$response = curl_exec($curl);
		// Close the handle
		curl_close($curl);

		// Increase the counters.
		$this->increaseApiRateLimitCount(1);
		$this->increaseApiRequestCountTotal(1);

		// If the rate limit counter gets to 100, reset it, and sleep.
		if ($this->getApiRateLimitCount() >= 100) {
			log_message('debug', $this->getApiRateLimitCount());
			log_message('debug', 'MODEL: PopuliConnect | API rate limit counter RESET');
			$this->setApiRateLimitCount(0);
			sleep(60);
			log_message('debug', 'MODEL: PopuliConnect | API rate limit sleep OVER');
		}

		// If we were able to successfully connect to Populi.
		if ($curl !== false) {
			if ($return_raw) {
				return $response;
			} else {
				// Use Simple XML to put results into Simple XML object (requires PHP5)
				try {
					$xml = new SimpleXMLElement($response);
				} catch(Exception $e) {
					echo htmlentities($response) . '<br><br><br>';
					throw new PopuliException('Problem parsing the XML response: ' . $e->getMessage());
				}

				// If there was a positive response.
				if ($xml->getName() == 'response') {
					if ($returnType == 'json') {
						// Encode the returned value as a json string.
						return json_encode($xml);
					} else {
						return $xml;
					}
				} else if ($xml->getName() == 'error') {
					// If there was an error.
					throw new PopuliException((string)$xml->message, (string)$xml->code);
				} else {
					// Woah - response or error should always be the root element
					throw new PopuliException('Problem parsing the XML response: invalid root element.');
				}
			}
		} else {
			throw new PopuliException('Could not connect to Populi.', 'CONNECTION_ERROR');
		}
	}






}




































/**
 * Secondary class for creating and communicating exceptions.
 */
class PopuliException extends Exception {
	/**************************************************************************************************
	 *	We have our own variable since we don't feel like using numeric error codes
	 *	Should be one of:
	 *		AUTHENTICATION_ERROR - Couldn't login to the API (bad username/password)
	 *		BAD_PARAMETER - You called a task using parameters it didn't like
	 *		CONNECTION_ERROR - Thrown if we can't connect to Populi
	 *		LOCKED_OUT - Your user account is blocked (too many failed login attempts?)
	 *		OTHER_ERROR - Default generic error
	 *		PERMISSIONS_ERROR - You aren't allowed to call that task with those parameters
	 *		UKNOWN_TASK - You tried to call an API task that doesn't exist
	****************************************************************************************************/
	/* Instance variables */
	public $populi_code = null;

	/* Constructor */
	public function __construct($message, $populi_code = 'OTHER_ERROR') {
		parent::__construct($message);
		$this->populi_code = $populi_code;
	}

	/* Instance methods */
	public function getPopuliCode() {
		return $this->populi_code;
	}
}





?>
