<?php

// Load the header
$this->load->view('parts/header');

// Look for a backslash in the content view name.
$lastSegmentPosition = strrpos($content_inner_view, '/');

if ($lastSegmentPosition == false) {
  // If no backslash, this is the page name
  $pageName = $content_inner_view;
} else {
  // If found, get the last segment, this will be the page name.
  $pageName = substr($content_inner_view, $lastSegmentPosition + 1);
}

// Open the site-content div and apply an id that is equal to the pageName.
echo '<div id="' . $pageName . '" class="site-content-inner ' . $pageName . '">';

// Load the view.
$this->load->view($content_inner_view);

// Close the site-content div.
echo '</div>';

// Load the footer.
$this->load->view('parts/footer');
