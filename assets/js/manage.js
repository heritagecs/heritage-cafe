/**
 * Manage.js
 * Created: 2017-04-24
 * Author: Russ Shouldice
 *
 * Description: Javascript related to any of the Management views.
 */




/**
 *
 * When the document is loaded, run this stuff.
 *
 */
$( document ).ready( function() {



  // For any input fields that are required, tell the user they are required.
  $('input[required], select[required]').before('<span class="form-label-required text-small">(required)</span>');





});






/**
 * Shows the credit transaction form
 */
function toggleFormAddTransaction() {
  console.log('manage.js | toggleFormAddTransaction()');
  var form = document.querySelector('.form-add-transaction');
  var button = document.querySelector('.btn-add-transaction');
  var buttonTransferBalance = document.querySelector('.btn-transfer-balance');
  var firstInput = document.getElementById('form-add-transaction-amount');

  // Toggle the visibility state of the form
  form.classList.toggle('hidden');
  form.reset();
  button.classList.toggle('hidden');
  buttonTransferBalance.classList.toggle('hidden');
  firstInput.focus();

  // Jump to the form.
  var accountActionsLocation = document.getElementById('account-actions').offsetTop;
  window.scrollTo(0, accountActionsLocation - 18);
}




/**
 * Shows the transfer balance form.
 */
function toggleFormTransferBalance() {
  console.log('manage.js | toggleFormCreditTransaction()');
  var form = document.querySelector('.form-transfer-balance');
  var button = document.querySelector('.btn-transfer-balance');
  var buttonCreditAccount = document.querySelector('.btn-credit-account');
  var firstInput = document.getElementById('form-transfer-balance-amount');

  // Toggle the visibility state of the form
  form.classList.toggle('hidden');
  form.reset();
  button.classList.toggle('hidden');
  buttonCreditAccount.classList.toggle('hidden');
  firstInput.focus();

  // Jump to the form.
  var accountActionsLocation = document.getElementById('account-actions').offsetTop;
  window.scrollTo(0, accountActionsLocation - 18);
}













/**
 * Form validation for the credit account transaction.
 */

// Get some important elements.
var formAddTransaction = new Object();
formAddTransaction.form = document.getElementById('form-add-transaction');
formAddTransaction.amountField = document.getElementById('form-add-transaction-amount');
formAddTransaction.amountError = document.querySelector('#form-add-transaction-amount + .input-error-message');
formAddTransaction.dateField = document.getElementById('form-add-transaction-date');
formAddTransaction.dateError = document.querySelector('#form-add-transaction-transaction-date + .input-error-message');

formAddTransaction.amountField.addEventListener("keyup", function(event) {
  // Each time the user types something, we check if the amount field is valid.
  if (formAddTransaction.amountField.validity.valid) {
    // In case there is an error message, we remove the error message.
    formAddTransaction.amountField.className = "";
    formAddTransaction.amountError.innerHTML = "";
    formAddTransaction.amountError.className = "input-error-message";
  }
}, false);

formAddTransaction.dateField.addEventListener("keyup", function(event) {
  // Check the date field every time the user types something.
  if (formAddTransaction.dateField.validity.valid) {
    // If the date field is presently valid, make sure to remove any error messages.
    formAddTransaction.dateField.className  = "";
    formAddTransaction.dateError.innerHTML = "";
    formAddTransaction.dateError.className = "input-error-message";
  }
}, false);

// Validation when the form is submitted.
formAddTransaction.form.addEventListener("submit", function(event) {
  console.log('manage.js | Form validation for add transaction.');
  // Each time the user tries to send the data, we check if the amount field is valid.
  if (!formAddTransaction.amountField.validity.valid) {
    // If the amount field is not valid, display a custom error message.
    formAddTransaction.amountField.className = "input-error-field";
    formAddTransaction.amountError.innerHTML = "Enter a positive dollar price.";
    formAddTransaction.amountError.className = "input-error-message active";
    // And we prevent the form from being send by canceling the event.
    event.preventDefault();
  }

  // Check the validity of the date field.
  if (!formAddTransaction.dateField.validity.valid) {
    formAddTransaction.dateField.className = "input-error-field";
    formAddTransaction.dateError.innerHTML = "Enter a valid date.";
    formAddTransaction.dateError.className = "input-error-message active";
    // And we prevent the form from being send by canceling the event.
    event.preventDefault();
  }
}, false);

















/**
 * Form validation for the transfer balance transaction.
 */

// Create a new object for the transfer balance form.
var formTransferBalance = new Object();
formTransferBalance.form        = document.getElementById('form-transfer-balance');
formTransferBalance.amountField = document.getElementById('form-transfer-balance-amount');
formTransferBalance.amountError = document.querySelector('#form-transfer-balance-amount + .input-error-message');
formTransferBalance.dateField   = document.getElementById('form-transfer-balance-transaction-date');
formTransferBalance.dateError   = document.querySelector('#form-transfer-balance-transaction-date + .input-error-message');
formTransferBalance.recipientField  = document.getElementById('form-transfer-balance-recipient-account');
formTransferBalance.recipientError  = document.querySelector('#form-transfer-balance-recipient-account + .input-error-message');

formTransferBalance.amountField.addEventListener("keyup", function(event) {
  // Each time the user types something, we check if the amount field is valid.
  if (formTransferBalance.amountField.validity.valid) {
    // In case there is an error message, we remove the error message.
    formTransferBalance.amountField.className = "";
    formTransferBalance.amountError.innerHTML = "";
    formTransferBalance.amountError.className = "input-error-message";
  }
}, false);

formTransferBalance.dateField.addEventListener("keyup", function(event) {
  // Check the date field every time the user types something.
  if (formTransferBalance.dateField.validity.valid) {
    // If the date field is presently valid, make sure to remove any error messages.
    formTransferBalance.dateField.className  = "";
    formTransferBalance.dateError.innerHTML = "";
    formTransferBalance.dateError.className = "input-error-message";
  }
}, false);

// Validation when the form is submitted.
formTransferBalance.form.addEventListener("submit", function(event) {
  console.log('manage.js | Submitting the transfer balance form.');
  // Each time the user tries to send the data, we check if the amount field is valid.
  if (!formTransferBalance.amountField.validity.valid) {
    // If the amount field is not valid, display a custom error message.
    formTransferBalance.amountField.className = "input-error-field";
    formTransferBalance.amountError.innerHTML = "Enter a positive dollar price.";
    formTransferBalance.amountError.className = "input-error-message active";
    // And we prevent the form from being send by canceling the event.
    event.preventDefault();
  }

  // Check the validity of the date field.
  if (!formTransferBalance.dateField.validity.valid) {
    formTransferBalance.dateField.className = "input-error-field";
    formTransferBalance.dateError.innerHTML = "Enter a valid date.";
    formTransferBalance.dateError.className = "input-error-message active";
    // And we prevent the form from being send by canceling the event.
    event.preventDefault();
  }

  // Check the validity of the recipient field.
  if (formTransferBalance.recipientField.options[formTransferBalance.recipientField.selectedIndex].value <= 0) {
    formTransferBalance.recipientField.className = 'input-error-field';
    formTransferBalance.recipientError.innerHTML = 'Select an account to transfer the amount to.';
    // And we prevent the form from being send by canceling the event.
    event.preventDefault();
  }

}, false);
