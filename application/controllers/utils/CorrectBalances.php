<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CorrectBalances extends CI_Controller {

  public function __construct() {
    // Call the parent constructor
		parent::__construct();

    // Load up the appropriate models.
    $this->load->model('User');
    $this->load->model('Role');
    $this->load->model('MealPlan');
    $this->load->model('populi/PopuliApi');
    $this->load->model('Account');
    $this->load->model('Transaction');
    $this->load->model('Sync');
    $this->load->model('InventoryTable');
    $this->load->model('Sales');
    $this->load->model('MealPlan');
    $this->load->model('AcademicTerms');
    $this->load->model('PaymentType');
    $this->load->model('ItemsSold');
    $this->load->model('common/Settings');
  }

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index() {
        log_message('debug', '');
        log_message('debug', '==================================');
        log_message('debug', '-----> CONTROLLER: CorrectBalances');
        log_message('debug', '-----> CONTROLLER: Correct balances of meal plan accounts where the semester meal plans were accidentally added again on October 21st, 2020.');
        log_message('debug', '==================================');

    
        $this->load->view('parts/header-test');

        // Only folks from this machine can run this utility.
        $adminIP = array('10.0.1.7', '::1');

        // Execute the update.
        $transactions = array('108041','108042','108043','108044','108045','108046','108047','108048','108049','108050','108051','108052','108053','108054','108055','108056','108057','108058','108059','108060','108061','108062','108063','108064','108065','108066','108067','108068','108069','108070','108071','108072','108073','108074','108075','108076','108077','108078','108079','108080','108081','108082','108083','108084','108085','108086','108087','108088');
        $reportData = array(
            'no_transactions'   => sizeof($transactions),
            'counter_transactions_processed' => 0,
            'transactions_processed_ids' => [],
        );


        // If this is an administrator machine (like Russ')
        if (in_array($this->input->ip_address(), $adminIP)) {

            $count = 0;
            foreach ($transactions as $trId) {
                $count++;
                log_message('debug', 'CONTROLLER: Manage | ' . $count . '. Working on account ' . $trId);

                // Void the transaction
                $result = $this->Transaction->voidTransaction($trId);

                // Save the results for our report.
                if ($result) {
                    $reportData['counter_transactions_processed']++;
                    $reportData['transactions_processed_ids'][] = $trId;
                }

                log_message('debug', 'CONTROLLER: Manage |');   
            }

            log_message('debug', 'CONTROLLER: Manage | No. of transactions to process: ' . $reportData['no_transactions']);
            log_message('debug', 'CONTROLLER: Manage | No. of transactions to processed: ' . $reportData['counter_transactions_processed']);
            log_message('debug', 'CONTROLLER: Manage | Transaction IDs: ' . implode(',', $reportData['transactions_processed_ids']));
    
            log_message('debug', '===========================================');
            log_message('debug', '-----> CONTROLLER: CorrectBalances Complete');
            log_message('debug', '===========================================');
            log_message('debug', '');
    
            
            /*
                Display the data returned by the test function.
                */
            echo 'RETURNED TYPE: ' . gettype($reportData) . '<br />';
            if (is_array($reportData)) {
                echo 'Yep, that was an array... ' . count($reportData) . ' records returned.';
                echo '<br />';
                echo '<br />';
                foreach ($reportData as $d) {
                echo var_dump($d);
                echo '<br />';
                echo '<br />';
                }
            } else {
                echo 'Only one element was returned.';
                echo '<br />';
                echo '<br />';
                var_dump($reportData);
            }

        } else {
            // If this is not an adminstrator machine
            echo 'Utilities disabled.';
        }




		$this->load->view('parts/footer');
	}



}
