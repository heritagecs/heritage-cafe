<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ItemsSold extends CI_Model {

  public function __construct() {
    // Call the parent constructor
		parent::__construct();

    // Load up the appropriate models.

  }




















  public function getItemsSold($start, $end, $options = null) {
    log_message('debug', 'MODEL: ItemsSold | getItemsSold()');

    // Add time stamps to the dates.
    $start = appendTimeToDate($start);
    $end = appendTimeToDate($end, true);

    // Query the database
    $this->db->where('date >=', $start);
    $this->db->where('date <=', $end);

    // Check for options in the query.
    if (!is_null($options)) {
      if(isset($options['returned_items']) && $options['returned_items']) {
        $this->db->where('price <', 0);
      }
    }

    // Run the query.
    $query = $this->db->get('items_sold');

    // If the first row return by the query has data, return that data.
    ($query->result()) ? $result = $query->result() : $result = false;
		if (!$result) log_message('error', 'MODEL: ItemsSold | No records found.');
		return $result;
  }








  public function updateItemsSoldDate($transId) {
    log_message('debug', 'MODEL: ItemsSold | updateItemsSoldDate()');

    // Get the transaction's date.
    $trans = $this->Transaction->getTransaction($transId);

    // Setup the query.
    $this->db->where('transaction_id', $transId);

    // Execute the query.
    $result = $this->db->update('items_sold', array('date' => $trans->date));

    // Return the result.
    return $result;
  }





























}
