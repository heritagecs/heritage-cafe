<?php
// Set the version.
$versionObject = json_decode(read_file('./application/version.json'));
$version = $versionObject->version . '.' . $versionObject->feature;
if ((int)$versionObject->build != 0) $version .= '.' . $versionObject->build;

// Check for the dev flag.
(ENVIRONMENT === 'development') ? $devFlag = true : $devFlag = false;
(ENVIRONMENT === 'testing') ? $testFlag = true : $testFlag = false;

// Get the branch name from the git head file.
if ($devFlag) {
	$file = explode('/', file_get_contents('.git/HEAD'), 3);
	$branch = trim($file[2]);
} else {
	($testFlag) ? $branch = 'testing' : $branch = 'production';
}

// Tell the logs what version this is regardless of the environment.
log_message('debug', 'VIEW: footer | Branch: ' . $branch);
log_message('debug', 'VIEW: footer | Application Version: ' . $version);
?>

<!-- Start of Footer -->

	</div><!-- #container DIV -->

  <footer class="site-footer">
		<div id="div-footer" class="div-footer">
			<div class="dev-stats">
				<p class="footer">
					<?php
						echo ($devFlag || $testFlag) ? 'Page rendered in <strong>{elapsed_time}</strong> seconds. ' : '';
						echo ($devFlag) ? 'CodeIgniter Version <strong>' . CI_VERSION . '.</strong> ' : '';
					?>
				</p>
			</div>
			<div class="dev-flags">
				<?php
					if ($devFlag || $testFlag) {
						// Create some text for displaying.
						echo '<p class="flag in-dev">';
						echo ($devFlag) ? 'In Development' : 'Testing';

						// If this is master branch, show the version number, otherwise, show the branch name.
						if ($branch == 'master') {
							echo ' | ' . $version . '</p>';
						} else if ($testFlag) {
							echo '</p>';
						} else {
							echo ' | ' . $branch . '</p>';
						}

					} else {
						// If not in development or testing add a hidden version tag.
						echo '<p class="flag in-production">&copy; 2018 Heritage College and Seminary | ' . $version . '</p>';
					}
				?>
			</div>
		</div>
  </footer>

</div><!-- #page wrapper div with the class site -->
</body>
</html>
