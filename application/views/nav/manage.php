<?php

// Get the boolean from the session variable.
$is_admin = $this->session->is_admin;

?>

<nav id="hcs-secondary-navigation" class="hcs-secondary-navigation hcs-navigation" role="navigation">
  <ul class="unstyled-list">
  <?php // Check if this person is a manager or admin.
    if ($this->Role->validateAccess(array('admin', 'manager'))) { ?>
      <li class="manage-accounts"><a href="<?php base_url(); ?>/manage/accounts">Accounts</a></li>
      <li class="inventory"><a href="<?php base_url(); ?>/manage/inventory">Inventory</a></li>
      <li class="manage-featured"><a href="<?php base_url(); ?>/manage/featured">Featured Items</a></li>
  <?php } ?>

  </ul>
</nav>
