<?php
// Get the storefront from the cookie.
$this->load->helper('cookie');
define('STOREFRONT', get_cookie('storefront'));


// Prep the data depending on the storefront
// A little counting.
$mealPlanCount = 0;
$prePaidCount = 0;
$staffCount = 0;

if (STOREFRONT === 'CAFETERIA') {
	foreach($accounts as $a) {
		if ($a->is_mp_added == 1) $mealPlanCount++;
		if ($a->is_mp_added == 0 && $a->mp_balance > 0) $prePaidCount++;
		if ($a->type == 'STAFF') $staffCount++;	
	}
	$totalAccountsCount = $mealPlanCount + $prePaidCount + $staffCount;
} else if (STOREFRONT === 'COFFEESHOP') {
	$totalAccountsCount = sizeof($accounts);
	$coffeeShopTabsInUse = 0;
	foreach ($accounts as $a) {
		if ($a->coffeeshop_balance != 0) $coffeeShopTabsInUse++;
	}
}

?>


<div id="" class="site-content-inner">

  <?php
	if (STOREFRONT == 'CAFETERIA') $sfDisplay = 'Lily Ruth Catering';
	if (STOREFRONT == 'COFFEESHOP') $sfDisplay = 'The Bean';
	
	// Display the title
	echo '<h1>Accounts Report - ' . $sfDisplay . ' </h1>';
  ?>

	<?php
	// Taylor 

	?>


	<section class="local-account-stats">
		<ul class="no-bullets">
			<li>Total Accounts : <code><?php echo $totalAccountsCount; ?></code></li>
			<?php
			// Conditional logic based on the storefront.
			if (STOREFRONT === 'CAFETERIA') {
				echo '<li>Heritage Meal Plans (<i style="font-size: 0.825em;" class="fas fa-utensils"></i>): ';
				echo '<code>' . $countMealPlanAccounts . '</code></li>';
				echo '<li>Lily Ruth Pre-Paid (<i style="font-size: 0.825em;" class="fas fa-wine-glass-alt"></i>): ';
				echo '<code>' . $prePaidCount . '</code></li>';
				echo '<li>Staff & Faculty (<i style="font-size: 0.825em;" class="fas fa-user-friends"></i>): ';
				echo '<code>' . $staffCount . '</code></li>';
			} else if (STOREFRONT === 'COFFEESHOP') {
				echo '<li>Tabs in Use (<i style="font-size: 0.825em;" class="far fa-money-check-edit"></i>): ';
				echo '<code>' . $coffeeShopTabsInUse . '</code></li>';
			}


			?>
		</ul>
	</section>

	<?php
		// A little block that shows the number of active accounts and the filter.
		echo '<section>';

		// Select the filter
		echo '<span class="text-small">Select Filter: ';
		if (STOREFRONT === 'CAFETERIA') {
			echo '<a href="/reports/accounts?type=MEAL_PLAN">Heritage Meal Plans</a> | ';
			echo '<a href="/reports/accounts?type=PRE_PAID">Lily Ruth Pre-Paid</a> | ';
			echo '<a href="/reports/accounts?type=STAFF">Staff & Faculty</a>';
		} else if (STOREFRONT === 'COFFEESHOP') {
			echo '<a href="/reports/accounts?type=ONLY_TABS">Only accounts with tabs</a>';			
		}
		echo '</span>';
		echo '<br />';

		// Display the current filer
		($filter_type) ? $filterMeta = $filter_type : $filterMeta = 'None';
		echo '<span>Current Filter: <strong>' . $filterMeta . '</strong></span>';
		if ($filter_type) echo '<a class="text-small" style="margin-left: 0.45em;" href="/reports/accounts"><i class="fad fa-times-circle"></i></a>';
		echo '</section>';

		
	?>


	

  <section class="reports-accounts">
		<?php $this->load->view('manage/accounts-table'); ?>
	</section>
</div><!-- .site-content-inner DIV -->
