<?php
/**
 * A PHP Class for caching data that largely remains static in our local database
 */


class PopuliApiCache extends CI_Model {

	public function __construct() {
		// Call the parent constructor
		parent::__construct();

		$this->load->model('populi/PopuliApi');
		//$this->load->model('CacheAcademicYear');
		//$this->load->model('CacheTagList');


	}

















	/**
	 * Looks up the list of academic years in Populi and compares it with what we are storing locally and
	 * updates the data as needed.
	 * @return boolean			Were we successful in updating or not.
	 */
	public function cacheAcademicYears() {
		log_message('debug', 'MODEL: PopuliApiCache() | UPDATING the local cache of academic years.');

		$academicYears = $this->PopuliApi->getAcademicYears();

		// Loop over the list of degrees and decide what to do with it.
		foreach ($academicYears as $year) {

			// Prep the data.
			$data = array(
				'start_year'	=> (int)$year->start_year,
				'end_year'		=> (int)$year->end_year,
			);

			// See if there is already a cached year in the database.
			$cachedYear = $this->CacheAcademicYear->getAcademicYear($year->yearid);

			// Do we update or insert?
			if ($cachedYear) {
				// Update the record only if something has changed.
				if ($cachedYear->start_year != $year->start_year || $cachedYear->end_year != $year->end_year) {
					log_message('info', 'MODEL: PopuliApiCache | Update the already cached year ' . $year->yearid . '.');
					// Push the update.
					$this->db->where('year_id', $year->yearid);
					$this->db->update('cache_academic_year', $data);
					$message = 'UPDATED';
				} else {
					log_message('info', 'MODEL: PopuliApiCache | Actually...nothing changed about year id ' . $year->yearid . ' so no update is needed.');
					$result = true;
					continue;
				}
			} else {
				log_message('info', 'MODEL: PopuliApiCache | The year with id ' . $year->yearid . ' has not been cached...lets do that!');
				// Add a couple things and insert a new record.
				$data['year_id'] = (int)$year->yearid;
				$data['ts_create'] = getSqlDateTime();
				$this->db->insert('cache_academic_year', $data);

				$message = 'CREATED';
			}

			$cachedYear = $this->CacheAcademicYear->getAcademicYear($year->yearid);
			if ($cachedYear->start_year == $year->start_year && $cachedYear->end_year == $year->end_year) {
				log_message('debug', 'MODEL: PopuliApiCache | The cached academic year (' . $year->yearid . ') record was ' . $message . ' successfully.');
				$result = true;
			} else {
				log_message('debug', 'MODEL: PopuliApiCache | The Cached academic year (' . $year->yearid . ') record failed to be ' . $message . '.');
				$result = false;
			}
		}

		// Return the result.
		return $result;
	}









	public function cacheAcademicTerms() {
		log_message('debug', 'MODEL: PopuliApiCache | cacheAcademicTerms()');

		// Get a list of all the academic terms.
		$data = json_decode($this->PopuliConnect->doTask('getAcademicTerms'));

		// Loop through the academic terms.
		foreach($data->academic_term as $term) {
			$t = json_decode(json_encode($term));

			// Prepare the data.
			$data = array(
				'term_id'		=> $t->termid,
				'name'			=> $t->name,
				'fullname'	=> $t->fullname,
				'start_date'	=> $t->start_date,
				'end_date'		=> $t->end_date,
				'year_id'			=> $t->yearid,
				'start_year'	=> $t->start_year,
				'end_year'		=> $t->end_year,
			);

			if (!$this->getCachedTerm($t->termid)) {
				// Insert.
				log_message('debug', 'cacheAcademicTerms() Inserting new term with id ' . $t->termid);
	      // Insert the data into the database.
	      $this->db->insert('cache_academic_terms', $data);
			} else {
				// Update.
				log_message('info', 'cacheAcademicTerms() Updating existing term with id ' . $t->termid);
        // Perform the update.
        $this->db->where('id', $t->termid);
        $this->db->update('cache_academic_terms', $data);
			}
		}
	}




	/**
	 * Looks in the local database for the cached term information.
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getCachedTerm($id) {
		log_message('info', 'getCachedTerm() $id = ' . $id);

    // Query the database
    $this->db->where('term_id', $id);
    $query = $this->db->get('cache_academic_terms');

    // Get the row.
    return $query->row();
	}


















	/**
	 * Checks Populi for the current year and makes a note of it in the local database.
	 */
	public function cacheCurrentYearId() {
		log_message('debug', 'MODEL: PopuliApiCache | cacheCurrentYearId()');
		// Get the current term from populi.
		$currentTerm = $this->PopuliApi->getCurrentAcademicTerm();
		// Update the setting in the database.
		$result = $this->Settings->setSettingValue('current_academic_year_id', $currentTerm->yearid);
		// Return the result.
		return $result;
	}
	public function getCurrentYearId() {
		// Get the current year id from settings.
		$yearId = $this->Settings->getSettingValue('current_academic_year_id');
		// Return the value as an int.
		return (int)$yearId;
	}




	/**
	 * Cache the current academic term id that Populi is aware of.
	 */
	public function cacheCurrentTermId() {
		log_message('debug', 'MODEL: PopuliApiCache | cacheCurrentTermId()');
		// Get the current term from populi.
		$currentTerm = $this->PopuliApi->getCurrentAcademicTerm();
		// Update the setting in the database.
		$result = $this->Settings->setSettingValue('current_term_id', $currentTerm->termid);
		// Return the result.
		return $result;
	}
	public function getCurrentTermId() {
		// Get the current year id from settings.
		$termId = $this->Settings->getSettingValue('current_term_id');
		// Return the value as an int.
		return (int)$termId;
	}








	/**
	 * Cache the list of person ids in a tag list of people
	 */
	public function cacheTaggedPeople($tagId) {
		log_message('debug', 'MODEL: PopuliApiCache | cacheTaggedPeople()');
		// Validate that the tag id has been specified.
		if (!$tagId) {
			log_message('error', 'MODEL: PopuliApiCache | Please specify a tag id.');
			return false;
		}
		// Make calls to Populi to get the list of tags and the list of people for the tag in question.
		$tags = $this->PopuliApi->getTags();
		$taggedPeople = $this->PopuliApi->getTaggedPeople($tagId);

		// Loop through the array of tagged people and make an array of the person ids.
		$peopleIds = array();
		foreach ($taggedPeople as $p) {
			array_push($peopleIds, $p->person_id);
		}

		// Create a new CSV list of the people ids.
		$peopleIds = implode(',',$peopleIds);

		// Loop through the list of tags and find the name for our tag.
		foreach ($tags as $t) {
			if ($t->id == $tagId) $tagName = $t->name;
		}

		// Update the database with this list.
		$result = $this->CacheTagList->setTagList($tagId, $tagName, $peopleIds);
		// The result of this function is the reuslt of what happened when we interacted with the database.
		return $result;
	}

















	/**
	 * Save the data about all the folks currently on a Meal Plan into the database cache.
	 */
	public function cachePeopleOnMealPlans() {
		log_message('debug', 'MODEL: MealPlan | cachePeopleOnMealPlans()');

		// Go get the bulk data from Populi.
		$termId = $this->PopuliApiCache->getCurrentTermId();
		
		// Get the raw data from Populi.
		$people = $this->PopuliApi->getTermBillingInfo($termId);
		
		// Remove all data from this table first so we only add folks who have meal plans.  This table must be
		// an exact replica of the data in myHeritage.
		$this->db->truncate('cache_term_meal_plans');

		/*
		* Prepare the data to be inserted into the database.
		*/
		// Add data to the array.
		// Only add data for if there is a meal plan for this person.
		$mealPlanData = false;
		foreach ($people as $p) {
			$pId = (int)$p->person_id;
			// If the meal plan ID has data, then add or update this data in the database.
			if (!empty((array)$p->meal_plan_id)) {	
				// Prepare the data.
				$data = array(
					'person_id'		=> $pId,
					'term_id'		=> $termId,
					'mp_id'			=> (int)$p->meal_plan_id,
					'mp_name'		=> (string)$p->meal_plan_name,
				);
				
				// Update or add the record to the database.
				$mealPlanData[] = $this->MealPlan->setCachedMealPlanByPerson((Object)$data);
			}
		}

		// Return the list of folks on a meal plan.
		return $mealPlanData;
	}

	
	/**
	 * A set of functions to supporting the caching or roles per person, updating those roles,
	 * and removing roles when they no longer apply.
	 */
	// Returns an array of roles for this person.
	public function cacheGetRolesByPersonId($personId) {
		log_message('info', 'MODEL: PopuliApiCache | cacheGetRolesByPersonId()');

		$this->db->where('person_id', $personId);
		$query = $this->db->get('cache_role_members');

		return ($query->row()) ? json_decode($query->row()->roles) : false;
	}
	public function getRoleMembers($role = null) {
		log_message('info', 'MODEL: PopuliApiCache | cacheGetRoleMembers()');

		// Make this capital case so we can properly do comparisons.
		$role = ucwords($role);
		// Get all the records from the database.
		$query = $this->db->get('cache_role_members');
		
		// If there are results, loop over the results a build a list of person ids
		if ($query->result()) {
			$members = [];
			foreach ($query->result() as $row) {
				// Turn the list of current roles into a lower-case array of strings.
				$currentRoles = json_decode($row->roles);
				// If the role we are looking for is in the list of roles for this person.
				if (in_array($role, $currentRoles)) $members[] = (int)$row->person_id;
			}
			// Return the list of members.
			return $members;
		} else {
			return false;
		}
	}
	public function cacheRoleMembers($role) {
		// Make this capital case so we can properly do comparisons.
		$role = ucwords($role);
		$acceptableRoles = array('Staff', 'Student', 'Faculty');

		// Some logging.
		log_message('debug', 'MODEL: PopuliApiCache | cacheRoleMembers( ' . $role . ' )');
		
		// At the moment we only cache certain roles.
		if (!in_array($role, $acceptableRoles)) {
			log_message('debug', 'MODEL: PopuliApiCache | We do not cache the role ' . $role);
			return false;
		}

		// If we're asking about the staff role, use the special function for that which removes the API users.
		if ($role == 'Staff') {
			$newMembers = $this->PopuliApiExtended->getStaffActive();
		} else {
			$newMembers = $this->PopuliApi->getRoleMembers($role);
		}

		$cachedMembers = $this->getRoleMembers($role);
		($cachedMembers) ? $countRoleMembersInit = sizeof($cachedMembers) : $countRoleMembersInit = 0;

		/* Add or update roles for new or existing members */
		foreach ($newMembers as $mem) {
			// Get some important data
			$pId = $mem->person_id;
			$roleCache = $this->cacheGetRolesByPersonId($pId);
			
			// Add person ID to the array of data for this person
			$data['person_id'] = (int)$pId;

			// If there isn't a role cache for this person, insert one.
			if ($roleCache === false) {
				// Add the role to the array.
				$data['roles'] = '["' . $role . '"]';
				// Insert the data
				$result = $this->db->insert('cache_role_members', $data);
				$resultType = 'insert';
			} else {
				// Otherwise, update the existing one.
				// Get the existing roles for this person.
				$currentRoles = $this->cacheGetRolesByPersonId($pId);
				// If the person already has the role, move on to the next person.
				if (in_array($role, $currentRoles)) {
					log_message('info', 'MODEL: PopuliApiCache | ' . $pId . ' already has the role ' . $role . ' in the cache');
					continue;
				} else {
					// If this person does not have the role in their cache list of roles, add it.
					$currentRoles[] = $role;
					// Convert the array of roles to a Json string.
					$data['roles'] = json_encode($currentRoles);
					// Update the record in the database.
					$this->db->where('person_id', $pId);
					$result = $this->db->update('cache_role_members', $data);
					$resultType = 'udpate';
				}
			}
			
			// Put something useful in the log.
			if (!isset($currentRoles)) $currentRoles = $role;
			$log = 'Roles were altered for ' . $pId . ': ' . $resultType . ' | ' . json_encode($currentRoles);
			if ($result) log_message('debug', 'MODEL: PopuliApiCache | ' . $log);
		}
		
		/* Now remove the role from any members that are no longer viable */
		// Get the current list of role members before any alterations.
		$oldMembers = $this->getRoleMembers($role);
		
		// Make an array of just new member ids
		$newMemberIds = [];
		foreach ($newMembers as $new) {
			$newMemberIds[] = (int)$new->person_id;			
		}

		// Loop over the list of member currently in the DB for this role.
		foreach ($oldMembers as $old) {
			// Set a boolean
			$isCurrent = false;
			// Set the current person id.
			$pId = $old;

			if (in_array($old, $newMemberIds)) {
				log_message('info', 'MODEL: PopuliApiCache | ' . $old . ' is a member of the ' . $role . ' role');
			} else {
				// This ID is not in the list of new members.  Remove this role from the record.
				// Get the existing roles for this person.
				$currentRoles = $this->cacheGetRolesByPersonId($pId);

				// Remove this role from the list of roles.
				foreach ($currentRoles as $key => $value) {
					if (ucwords($value) == $role) unset($currentRoles[$key]);
				}
				// Reindex the array.
				$currentRoles = array_values($currentRoles);		
				var_dump($currentRoles);

				// If this still has other roles, put them back into the database
				if (sizeof($currentRoles) > 0) {
					log_message('debug', 'MODEL: PopuliApiCache | ' . $old . ' is NO LONGER a member of the ' . $role . ' role');
					// Re-encode the roles into a Json string.					
					$json = json_encode($currentRoles);
					// Put the new list of roles back into the database.
					$this->db->where('person_id', $old);
					$this->db->update('cache_role_members', array('roles' => $json));
				} else {
					log_message('debug', 'MODEL: PopuliApiCache | ' . $old . ' has NO roles');
					// Otherwise, remove them from the table
					$this->db->delete('cache_role_members', array('person_id' => $old));
				}
			}
		}

		$members = $this->getRoleMembers($role);
		log_message('debug', 'MODEL: PopuliApiCache | The ' . $role . ' role contained ' . $countRoleMembersInit . ' people.');
		log_message('debug', 'MODEL: PopuliApiCache | The ' . $role . ' role now contains ' . sizeof($members) . ' people.');

		// Return the array of members.
		return $members;
	}















}
