<?php
//include('Support.php');
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {



  /**
   * [validate Validates the user's credentials against what is in the database
   * returns true if the user's credentials validate.]
   * @return [type] [true is valid user account is found]
   */
  public function validate() {
    log_message('debug', 'MODEL: User->validate()');

    // Access the database accessing the users table.
    $this->db->where('username', $this->input->post('username'));
    $this->db->where('password', md5($this->input->post('password')));
    $query = $this->db->get('user');

    $row = $query->row();

    if (isset($row)) {
      // A row was returned meaning there is a user in the database that matched
      // their username and password.
      log_message('debug', 'User->validate() The user was validated');
      return true;
    } else {
      log_message('debug', 'User->validate() The user was NOT validated');
      return false;
    }
  }


  public function is_logged_in() {
		$is_logged_in = $this->session->is_logged_in;

    log_message('info', 'MODEL: User | This is what is in the session: ' . json_encode($this->session) . '.  is_logged_in() contains: ' . $is_logged_in);

		if (!isset($is_logged_in) || $is_logged_in != true) {
      // Redirect to the login page.
      //log_message('error', 'MODEL: User | The user is NOT logged in, redirecting to the login screen.');
      redirect('auth/index');
		} else {
      // If logged in, return true.
      return true;
    }
	}





  /**
   * [is_admin Checks if the user is an admin]
   * @return boolean [Returns true if the current user is an admin]
   */
  public function is_admin() {
    // Get the acl list for the current user if logged in.
    if ($this->is_logged_in()) {
      $roles = $this->getAcl($this->session->username);
    }

    // Search the roles list of the admin flag.
    $search = false;
    foreach ($roles as $r) {
      if ($r == 'admin') {
        $search = true;
      }
    }

    // Return true if the user is admin, false otherwise.
    if ($search) {
      return true;
    } else {
      return false;
    }

  }












  /**
   * [getAcl Returns a list of roles that the current user has been granted]
   * @return [type] [an array of roles the user has been granted.]
   */
  public function getAcl($username) {
    log_message('info', 'MODEL: User | Return ACL for ' . $username);

    // Create the list of users and their roles.
    // Everyone who is not on this list becomes an operator.
    $users = array(
      'russ'        => 'admin,manager,operator',
      'carrie'      => 'manager,operator',
      'dkiff'       => 'manager,operator',
      'lilyruth'    => 'manager,operator',
      'beanmanager' => 'manager,operator',
      'cassie'      => 'admin,manager,operator',
      'carolyn'     => 'manager,operator',
      'bean'        => 'operator',
      'operator'    => 'operator',
      'lily'        => 'operator',
      'shawn'       => 'admin,manager,operator'
    );

    // Go through the list of users.
    foreach ($users as $u => $value) {
      if (strtolower($u) == strtolower($username)) {
        // If this is the user we are looking for, populate the roles.
        // Create an array out of the roles list.
        $roles = str_getcsv($value, ',');
        break;
      } else {
        $roles = false;
      }
    }

    log_message('info', 'MODEL: User | ' . $username . ' has been granted the roles of ' . implode(',', $roles) . '.');

    // Return the list of roles.
    return $roles;
  }



















}
