<?php
/**
 * A PHP Class for negotiating sessions and data with the Populi database.
 */


class PopuliApiExtended extends CI_Model {

	public function __construct() {
		// Call the parent constructor
		parent::__construct();

    // Load up the appropriate models.
    $this->load->model('populi/PopuliConnect');
    $this->load->model('populi/PopuliApi');
	}





  /**
   * Helper function that gets the current academic year from Populi's API initial term call.
   * @return int    Year id of the current academic year.
   */
  public function getCurrentAcademicYear() {
    $termData = $this->PopuliApi->getCurrentAcademicTerm();
    return (int)$termData->yearid;
  }















	/**
	 * Function to accompany getCurrentTermId.  Gets the current term by name.
	 */
	public function getCurrentTermName($currentTermId = -1) {
		// Variable for the current term's name.
		$currentTermName = -1;

		// If the parameter is -1, assume no input and that we want to return the current term name.
		if ($currentTermId = -1) {
			$currentTermId = $this->PopuliApiExtended->getCurrentTermId();
		}
		log_message('info', 'getCurrentTermName() $currentTermId = ' . $currentTermId);

		// Get a list of all the academic terms.
		$terms = json_decode($this->PopuliConnect->doTask('getAcademicTerms'))->academic_term;

		// Find the one with the id that is the same as the current term's id.
		foreach ($terms as $t) {
			if ($currentTermId == (int)$t->termid) {
				log_message('info', 'getCurrentTermName() Found the term id in the list.');
				$currentTermName = $t->name;
				break;
			}
		}

		log_message('info', 'getCurrentTermName() RETURN = ' . $currentTermName);
		return strtolower($currentTermName);
	}






















	/**
	 * [getCurrentTerm Calculates which term is the current one and returns the id for that term.]
	 * @return [type] [The term_id for the current term.  -1 if there is no current term.]
	 */
	public function getCurrentTermId() {
		// Get the raw data from Populi.
		$currentAcademicTerm = $this->PopuliApi->getCurrentAcademicTerm();

		// Get just the termi Id and cast it to an int.
		$currentTermId = (int)$currentAcademicTerm->termid;

		// Verify that we have a value or not.
		if ($currentTermId) {
			// Store this value in the database so we can retrieve it in case we are in between semesters.
			$this->Settings->setSettingValue('current_term_id', $currentTermId);
			log_message('info', 'MODEL: PopuliApi | current_term_id (' . $currentTermId . ') retrieved from Populi, caching this value into the database.');
		} else {
			$currentTermId = $this->Settings->getSettingValue('current_term_id');
			log_message('error', 'MODEL: PopuliApi | Failed to retrieve current_term_id from Populi.  Using locally cached value instead (' . $currentTermId . ').');
		}

		// Return the current term's id.  -1 if wrong.
		return $currentTermId;
	}






















	/**
	 * Determines in if a student is listed as an OSAP student in Populi.  We check for the
	 * custom field called OSAP and if this field is yes.
	 * @param  int  $personId		A numerical person id.
	 * @return boolean          Returns true is this person is an OSAP student, false otherwise.
	 */
	public function isOsapStudent($personId) {
		log_message('info', 'MODEL: PopuliApiExtended | isOsapStudent()');

		$isOsapStudent = false;

		// Go get this persons custom fields.
		$fields = $this->PopuliApi->getCustomFields($personId, strtoupper('person'));

		// Loop over the list of fields and see if one of them is the OSAP field.  Then check is value.
		// First check that fields were returned (this will be false if no fields).
		if ($fields) {
			foreach ($fields as $field) {
				if ($field->name == 'OSAP') {
					// Custom field found.
					// If the value of this custom field is Yes, we have a winner.
					$value = $field->values->value->value;
					if ($value == 'Yes') {
						$isOsapStudent = true;
						log_message('debug', 'MODEL: PopuliApiExtended | Person ' . $personId . ' is an OSAP student.');
					}
				} else {
				}
			}
		}

		if (!$isOsapStudent) {
			log_message('debug', 'MODEL: PopuliApiExtended | Person ' . $personId . ' is NOT an OSAP student.');
		}

		return $isOsapStudent;
	}





	/**
	 * Figure out the list of tags that have the term OSAP in them.
	 * @return array	  An array of tag objects (id, name).
	 */
	public function setOsapTags() {
		log_message('info', 'MODEL: PopuliApiExtended | getOsapTags()');

		// Get the list of all tags from Populi.
		$allTags = $this->PopuliApi->getTags();

		// Loop over all the tags, searching for the term 'osap' in the name.
		$osapTags = array();
		foreach ($allTags as $tag) {
			$name = trim($tag->name);
			log_message('info', 'MODEL: PopuliApiExtended | Searching inside of the tag named: ' . $name);
			if (stristr($name, 'osap')) {
				log_message('debug', 'MODEL: PopuliApiExtended | An OSAP tag has been found: '. $name);
				array_push($osapTags, $tag);
			}
		}

		$this->Settings->setSettingValue('osap_tags', json_encode($osapTags));
	}










	/**
	 * Determine's the primary contact info of a certain type based on a person object.
	 * @param  [object] $personObject		A person object like the one that gets returned by PopuliApi->getPerson().
	 * @param  [String]	$type						The type of contract info we are interested in.  'address', 'phone', 'email'.
	 * @return [ojbect]               	An object containing the primary address and it's information.
	*/
	public function determinePrimaryContactInfo($personObject, $type = 'email') {
		log_message('debug', 'MODEL: PopuliApiExtended | determinePrimaryContactInfo().  The type is ' . strtoupper($type));

		$error = false;

		switch ($type) {
			case 'address':
				/*
					If the address is set in the person object, simplify the variable, and then
					check to see if the address is an array or single object.  If not an array, make it one.
				 */
				if (isset($personObject->address)) {
					$addresses = $personObject->address;
					(is_array($addresses)) ? $typeArray = $addresses : $typeArray = array($addresses);
				} else {
					$error = 'a mailing address';
				}
				break;
			case 'phone':
				/* Do the same thing for the phone type */
				if (isset($personObject->phone)) {
					$phoneNumbers = $personObject->phone;
					(is_array($phoneNumbers)) ? $typeArray = $phoneNumbers : $typeArray = array($phoneNumbers);
				} else {
					$error = 'a phone number';
				}
				break;
			default:
				/* The default case is the email case. */
				if (isset($personObject->email)) {
					$emails = $personObject->email;
					(is_array($emails)) ? $typeArray = $emails : $typeArray = array($emails);
				} else {
					$error = 'an email address';
				}
				break;
		}

		// If there was an error.
		if ($error) {
			log_message('error', 'MODEL: PopuliApiExtended | The passed person object does not contain ' . $error . '.');
			return false;
		}

		// Process the typeArray to determine the primary entry of this type.
		foreach ($typeArray as $item) {
			// If this item is the primary, note it and break the loop.
			if ($item->is_primary == '1') {
				$itemObject = $item;
				break;
			} else {
				$itemObject = false;
			}
		}

		// Return the object or false;
		return $itemObject;
	}












	/**
	 * Retrieves the primary email address for a specific person.
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getPrimaryEmail($id) {
		log_message('info', 'MODEL: PopuliApiExtended | getPrimaryEmail()');

		// Check that the ID is numberic.
		if (!is_int($id)) {
			log_message('error', 'MODEL: PopuliApiExtended | The parameter, person_id, is not an integer.');
			return;
		}

		// Get data on this person from Populi.
		$person = $this->PopuliConnect->doTask('getPerson', array('person_id' => $id));

		$emailData = json_decode($person)->email;

		if (!is_array($emailData)) {
			// If there is just one email address, we don't get an array.
			log_message('info', 'MODEL: PopuliApiExtended | There is only one email address so it must be the primary.');
			$email = $emailData->address;
		} else {
			// If the email data is an array, we have multiple addresses and need to figure out which one is the primary.
			log_message('info', 'MODEL: PopuliApiExtended | There are multiple email addresses.');

			foreach ($emailData as $em) {
			// Check each of the addresses for this person.
				if ($em->is_primary == 1) {
					// If this address is the primary address
					log_message('info', 'MODEL: PopuliApiExtended | Primary email address identified.');
					$email = $em->address;
				}
			}
		}

		// Return the primary email address string.
		log_message('info', 'MODEL: PopuliApiExtended | The primary email address for pid=\'' . $id . '\' is ' . $email . '.');
		return $email;
	}

















  /**
   * Figures out which program this student is in.  If they are not in a program, the function returns null.
   * @param  [type] $personId [description]
   * @return [type]           Returns a program or object or null.
   */
  public function getStudentCurrentProgram($personId) {
    $currentDegree = null;

    // Get the list of current programs for this student.
    $programs = $this->PopuliApi->getStudentPrograms($personId);

    // If there are programs.
    if (isset($programs)) {
      // Loop through the list looking for the active program.
      foreach ($programs as $p) {
        // Check that the degrees variable contains something.
        if ($p->degrees) {
          // Ok, this student has at least one degree.  Check the array for multiple degrees, looking for the current one.
          foreach ($p->degrees as $d) {
            if ($d->status == 'ACTIVE') {
              log_message('info', 'MODEL: PopuliApiExtended | getStudentCurrentProgram() ' . $d->name . ' is the active program.');
              $currentDegree = $d;
            }
          }
        } else {
          // This person is not enrolled in a degree program.
          log_message('info', 'MODEL: PopuliApiExtended | getStudentCurrentProgram().  person_id ' . $personId . ' is not enrolled in a degree/program.');
          $currentDegree = null;
        }
      }
    }

    // Return the active program as an object.
    return $currentDegree;
  }















	/**
	 * Acquires the program data from Populi and constructs an array where each element of the array contains
	 * a degree and it's data pertinent to that student.
	 * @param  [int] $personId			A numerical person Id.
	 * @return [array]           		An array of degree objects.  Returns false on failure.
	 */
	public function getStudentDegrees($personId) {
		log_message('info', 'MODEL: PopuliApiExtended | getStudentDegrees()');

		// Determine if the input value was an int.
		if (!is_int($personId)) {
			log_message('error', 'MODEL: PopuliApiExtended | The personId supplied by the caller was not an integer.');
			return false;
		}

		// Get the program information about this student.
		$programs = $this->PopuliApi->getStudentPrograms($personId);

		// If programs is not false - contains some data.
		if ($programs) {
			// An empty array that will hold all the degrees.
			$degrees = array();
			// Iterate over each of the programs and extract the degree info.
			$countDegrees = 0;
			foreach ($programs as $pro) {
				log_message('info', 'MODEL: PopuliApiExtended | Evaluating information from program ' . $pro->program_id . ' for person ' . $personId);
				// Check if there are degrees
				if (isset($pro->degrees)) {
					// Iterate over each degree within this program, adding it's info to the new array.
					foreach ($pro->degrees as $deg) {
						$countDegrees++;

						// Setup the information about this degree.
						$array = array(
							'person_id'						=> $personId,
							'degree_id'						=> (int)$deg->degree_id,
							'program_id'					=> (int)$pro->program_id,
							'abbrv'								=> $deg->abbrv,
							'name'								=> $deg->name,
							'status'							=> $deg->status,
							'active_date'					=> $deg->active_date,
							'graduate_degree'			=> (int)$deg->graduate_degree,
							'catalog_year_id'			=> (int)$deg->catalog_year_id,
							'catalog_start_year'	=> $deg->catalog_start_year,
							'catalog_end_year'		=> $deg->catalog_end_year,
							'program_name'				=> $pro->name,
						);

						// If specializations is not null, encode it as as json string first.
						if ($deg->specializations)			$array['specializations'] = json_encode($deg->specializations);

						// Additional data that is not always set.
						if (is_string($deg->inactive_date)) 		$array['inactive_date'] = $deg->inactive_date;
						if (is_string($deg->graduation_date))		$array['graduation_date'] = $deg->graduation_date;
						if (is_string($pro->started_on)) 				$array['program_started_on'] = $pro->started_on;
						if (is_string($pro->inactive)) 					$array['program_inactive'] = $pro->inactive;

						// Encode the data into an object.
						$newDegree = json_decode(json_encode($array));

						// Put the object in the array of degrees.
						array_push($degrees, $newDegree);
					}
				}
				log_message('info', 'MODEL: PopuliApiExtended | Program ' . $pro->program_id . ' has ' . $countDegrees . ' degrees.');
			}
		} else {
			/* If there are no programs */
			$degrees = false;
		}

		// Tell the log
		if (!$degrees) log_message('error', 'MODEL: PopuliApiExtended | This person ' . $personId . ' does not have any degrees.');

		// Return the degrees array.
		return $degrees;
	}










	public function getActiveAccounts() {
		log_message('info', 'MODEL: PopuliApiExtended | getActiveAccounts()');

		$termStudents = $this->PopuliApi->getTermStudents();
		$staff  = $this->PopuliApiExtended->getStaffActive();
		$mealPlanStudents = $this->MealPlan->getPeopleOnMealPlans(true);
		$foodServicesCreditAccounts = $this->PopuliApi->getTaggedPeople(null, 'Food Services Credit Accounts (Cafeteria)');

		// Merge the accounts.		
		$array = array_merge($termStudents, $staff, $mealPlanStudents);
		$activeAccounts = [];
		foreach ($array as $ar) {
			$activeAccounts[] = (int)$ar->person_id;
		}

		// Remove any duplicate values
		$activeAccounts = array_unique($activeAccounts, SORT_NUMERIC);
		sort($activeAccounts);

		log_message('debug', 'MODEL: PopuliApiExtended | There are ' . sizeof($activeAccounts) . ' active accounts in Populi');

		// Return the active accounts list.
		return $activeAccounts;
	}






	/**
	 * Returns a list of all active students.  This function is a specific abstration of PopuliApi->getRoleMembers()
	 * @return [array]		An array containing student objects (person_id, first, last, inactive).
	 */
	public function getStudentsActive() {
		log_message('info', 'MODEL: PopuliApiExtended | getActiveStudents()');
		$activeStudents = $this->PopuliApi->getRoleMembers('student', null, 'ACTIVE', null, true);
		return $activeStudents;
	}

	/**
	 * Returns a list of all active staff members.  Removes certain utility accounts such as the Finance API and Cafeteria API users.
	 */
	public function getStaffActive($includeFaculty = false) {
		log_message('info', 'MODEL: PopuliApiExtended | getStaffActive()');
		
		$staff = $this->PopuliApi->getRoleMembers('staff');
		
		// If the include faculty flag is set, also include faculty - which will catch adjunct faculty.
		// Remove duplicates
		if ($includeFaculty) {
			// Commonalities is first, last, inactive, username, person_id - all strings
			$faculty = $this->PopuliApi->getRoleMembers('faculty');

			// Merge the arrays.
			$new = array_unique(array_merge($staff, $faculty), SORT_REGULAR);
			sort($new);
			$staff = $new;
		}

		// Do not includes these account numbers - they are special system numbers.
		$doNotInclude = array(12605036,14673992,11298392,19648448,19660904);
		// Loop over list and remove do not includes and rename the person_id property.
		foreach ($staff as $key => $val) {			
			// If this account is in the doNotInclude list.
			if (in_array($val->person_id, $doNotInclude)) {
				unset($staff[$key]);
				continue;
			}				
		}

		return $staff;
	}




	
	












  /**
   * Figure out which terms are included in the current academic year.
   * @return array    An array of term objects.
   */
  public function getTermsCurrentAcademicYear() {
    log_message('info', 'MODEL: PopuliApiExtended | getTermsCurrentAcademicYear()');

    // Get the id's for the current term and academic year.
    $currentTerm = $this->PopuliApi->getCurrentAcademicTerm();
    $termId = $currentTerm->termid;
    $yearId = $currentTerm->yearid;

    // Get a list of academic terms
    $academicTerms = $this->PopuliApi->getAcademicTerms();

    // Loop through the list looking for adding only terms from yearId to the array.
    $termsCurrentYear = array();
    foreach ($academicTerms as $term) {
      if ($term->yearid == $yearId) {
        array_push($termsCurrentYear, $term);
      }
    }

    return $termsCurrentYear;
  }










	/**
	 * getUpdatedStudents gets a short list of students who's personal data has changed in the past
	 * few days since $startDate.  If $startDate isn't defined, we pick 3 days in the past.
	 * @param  String $startDate Start date in the form 'YYYY-mm-dd';
	 * @return Array            	An array of basic person objects containing person id, name, etc.
	 */
	public function getUpdatedStudents($startDate = null) {
		log_message('debug', 'MODEL: PopuliApiExtended | getUpdatedStudents()');

		if (!$startDate) {
			// Caller didn't specify a start date, so we need to apply a date in the past.
			$startDate = date('Y-m-d', strtotime('-1 days', now()));
		}

		// Get a list of active students from Populi.
		$students = $this->PopuliApiExtended->getStudentsActive();
		// Get a list of updated people from People.
		$updatedPeople = $this->PopuliApi->getUpdatedPeople($startDate);
		// An empty array for the updated students.
		$updatedStudents = array();

		// Check the list of active students to see if any of them has updated person data.
		$count = 1;
		foreach ($students as $s) {
			// Loop over the updated people and see if this person is on the list.
			foreach ($updatedPeople as $person) {
				if ((int)$person->id == (int)$s->person_id) {
					// We found an active student who's personal information has changed!
					log_message('debug', 'MODEL: PopuliApiExtended | Person data for ' . $person->id . ' has changed since '. $startDate);
					// Prep an object with the combined data.
					$args = (Object)array(
						'person_id'					=> (int)$s->person_id,
						'username'					=> $s->username,
						'is_active_user'		=> (int)$person->is_active_user,
						'inactive_student'	=> (int)$s->inactive,
						'status'						=> $person->status,
						'first'							=> $s->first,
						'last'							=> $s->last,
						'preferred_name'		=> $person->preferred_name,
						'middle_name'				=> $person->middle_name,
						'prefix'						=> $person->prefix,
						'suffix'						=> $person->suffix,
						'former_name'				=> $person->former_name,
						'maiden_name'				=> $person->maiden_name,
						'gender'						=> $person->gender,
						'birth_date'				=> $person->birth_date,
						'updated_at'				=> $person->updated_at,
					);
					// Push the person object into the array.
					array_push($updatedStudents, $args);
					// Break out of this loop.
					break;
				}
			}
		}

		// Return the list of students who's personal info has been updated.
		return $updatedStudents;
	}










  /**
   * Generates a list of student objects for each student enrolled in a class in the current academic year.
   * @return [array] 		An array of objects containing the students from the current academic year.
   */
  public function getYearStudents() {
    log_message('debug', 'MODEL: PopuliApiExtended | getYearStudents()');

    // Get a list of terms from the current academic year.
    $terms = $this->PopuliApiExtended->getTermsCurrentAcademicYear();

    // Generate a unix time stamp for now.
    $now = time();

    // Loop through each of the terms to determine if any are in the future.
    $termIdsToQuery = array();
    foreach ($terms as $term) {
      // Convert the start date to a nice date.
      $start = nice_date($term->start_date);
      // If this term begins before now, add it to our array.
      if ($start <= $now) {
        array_push($termIdsToQuery, $term->termid);
      }
    }

    // Sort the array for the earliest is first.
    sort($termIdsToQuery);
		log_message('debug', 'MODEL: PopuliApiExtended | Querying these term id\'s: ' . implode(',', $termIdsToQuery));

    // Loop through each of the terms to Query and generate a list of students from Populi.
    $currentYearStudents = array();
    $i = -1;
    foreach ($termIdsToQuery as $termId) {
      $i++;
      // Get the data from Populi for this semester.
      log_message('info', 'MODEL: PopuliApiExtended | Getting student data from Populi for term id ' . $termId);
      $termStudents = $this->PopuliApi->getTermStudents($termId);

      // Loop over the data and add each student to the array.
      foreach ($termStudents as $student) {
        // Use $i to determine if we computing for the first term or another term in the list of $termIdsToQuery.
        if ($i == 0) {
          log_message('info', 'MODEL: PopuliApiExtended | Adding student id ' . $student->student_id . ' who is a new student for this semester.');
          // Add this student to the array of current year students.
          array_push($currentYearStudents, $student);
        } else {
          // Be careful, only add students to the array who aren't already there.
          if (!searchAssociativeArrayByKey($currentYearStudents, 'student_id', $student->student_id)) {
            log_message('info', 'MODEL: PopuliApiExtended | Adding student id ' . $student->student_id . ' who is a new student for this semester.');
            // Add this student to the array of current year students.
            array_push($currentYearStudents, $student);
          }
        }
      }
    }

		// Return the new array of json objects containing the students from the current academic year.
    log_message('debug', 'MODEL: PopuliApiExtended | There are ' . sizeof($currentYearStudents) . ' unique students in the current academic year.');
    return $currentYearStudents;
  }




  /**
   * A little helper function to get a specific custom field ID for a specific field from Populi.
   */
  public function getCustomFieldId($name) {
	// Get a list of all custom fields.
	$allCustomFields = $this->PopuliApi->getAllCustomFields();
	// Loop over each of the custom fields returned
	foreach ($allCustomFields as $f) {
		if ($f->name == $name) return $f->id;
	}
	// We didn't find anything, return false.
	return false;
}









}
?>
