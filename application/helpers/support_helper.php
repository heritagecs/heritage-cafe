<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Supporting functions.
 */


/**
 * [getDateTime figures out the proper date and time based on the SQL format]
 * @return [type] [returns a date time string in the proper SQL format]
 */
function getSqlDateTime($date = 'Y-m-d', $time = 'H:i:s') {
  return date($date . ' ' . $time);
}




// Creates a csv string from single dimension array.
function csv($array) {
  log_message('info', 'HELPER: csv() | parameter: $array = ' . implode($array));
  
  // Empty variable for the new string.
  $csv = "";

  // Loop over the array.
  for( $i = 0; $i < count($array); $i++ ) {
    // Add the current element to the string.
    $csv .= '"' . str_replace('"', '""', $array[$i]) . '"';
    // Only if this is not the last item, add a comma.
    if( $i < count($array) - 1 ) $csv .= ",";
  }
  return $csv;
}











	/**
	 * Takes an associative array, a key to search through and a search term to search for and Determines
	 * if the array contain the search string somewhere within the values for that key.
	 * @param  [array] $array      		The associative array to search through.
	 * @param  [String] $key        	The name of the key to search in.
	 * @param  [String] $searchTerm 	The search term we are looking for.
	 * @return [boolean]             	Returns true if the search term was found in the array false if not.
	 */
  function searchAssociativeArrayByKey($array, $key, $searchTerm) {
    log_message('info', 'searchAssociateArrayByKey().  $key = '. $key . ', $searchTerm = ' . $searchTerm);
    $match = false;

    // Loop over the array to be searched.
    foreach ($array as $item) {
      // Cast the object to an array.
      $item = (array)$item;

      // If the value of the key for this current row is equal to the value I'm searching for.
      if ($item[$key] == $searchTerm) {
        $match = true;
      }
    }

    return $match;
  }



















  /**
   * Functions that have to do with dates.
   */




  /**
  * Ensures that the value is a proper date.  If not, the date is set to today.
  * @param  [type] $date A date string.
  * @return [type]       A date string.
  */
  function sanitizeDate($date = -1) {
     // Tell the log
     log_message('info', 'HELPER: support | sanitizeDate(PARAMETER: $date = ' . $date . ')');

     // Create a new date object using the input parameter.
     $dateObject = new DateTime($date);
     // Format the date object.
     $newDate = $dateObject->format('Y-m-d');
     // Log a message
     log_message('info', 'HELPER: support | sanitizeDate() RETURN: ' . $newDate);
     // Spit back the formatted date.
     return $newDate;
  }


  function subtractFromDate($inputDate, $days) {
    $date = new DateTime($inputDate);
    $date->sub(new DateInterval('P' . $days . 'D'));
    return $date->format('Y-m-d');
  }

  function addToDate($inputDate, $days) {
    $date = new DateTime($inputDate);
    $date->add(new DateInterval('P' . $days . 'D'));
    return $date->format('Y-m-d');
  }



  function setEndDate($start, $kind) {
    if ($kind == 'week') {
     $end = addToDate($start, 7);
    } else if ($kind == 'month') {
     $end = addToDate($start, $this->daysPerMonth($start));
    } else if ($kind == 'year') {
     $end = addToDate($start, 365);
    } else {
     $end = null;
    }

    log_message('debug', 'HELPER: support | setEndDate($start[' . $start . '], $kind[' . $kind . '])');
    return $end;
  }


   /**
    *  Add the appropriate time stamps to a day date based on if the date is the beginning
    *  or end of a date range.
    */
   function appendTimeToDate($date, $isEnd = false) {
     // Tell the log
     log_message('info', 'HELPER: support | appendTimeToDate(PARAMETERS: $date = ' . $date . ', $isEnd = ' . $isEnd . ')');

     // Sanitize the input.
     $date = strtotime(sanitizeDate($date));

     // Change to a unix timestamp.
     //$date = nice_date($date);

     // Add almost a full day to the timestamp is the $start parameter is false.
     if ($isEnd) {
       $date = $date + 86399;
     }

     // Convert the timestamp back into human readable form.
     $date = unix_to_human($date, true, 'euro');

     // Return the newly format date string.
     log_message('info', 'HELPER: support | appendTimeToDate() RETURN: ' . $date);
     return $date;
   }


   /**
    * Figure out the date that is the start of the week of the submitted date.
    * If no date, assume this present week.
    * @param  [type] $date A date in Y-m-d format.
    * @return [type]       A date in Y-m-d format that is a Monday.
    */


  function findStartOfWeek($date = null) {
     $previousMonday = null;

     // If no parameter was send, make it today.
     if (!isset($date)) {
       $date = date('Y-m-d');
     }

     // Figure out which day of the week it is.  Convert it to unix timestamp, then to standard date string
     $date = strtotime($date);
     $standardDate = date(DATE_RFC822, $date);

     // Tell the log.
     log_message('info', 'HELPER: support | findStartOfWeek(), $date(' . $date . '), $standardDate(' . $standardDate . ')');

     if (strpos($standardDate, 'Mon') > -1) {
       $dayOfTheWeek = 'Monday';
       $previousMonday = $date;
     } else if (strpos($standardDate, 'Tue') > -1) {
       $dayOfTheWeek = 'Tuesday';
       $previousMonday = $date - 86400;
     } else if (strpos($standardDate, 'Wed') > -1) {
       $dayOfTheWeek = 'Wednesday';
       $previousMonday = $date - (86400 * 2);
     } else if (strpos($standardDate, 'Thu') > -1) {
       $dayOfTheWeek = 'Thursday';
       $previousMonday = $date - (86400 * 3);
     } else if (strpos($standardDate, 'Fri') > -1) {
       $dayOfTheWeek = 'Friday';
       $previousMonday = $date - (86400 * 4);
     } else if (strpos($standardDate, 'Sat') > -1) {
       $dayOfTheWeek = 'Saturday';
       $previousMonday = $date - (86400 * 5);
     } else {
       $dayOfTheWeek = 'Sunday';
       $previousMonday = $date - (86400 * 6);
     }

     // Convert the date string into Y-m-d.
     $previousMonday = date('Y-m-d', $previousMonday);

     // Tell the log.
     log_message('info', 'HELPER: support | The input day is a ' . $dayOfTheWeek . '.  The most recent Monday is ' . $previousMonday);

     // Return the previous Monday's date.
     return $previousMonday;
   }





   /**
    * Based on the input date (today if null), figure out the start date of the month.
    * @param  [type] $date [description]
    * @return [type]       [description]
    */
   function findStartOfMonth($date = null) {
     log_message('debug', 'HELPER: support | findStartOfMonth(PARAMETERS: $date = ' . $date . ')');
     $newDate = null;

     // If no parameter was sent, make the date today.
     if (!isset($date)) {
       $date = date('Y-m-d');
     }

     // Extract the month component from the date.
     $timestamp = strtotime($date);
     $month = date("m", $timestamp);
     $year = date("Y", $timestamp);

     log_message('info', 'The month is ' . $month);
     // Build a new date that is the first of the month in question.
     $newDate = $year . '-' . $month . '-01';

     // Return the new date string.
     log_message('debug', 'HELPER: support | findStartOfMonth RETURN: ' . $newDate);
     return $newDate;
   }


   /**
    * Accepts a date and returns the number of days in that month.
    * @param  [type] $date [description]
    * @return [type]       [description]
    */
   function daysPerMonth($date) {
     // Begin function
     log_message('info', 'HELPER: support | daysPerMonth($date = ' . $date . ')');

     // Turn the date into a unix timestamp.
     $timestamp = strtotime(sanitizeDate($date));

     // Get the month and year from the date given.
     $month = date('m', $timestamp);
     $year = date('Y', $timestamp);

     // Set the days per month based on the input month.
     $daysInMonth = days_in_month($month, $year);

     // Tell the log what we found.
     log_message('info', 'HELPER: support | daysPerMonth() RETURN = ' . $daysInMonth);

     // Return the correct number of days.
     return $daysInMonth;
   }


   /**
    * Figure out which year it is and then make sure the date is the first of January
    * for that year.
    * @param  [type] $date [description]
    * @return [type]       [description]
    */
   function findStartOfYear($date) {
     // Begin function
     log_message('info', 'HELPER: support | findStartOfYear() PARAMETERS: $date = ' . $date);

     // Sanitize the input date.
     $timestamp = strtotime(sanitizeDate($date));

     // Get just the year.
     $year = date('Y', $timestamp);

     // Build the new date.
     $newDate = $year . '-01-01';

     // Begin function
     log_message('info', 'HELPER: support | findStartOfYear() RETURN: ' . $newDate);

     // Return the correct date for the beginning of the input year.
     return $newDate;
   }


   // Tells the caller how many days are in the specified year.
   function daysPerYear($date) {
     log_message('info', 'HELPER: support | daysPerYear() PARAMETERS: $date = ' . $date);

     // Sanitize the date.
     $timestamp = strtotime(sanitizeDate($date));

     // Determine if this date is a leap year.
     if (date('L', $timestamp)) {
       $days = 366;
     } else {
       $days  = 365;
     }

     log_message('info', 'HELPER: support | daysPerYear() RETURN: ' . $days);

     // Return the appropriate number of days.
     return $days;
   }
