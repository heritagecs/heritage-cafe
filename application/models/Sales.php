<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Model {

  public function __construct() {
    // Call the parent constructor
		parent::__construct();

    // Load up the appropriate models.
    $this->load->model('InventoryTable');
    $this->load->model('Transaction');
    $this->load->model('ItemsSold');
  }





  /**
   * Generates sales data using the transaction and items_sold tables.
   * @param  [type] $startDate The first; date of interest.  Year month day.
   * @param  [type] $endDate   The last date of interest.  Year month day.
   * @return [type]            An array of objects.  Each object represents an item in inventory and its sales data
   *                              for the dates provides by the caller.
   */
  public function getSalesData($startDate, $endDate = -1) {
    log_message('debug', 'getSalesData(PARAMETERS: $startDate = ' . $startDate . ', $endDate = ' . $endDate . ')');

    // An empty array for the item objects.
    $items = array();

    // Add time stamps to the dates.
    $startDate = $this->appendTimeToDate($startDate);
    $endDate = $this->appendTimeToDate($endDate, true);

    log_message('debug', 'getSalesData() | Dates used in query: $startDate = ' . $startDate . ', $endDate = ' . $endDate);

    // Query the items_sold table for each of these items counting how many rows there are for each.
    // Use the dates from above.
    $fields = 'items_sold.inventory_id, inventory.category_id,';
    $fields .= 'COUNT(items_sold.inventory_id) as qty, inventory.description';

    // Build the query.
    $this->db->select($fields);
    $this->db->from('items_sold');
    $this->db->order_by('inventory.category_id', 'ASC');
    $this->db->order_by('qty', 'DESC');
    $this->db->order_by('inventory.description', 'ASC');
    $this->db->where('date >=', $startDate);
    $this->db->where('date <=', $endDate);
    $this->db->where('items_sold.price >=', 0);
    $this->db->join('inventory', 'inventory.id = items_sold.inventory_id');
    $this->db->group_by('items_sold.inventory_id');

    // Execute the query.
    $query = $this->db->get();
    $items = $query->result();

    // Additional processssing to account for reductions in quantity sold due to credit transactions.
    $returnedItems = $this->ItemsSold->getItemsSold($startDate, $endDate, array('returned_items' => true));
    if ($returnedItems) {
      // Loop over the returned items if there were some.
      foreach ($returnedItems as $ret) {
        // Search the main list for this item.
        foreach ($items as $i) {
          // If this items sold matched a returned item, decrement the qty sold by one.
          if ($i->inventory_id == $ret->inventory_id) $i->qty--;
        }
      }
    }

    // Return the array of item objects.
    return $items;
  }




  /**
   * Generates a list of cash transaction data for the inputted date range.
   * @param  [type]  $start Date formatted as yyyy-mm-dd.
   * @param  integer $end   Date formatted as yyyy-mm-dd.
   * @return [type]         An array of transaction objects each containing information for a specific transaction.
   */
  public function getCashSalesData($start, $end = -1) {
    log_message('info', 'getCashSalesData(PARAMETERS: $start = ' . $start . ', $end = ' . $end . ')');

    // Validate the end date input.
    if ($end == -1) {
      $end = $start;
    }

    // Add time stamps to dates.
    $start = $this->appendTimeToDate($start);
    $end = $this->appendTimeToDate($end, true);

    log_message('debug', 'getCashSalesData | Dates in query: $start = ' . $start . ', $end = ' . $end . ')');

    // List the fields
    $fields = 'id, date, subtotal, tax, discount, total';

    // Build a query.
    $this->db->select($fields);
    $this->db->where('date >=', $start);
    $this->db->where('date <=', $end);
    $this->db->where('payment_type', 4);  // Cash payment type.
    $query = $this->db->get('transaction');
    $transactions = $query->result();

    return $transactions;
  }

















  /**
   * Based on the start & end dates, and the kind this function calculates the new previous dates for the
   * previous and next buttons in the UI.  It returns a JSON object.
   * @param  [type] $start The start date
   * @param  [type] $end   The end date
   * @param  [type] $kind  The kind
   * @return [type]        A json object.
   */
  public function calculateReportDates($start, $end, $kind) {
    // Tell the log what we got.
    log_message('debug', 'calculateReportDates(PARAMETERS: [' . $start . '],[' . $end . '],[' . $kind . '])');

    // Sanitize the input dates.
    $start = $this->sanitizeDate($start);
    $end = $this->sanitizeDate($end);

    // If the end date is before the start date, make the end date equal to the start date.
    if ($end < $start) {
      log_message('debug', 'End date is before start date.  End date is now the same as the start date.');
      $end = $start;
    }

    // Set start and ends dates for the previous range based on the kind.
    switch ($kind) {
      case 'day':
        // Set the current dates.
        $end = $start;
        // The previous start date is one day behind the current start date.
        $pStart = $this->subtractFromDate($start, 1);
        $pEnd = $pStart;
        // The next start date is one day ahead of the current start date.
        $nStart = $this->addToDate($start, 1);
        $nEnd = $nStart;
        break;
      case 'week':
        // The start date is the Monday prior to the date supplied.
        $start = $this->findStartOfWeek($start);
        // Add 6 days to the start date to get the end of the current week.
        $end = $this->addToDate($start, 7 - 1);
        // Previous start date is one week behind the current start date.
        $pStart = $this->subtractFromDate($start, 7);
        $pEnd = $this->addToDate($pStart, 7 - 1);
        // The next start date is one week ahead of the current start date.
        $nStart = $this->addToDate($start, 7);
        $nEnd = $this->addToDate($nStart, 7 - 1);
        break;
      case 'month':
        $start = $this->findStartOfMonth($start);
        // Add a full month based on the days in this particular month.
        $end = $this->addToDate($start, $this->daysPerMonth($start) - 1);
        // Get the current month's start date, subtract a day, then get that current month's start date.
        $pStart = $this->findStartOfMonth($this->subtractFromDate($start, 1));
        $pEnd = $this->addToDate($pStart, $this->daysPerMonth($pStart) - 1);
        // Next dates are one month ahead of the current dates.
        $nStart = $this->addToDate($start, $this->daysPerMonth($start));
        $nEnd = $this->addToDate($nStart, $this->daysPerMonth($nStart) - 1);
        break;
      case 'year':
        // Calculate the current dates.
        $start = $this->findStartOfYear($start);
        $daysInCurrentYear = $this->daysPerYear($start);
        $end = $this->addToDate($start, $daysInCurrentYear - 1);
        // Get the start of the current year, subtract a day, then get the new year's start.
        $pStart = $this->findStartOfYear($this->subtractFromDate($start, 1));
        $pEnd = $this->addToDate($pStart, $daysInCurrentYear - 1);
        // Add a full year to the current start end and work from there.
        $nStart = $this->addToDate($start, $daysInCurrentYear);
        $nEnd = $this->addToDate($nStart, $this->daysPerYear($nStart) - 1);
        break;
      default:
        $pStart = $start;
        $pEnd = $end;
        $nStart = $start;
        $nEnd = $end;
    }


    // Create an associative array
    $dates = json_encode(array(
      'kind'          => $kind,
      'start'  => $start,
      'end'    => $end,
      'pStart' => $pStart,
      'pEnd'   => $pEnd,
      'nStart'     => $nStart,
      'nEnd'       => $nEnd,
    ));

    log_message('debug', 'calculateReportDates() RETURN = ' . $dates);

    // Encode a json string and return.
    return json_decode($dates);
  }





































  /**
   * Get the price of a single item
   * @param  [type] $id The id of that item.
   * @return [type]     The price.
   */
  public function getItemPrice($id) {
    // Get the item's price.
    $this->db->select('price');
    $this->db->where('id', $id);
    $query = $this->db->get('inventory');

    // Return the first row's price as a two-decimal number.
    return number_format($query->row()->price, 2);
  }















  /**
   * Ensures that the value is a proper date.  If not, the date is set to today.
   * @param  [type] $date A date string.
   * @return [type]       A date string.
   */
  public static function sanitizeDate($date = -1) {
    // Tell the log
    log_message('info', 'sanitizeDate(PARAMETER: $date = ' . $date . ')');

    // Create a new date object using the input parameter.
    $dateObject = new DateTime($date);
    // Format the date object.
    $newDate = $dateObject->format('Y-m-d');
    // Log a message
    log_message('info', 'sanitizeDate() RETURN: ' . $newDate);
    // Spit back the formatted date.
    return $newDate;
  }













/*
  public function subtractDateBasedOnKind($date, $kind) {
    // Determine the kind.
    if ($kind == 'day') {
      $newDate = $this->subtractFromDate($date, 1);
    } else if ($kind == 'week') {
      $newDate = $this->subtractFromDate($date, 7);
    } else if ($kind == 'month') {
      $newDate = $this->subtractFromDate($date, $this->daysPerMonth($date));
    } else if ($kind == 'year') {
      $newDate = $this->subtractFromDate($date, 365);
    } else {
      $newDate = $date;
    }

    log_message('info', 'subtractDateBasedOnKind($date[' . $date . '], [' . $kind . ']) | RETURN: ' . $newDate);
    return $newDate;
  }

  public function addDateBasedOnKind($date, $kind) {
    // Determine the kind.
    if ($kind == 'day') {
      $newDate = $this->addToDate($date, 1);
    } else if ($kind == 'week') {
      $newDate = $this->addToDate($date, 7);
    } else if ($kind == 'month') {
      $newDate = $this->addToDate($date, $this->daysPerMonth($date));
    } else if ($kind == 'year') {
      $newDate = $this->addToDate($date, 365);
    } else {
      $newDate = $date;
    }

    log_message('info', 'addDateBasedOnKind($date[' . $date . '], [' . $kind . ']) | RETURN: ' . $newDate);
    return $newDate;
  }*/

  public function subtractFromDate($inputDate, $days) {
  	$date = new DateTime($inputDate);
  	$date->sub(new DateInterval('P' . $days . 'D'));
  	return $date->format('Y-m-d');
  }

  public function addToDate($inputDate, $days) {
  	$date = new DateTime($inputDate);
  	$date->add(new DateInterval('P' . $days . 'D'));
  	return $date->format('Y-m-d');
  }































  /**
   * Figure out the date that is the start of the week of the submitted date.
   * If no date, assume this present week.
   * @param  [type] $date A date in Y-m-d format.
   * @return [type]       A date in Y-m-d format that is a Monday.
   */
  public static function findStartOfWeek($date = null) {
    $previousMonday = null;

    // If no parameter was send, make it today.
    if (!isset($date)) {
      $date = date('Y-m-d');
    }

    // Figure out which day of the week it is.  Convert it to unix timestamp, then to standard date string
    $date = nice_date($date);
    $standardDate = date(DATE_RFC822, $date);

    // Tell the log.
    log_message('info', 'findStartOfWeek(), $date(' . $date . '), $standardDate(' . $standardDate . ')');

    if (strpos($standardDate, 'Mon') > -1) {
      $dayOfTheWeek = 'Monday';
      $previousMonday = $date;
    } else if (strpos($standardDate, 'Tue') > -1) {
      $dayOfTheWeek = 'Tuesday';
      $previousMonday = $date - 86400;
    } else if (strpos($standardDate, 'Wed') > -1) {
      $dayOfTheWeek = 'Wednesday';
      $previousMonday = $date - (86400 * 2);
    } else if (strpos($standardDate, 'Thu') > -1) {
      $dayOfTheWeek = 'Thursday';
      $previousMonday = $date - (86400 * 3);
    } else if (strpos($standardDate, 'Fri') > -1) {
      $dayOfTheWeek = 'Friday';
      $previousMonday = $date - (86400 * 4);
    } else if (strpos($standardDate, 'Sat') > -1) {
      $dayOfTheWeek = 'Saturday';
      $previousMonday = $date - (86400 * 5);
    } else {
      $dayOfTheWeek = 'Sunday';
      $previousMonday = $date - (86400 * 6);
    }

    // Convert the date string into Y-m-d.
    $previousMonday = date('Y-m-d', $previousMonday);

    // Tell the log.
    log_message('info', 'The input day is a ' . $dayOfTheWeek . '.  The most recent Monday is ' . $previousMonday);

    // Return the previous Monday's date.
    return $previousMonday;
  }





  /**
   * Based on the input date (today if null), figure out the start date of the month.
   * @param  [type] $date [description]
   * @return [type]       [description]
   */
  public function findStartOfMonth($date = null) {
    log_message('debug', 'findStartOfMonth(PARAMETERS: $date = ' . $date . ')');
    $newDate = null;

    // If no parameter was sent, make the date today.
    if (!isset($date)) {
      $date = date('Y-m-d');
    }

    // Extract the month component from the date.
    $timestamp = strtotime($date);
    $month = date("m", $timestamp);
    $year = date("Y", $timestamp);

    log_message('info', 'The month is ' . $month);
    // Build a new date that is the first of the month in question.
    $newDate = $year . '-' . $month . '-01';

    // Return the new date string.
    log_message('debug', 'findStartOfMonth RETURN: ' . $newDate);
    return $newDate;
  }


  /**
   * Accepts a date and returns the number of days in that month.
   * @param  [type] $date [description]
   * @return [type]       [description]
   */
  public function daysPerMonth($date) {
    // Begin function
    log_message('info', 'daysPerMonth($date = ' . $date . ')');

    // Turn the date into a unix timestamp.
    $timestamp = nice_date($date);

    // Get the month and year from the date given.
    $month = date('m', $timestamp);
    $year = date('Y', $timestamp);

    // Set the days per month based on the input month.
    $daysInMonth = days_in_month($month, $year);

    // Tell the log what we found.
    log_message('info', 'daysPerMonth() RETURN = ' . $daysInMonth);

    // Return the correct number of days.
    return $daysInMonth;
  }


  /**
   * Figure out which year it is and then make sure the date is the first of January
   * for that year.
   * @param  [type] $date [description]
   * @return [type]       [description]
   */
  public function findStartOfYear($date) {
    // Begin function
    log_message('info', 'findStartOfYear() PARAMETERS: $date = ' . $date);

    // Sanitize the input date.
    $newDate = $this->sanitizeDate($date);

    // Turn the date into a unix timestamp.
    $timestamp = nice_date($newDate);

    // Get just the year.
    $year = date('Y', $timestamp);

    // Build the new date.
    $newDate = $year . '-01-01';

    // Begin function
    log_message('info', 'findStartOfYear() RETURN: ' . $newDate);

    // Return the correct date for the beginning of the input year.
    return $newDate;
  }


  // Tells the caller how many days are in the specified year.
  public function daysPerYear($date) {
    log_message('info', 'daysPerYear() PARAMETERS: $date = ' . $date);
    $timestamp = nice_date($date);

    // Determine if this date is a leap year.
    if (date('L', $timestamp)) {
      $days = 366;
    } else {
      $days  = 365;
    }

    log_message('info', 'daysPerYear() RETURN: ' . $days);

    // Return the appropriate number of days.
    return $days;
  }



















  public function setEndDate($start, $kind) {
    if ($kind == 'week') {
      $end = $this->addToDate($start, 7);
    } else if ($kind == 'month') {
      $end = $this->addToDate($start, $this->daysPerMonth($start));
    } else if ($kind == 'year') {
      $end = $this->addToDate($start, 365);
    } else {
      $end = null;
    }

    log_message('debug', 'setEndDate($start[' . $start . '], $kind[' . $kind . '])');
    return $end;
  }













  /**
   *  Add the appropriate time stamps to a day date based on if the date is the beginning
   *  or end of a date range.
   */
  public function appendTimeToDate($date, $isEnd = false) {
    // Tell the log
    log_message('info', 'appendTimeToDate(PARAMETERS: $date = ' . $date . ', $isEnd = ' . $isEnd . ')');

    // Sanitize the input.
    $date = $this->sanitizeDate($date);

    // Change to a unix timestamp.
    $date = nice_date($date);

    // Add almost a full day to the timestamp is the $start parameter is false.
    if ($isEnd) {
      $date = $date + 86399;
    }

    // Convert the timestamp back into human readable form.
    $date = unix_to_human($date, true, 'euro');

    // Return the newly format date string.
    log_message('info', 'appendTimeToDate() RETURN: ' . $date);
    return $date;
  }







}
