<nav id="hcs-secondary-navigation" class="hcs-secondary-navigation hcs-navigation" role="navigation">
  <ul class="unstyled-list">
    <li class="admin"><a href="<?php base_url(); ?>/admin/settings">Settings</a></li>
    <li class="admin-users"><a href="<?php base_url(); ?>/admin/users">Users</a></li>
    <li class="admin-util"><a href="<?php base_url(); ?>/admin/util">Utilities</a></li>
  </ul>
</nav>
