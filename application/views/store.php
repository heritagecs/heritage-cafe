<?php $this->load->view('parts/header'); ?>



<?php
// Verify the data loaded successfully.
$inventory = $inventory;
$categories = $categories;
$accounts = $accounts;
$featured = $featured;

// Get the storefront from the cookie.
define('STOREFRONT', get_cookie('storefront'));

// Convert the inventory to a json string.
$inventoryJson = json_encode($inventory);

?>

<!-- Start of the individual view -->

	<div id="" class="site-content-inner">
		<!--<h1>Store</h1>-->

		<div class="row grid-full">
			<form id="store" action="" method="post">
				<div class="form-input-area width-two-thirds">

					<?php if (STOREFRONT == 'CAFETERIA') { ?>
					<section class="store-block store-category-block">
						<header class="section-header">
							<h3>Featured Items<h3>
						</header>
						<div class="section-body clear">
							<?php
							// Loop through the featured items
							foreach ($featured as $item) {
								echo '<button class="btn btn-item btn-item-featured" type="button"';
								echo 'onclick="addItemToCheckout(' . $item->inventory_id . ')">';
								echo $item->description;
								echo '</button>';
							}
							?>
						</div>
					</section>
					<?php } ?>

					<section id="store-current-category" class="store-block store-category-block store-current-category">
						<header class="section-header"><h3 id="store-current-category-title">Category Title</h3></header>
						<nav class="nav nav-teriary" role="navigation">
							<ul>
							<?php
							// Loop through the categories.
							foreach ($categories as $cat) {
								// Prepare the category name cause we'll use it a few times.
								$name = $cat->category;
								$catInv = array();

								// If the storefront for this category is the same as the storefront selected by the storefront cookie,
								// display this category
								if ($cat->storefront == STOREFRONT) {
															
									// Loop over the inventory building a short list of items in this category
									log_message('info', 'VIEW: Store | Loading header and items for category ' . $name . '.');
									foreach ($inventory as $i) {
										if ($i->category_id == $cat->id) {
											array_push($catInv, $i);
										}
									}
								} else {
									// Since this category isnt part of the storefront, dont display it.
									continue;
								}
							?>
								<li><a class="" href="javascript:void(0)" onclick="loadCurrentCategoryItems(<?php echo $cat->id; ?>)"><?php echo $name; ?></a></li>
							<?php
							}
							?>
							</ul>
						</nav>
						<div class="section-body clear">
						</div>
					</section>

				<?php
          // Create a drop down for the inventory
          //$this->InventoryTable->createInventoryDropDown();
				?>
				</div>

				<div class="form-footer width-one-third">
					<section id="checkout" class="store-block checkout">
						<div class="section-header"><h3>Checkout</h3></div>
						<div class="section-body transaction-details">
								<section class="session-info">
									<code><?php echo getSqlDateTime(); ?></code>
								</section>
								<section class="items clear">
									<ul>
									</ul>
								</section>
								<section class="totals clear">
									<ul>
										<li><div class="totals-label">Subtotal</div><span id="subtotal" class="price price-total">0.00</span></li>
										<li><div class="totals-label">Tax</div><span id="tax" class="price price-total">0.00</span></li>
										<li><div class="totals-label">Total</div><span id="total" class="price price-total">0.00</span></li>
									</ul>
								</section>
						</div>
						<div class="section-footer actions">
							<div id="payment-actions" class="payment-actions">
								<!--<button id="action-credit" class="btn action btn-disabled" disabled="disabled">Credit</button>-->
								<button id="action-cash" class="btn btn-pos-payment payment-type">Cash</button>

								<?php
								/* Card Payment types */
								echo '<ul class="card-payment-types unstyled-list">';

								// The debit button.
								$data = array(
									'id'		=> 'action-debit',
									'class'	=> 'action-debit btn payment-type',
									'name'	=> 'action_debit',
									'content'	=> 'Debit',
								);
								echo '<li>' . form_button($data) . '</li>';

								// The credit button
								$data = array(
									'id'		=> 'action-credit',
									'class'	=> 'action-credit btn payment-type',
									'name'	=> 'action_credit',
									'content'	=> 'Credit',
								);
								echo '<li>' . form_button($data) . '</li>';

								echo '</ul><!-- END .card-payment-types -->';
								
								
								// Do not show the on account button for the CAFETERIA storefront.
								log_message('debug', 'VIEW: store | ' . count($accounts) . ' accounts from the controller.');
								if (STOREFRONT === 'CAFETERIA') {						
								?>
								<select id="action-account" class="btn btn-accounts payment-type">
									<option class="opt-on-account" value="-1" selected="selected">On Account</option>
									<?php
									$count = 0;
									foreach ($accounts as $a) {
										$id = $a->person_id;
										$accountId = $id;
										$name = $a->first . ' ' . $a->last;
										$type = $a->type;

										if ($type == 'MEAL_PLAN' || $type == 'PRE_PAID' || $type == 'STAFF') {
											$count++;
											echo '<option class="meal-plan-account opt-on-account"';
											echo 'value="' . $id . '">';
											echo $name . ' (' . $a->mp_balance . ') - ';
											if ($type == 'MEAL_PLAN') echo 'HMP';
											if ($type == 'PRE_PAID') echo 'LRPP';
											if ($type == 'STAFF') echo 'ST';
											echo '</option>';
										}
									}
									log_message('debug', 'VIEW: store | ' . $count . ' accounts available in the UI.');
									?>
								</select>
								<?php
								
								} else if (STOREFRONT === 'COFFEESHOP') {
									// A opening non-value.
									$options = array(-1 => 'The Bean Tab');
									// Create an array to hold the options (accounts)
									foreach ($accounts as $a) {
										$name = $a->first . ' ' . $a->last . ' (' . $a->coffeeshop_balance . ')';
										$options[$a->person_id] = $name;
									}
									// Some other attributes for the select box.
									$attributes = array(
										'id'	=> 'action-coffeeshop-tab',
										'class'	=> 'btn btn-accounts payment-type',
									);
									// Paint the drop down onto the screen.
									echo form_dropdown('action_coffeeshop_tab', $options, -1, $attributes);
									log_message('debug', 'VIEW: store | ' . sizeof($options) . ' The Bean accounts available in the UI.');
								}
								?>
							</div>
							<div class="complete-transaction-buttons">
								<input id="complete-transaction" class="btn btn-submit btn-primary" type="submit" name="complete_transaction" value="Complete"></input>
								<button id="clear-cart" class="btn-submit btn" type="button" onclick="clearCart()">Void</button>
							</div>
						</div>
					</section>
				</div>
			</form>
		</div>

		<?php

		if (STOREFRONT == 'CAFETERIA'):

		?>
		<section class="on-account-legend store-block" style="width: 31.76157%; float: right;">
			<section class="section-header">
			<h3>On Account Legend</h3>
			</section>
			<section class="section-body">
			<dl class="unstyled-list">
				<dt>Heritage Meal Plan</dt>
				<dd class="price">HMP</dd>
				<dt>Lily Ruth Pre-Paid Account</dt>
				<dd class="price">LRPP</dd>
				<dt>Staff & Faculty Accounts<dt>
				<dd class="price">ST</dd>
			</dl>
			</section>
		</section>

		<?php

		endif;

		include('modals/transaction-summary-cash.php');
		include('modals/transaction-summary-debit.php');

		?>
	</div>

<!-- End of the individual view -->

<?php $this->load->view('parts/footer'); ?>
