<div class="panel-short-form">
<?php

// Open the form.
echo form_open('manage/transferBalance', 'id="form-transfer-balance" class="form-short form-transfer-balance hidden" novalidate');

// Amount field and label.
echo form_label('Amount', 'form-transfer-balance-amount');
$extra = array(
  'id'        => 'form-transfer-balance-amount',
  'pattern'   => '\d+(\.\d{1,2})?',
  'required'  => 'required',

);
echo form_input('amount', '', $extra);
echo '<span class="input-error-message"></span>';

// Date picker
echo form_label('Date', 'form-transfer-balance-transaction-date');
echo '<span class="text-small">yyyy-mm-dd or yyyy/mm/dd | </span>';
echo '<input id="form-transfer-balance-transaction-date" name="transaction_date" value="' . date('Y-m-d') . '"';
echo 'pattern="^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$" type="date" onfocus="this.select();" required="required"/>';
echo '<span class="input-error-message"></span>';

// Recipient account dropdown.
echo form_label('Recipient Account', 'form-transfer-balance-recipient');
echo '<select id="form-transfer-balance-recipient-account" name="recipient_account" required="required">';
echo '<option value="-1" selected>Select Account</option>';
foreach ($recipientAccounts as $recipient) {
  $name = $recipient->last . ', ' . $recipient->first;
  $displayText = $name . '  (' . $recipient->person_id . ')';
  echo '<option value="' . $recipient->person_id . '"';
  echo '>' . $displayText . '</option>';
}
echo '</select>';
echo '<span class="input-error-message"></span>';

// The hidden account id field.
echo form_hidden('person_id', $account->person_id);

// Submit.
echo form_submit('submit', 'Save', 'class="btn-primary"');
echo form_button('btn-transfer-balance-cancel', 'Cancel', 'class="btn-transfer-balance-cancel" onclick="toggleFormTransferBalance()"');

// Close the form.
echo form_close();

?>
</div>
