<?php

class Tools extends CI_Controller {

	public function __construct() {
		// Call the parent constructor
		parent::__construct();

		// Load up the appropriate models.
    $this->load->model('Sync');
	}


  public function message($to = 'World')
  {
      log_message('debug', "Hello {$to}!".PHP_EOL);
  }








  public function nightly($key = false, $frondEnd = false) {
    log_message('debug', 'CONTROLLER: Tools | nightly()');
    $message = "\n\n";
    $message .= "\t" . '**********************************' . "\n";
    $message .= "\t" . '        HERITAGE CAFETERIA        ' . "\n\n";
    $message .= "\t" . 'BEGIN nightly maintenance routines' . "\n";
    $message .= "\t" . '<nightly>' . "\n";
    $message .= "\t" . '**********************************';
		log_message('debug', $message);
	  $beforeSync = now();


    /*
      Execute maintenance routines.
     */
    if ($key == getNightlyMaintenanceKey()) {
      log_message('debug', 'CONTROLLER: Tools | Nightly maintenance key accepted.');

      // Sync supporting data.
      $this->Sync->updateSupportingData();
      // Update the local accounts from Populi.
      $this->Sync->updateLocalAccounts();

      if (ENVIRONMENT === 'production') {
        log_message('debug', 'CONTROLLER: Tools | Looks like we are in production, so let\'s also update the balances in Populi.');
        // Pass new cafeteria balance back to Populi.
        $this->Sync->updatePopuliAccountBalances();
        // Update the coffee shop tab balance to Populi.
        $this->Sync->updatePopuliCoffeeshopTabBalances();
      } else {
				log_message('debug', 'CONTROLLER: Tools | We are not in production, DO NOT update the account balances in Populi.');
			}
			
			$this->Sync->updateAccountMetaData();
    } else {
      log_message('error', 'CONTROLLER: Tools | The key supplied was NOT correct.');
    }


    // Calculate the time taken.
    $afterSync = now();
    $timeToSync = $afterSync - $beforeSync;
    $this->Sync->updateLastFullSyncTimestamp();

    // Log the end results.
    $message = "\n";
    $message .= "\t" . '**********************************' . "\n";
    $message .= "\t" . '        HERITAGE CAFETERIA        ' . "\n\n";
    $message .= "\t" . 'END nightly maintenance routines' . "\n";
    $message .= "\t" . 'Total time: ' . $timeToSync . " seconds.\n";
    $message .= "\t" . '</nightly>' . "\n";
    $message .= "\t" . '**********************************' . "\n";
    log_message('debug', $message);
  }





}
