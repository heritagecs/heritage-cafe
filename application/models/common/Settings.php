<?php
/**
 * A PHP Class for negotiating sessions and data with the Populi database.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Model {

	public function __construct() {
		// Call the parent constructor
		parent::__construct();





	}




  /**
   * Gets a the data for a specific setting from the database.
   * @param  String $key  The alpha numerical key for the setting.
   * @return object       A settings object.
   */
  public function getSetting($key) {
    log_message('info', 'MODEL: Settings | getSetting().  Get a specific setting out of the database.');

    // Query the database
    $this->db->where('key', $key);
    $query = $this->db->get('settings');

    // If the first row return by the query has data return that data.
    ($query->row()) ? $row = $query->row() : $row = false;
		if (!$row) log_message('error', 'MODEL: Settings | A value for the setting ' . $key . ' was not found.');
    return $row;
  }


  /**
   * Gets all the settings for this application from the database.
   * @return Array      An array of settings objects.
   */
  public function getSettings() {
    log_message('info', 'MODEL: Settings | getSettings().  Get all the settings out of the database.');

    // Query the database.
    $query = $this->db->get('settings');

    // Put the results into an array.
    $result = $query->result();

    // Return the results.
    return $result;
  }


	/**
	 * Simply return the value that is associated with this setting.
	 * @param  [String] $key The text id of the setting in question.
	 * @return [String]      Text representation of the setting's value.  Could be a json string.
	 */
	public function getSettingValue($key) {
		log_message('info', 'MODEL: Settings | getSettingValue()');

		// Query the database
		$this->db->select('value');
		$this->db->where('key', $key);
		$query = $this->db->get('settings');

		// If the first row return by the query has data return that data.
		($query->row()) ? $row = $query->row()->value : $row = false;
		if (!$row) log_message('error', 'MODEL: Settings | A value for the setting ' . $key . ' was not found.');
		return $row;
	}











	/**
	 * Updates a setting's value in the database.
	 * @param [type] $key   [description]
	 * @param [type] $value [description]
	 */
	public function setSettingValue($key, $value) {
		// Check if this setting exists, if not, create it.
		if (!$this->Settings->getSetting($key)) {
			$this->db->insert('settings', array('key' => $key));
		}

		// Now, update the value of this setting.
		$this->db->where('key', $key);
		$this->db->update('settings', array('value' => $value));

		// Check that the setting was updated.
		if ($this->getSetting($key)->value == $value) {
			log_message('debug', 'MODEL: Settings | The setting for ' . $key . ' was updated successfully to ' . $value . '.');
			return true;
		} else {
			log_message('debug', 'MODEL: Settings | There was an error updating the ' . $key . ' address.');
			return false;
		}
	}





	/**
	 * Put together an array of tag objects that gives the caller a list of the approved osap tags and their id's.
	 * @return Array			An array of tag objects.
	 */
	public function getOsapTagsApproved() {
		log_message('debug', 'MODEL: Settings | getOsapTagsApproved()');

		// An empty array.
		$tags = array();

		// Get the value for all tags and for approved tags.
		if ($this->Settings->getSettingValue('osap_tags')) {
			$allTags = json_decode($this->Settings->getSettingValue('osap_tags'));
		} else {
			// If this setting does not exist, set it with data from Populi.
			$this->PopuliApiExtended->setOsapTags();
			$allTags = json_decode($this->Settings->getSettingValue('osap_tags'));
		}

		// Get the vlaue for the approved tags if there is one, otherwise, create the setting.
		if ($this->Settings->getSettingValue('osap_tags_approved')) {
			$approvedTags = json_decode($this->Settings->getSettingValue('osap_tags_approved'));
		} else {
			// If this setting does not exist, create it with a null value.
			$this->Settings->setSettingValue('osap_tags_approved', null);
			$approvedTags = array();
		}


		// Loop over all the tags to see if any of them are in the approved tags list.
		foreach ($allTags as $t) {
			if (in_array($t->id, $approvedTags)) {
				log_message('info', 'MODEL: Settings | Tag ' . $t->id . ' is one of the approved tags.');
				array_push($tags, $t);
			}
		}

		// Return the array of approved tags.
		return $tags;
	}






}
