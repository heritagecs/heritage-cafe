<?php $this->load->view('parts/header'); ?>

<!-- Start of the individual view -->
	<div id="view-manage" class="site-content-inner view-manage">
    <?php include('parts/nav-manage.php'); ?>

		<h2>Inventory</h2>

    <h3>Create New Item</h3>

		<form id="unittest-form" action="<?php base_url(); ?>/save/inventory" method="post">
      <div class="form-input-area">
  			<fieldset class="form-radio">
  				<legend>Tax Code</legend>
  				<ul>
  					<li>
  						<input id="tax-code1" name="tax_code" value="1" type="radio" />
  						<label for="tax-code1">Taxable</label>
  					</li>
  					<li>
  						<input id="tax-code2" name="tax_code" value="2" type="radio" />
  						<label for="tax-code2">PST Exempt</label>
  					</li>
  					<li>
  						<input id="tax-code3" name="tax_code" value="3" type="radio" />
  						<label for="tax-code3">Prepared Food</label>
  					</li>
  				</ul>
  			</fieldset>
      </div>

      <div class="form-footer">
        <input id="save-category-button" class="btn-submit btn-primary" name="commit" value="Save Item" type="submit"></input>
        <a class="btn-submit btn" href="<?php base_url(); ?>/inventory">Cancel</a>
      </div>
		</form>

  </div><!-- #view-manage DIV -->

<!-- End of the individual view -->

<?php $this->load->view('parts/footer'); ?>
