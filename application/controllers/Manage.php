<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends CI_Controller {

  /**
   * Constructor
   */
  public function __construct() {
  	// Call the parent constructor
  	parent::__construct();

    // Load up the appropriate models.
	  $this->load->model('User');
    $this->load->model('Role');
    $this->load->model('populi/PopuliApi');
    $this->load->model('Account');
    $this->load->model('Sync');
    $this->load->model('Transaction');
    $this->load->model('InventoryTable');
    $this->load->model('InventoryCategory');
    $this->load->model('PaymentType');
    $this->load->model('CoffeeshopTab');

		// Check if the user is logged in.
		$this->User->is_logged_in();

    // Check if the user is an admin or manager.
    if (!$this->Role->validateAccess(array('manager', 'admin'))) {
      redirect('store');
    }

    define('STOREFRONT', $_COOKIE['storefront']);
    log_message('debug', 'Manage | STOREFRONT = ' . STOREFRONT);
  }



	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
    // Send them to the inventory method.
    redirect('manage/accounts');
	}



  public function accounts() {
    // Get the accounts data.
    $data['accounts'] = $this->Account->getAccounts('first', 'ASC', false);
    $data['accountsActive'] = $this->Account->getAccounts('id', 'ASC', true);
    $data['lastFullSync'] = $this->Sync->getLastFullSyncTimestamp();
    $data['countMealPlanAccounts'] = sizeof($this->Account->getAccountsMealPlan());
    $data['filter_type'] = $this->input->get('type');

    // Load the view.
		$this->load->view('manage/accounts', $data);
  }




  /**
   * Retrieve account and transaction history details for this account and feed that data to the view.
   * @param  [int] $personId        A numberical person id.
   */
  public function accountDetails($personId, $transactionId = null) {
    log_message('debug', 'CONTROLLER: Manage | accountDetails()');

    // Generate the balance for each transaction in accountHistory.
    // Get a list of transactions for this account for the current storefront.
    $accountHistory = $this->Account->getAccountHistory($personId, STOREFRONT);
    // Set the initial balance to 0.
    $balance = 0;
    log_message('debug', 'CONTROLLER: Manage | Calculating the balance for each transaction in accountHistory...');
    foreach ($accountHistory as $t) {
      if ($t->transaction_type == 0) {
        $balance -= $t->total;
        $t->account_balance = $balance;
      } else {
        $balance += $t->total;
        $t->account_balance = $balance;
      }
    }

    // Round the balance to two decimal places.
    $balance = round($balance, 2);
    // Format the balance with number format.
    number_format($balance, 2);

    // Set up the data for this account.
    $data['account'] = $this->Account->getAccount($personId);
    $data['accountHistory'] = $accountHistory;
    $data['recipientAccounts'] = $this->Account->getAccounts();
    $data['paymentTypes'] = $this->PaymentType->getPaymentTypes();
    $data['balanceCalculated'] = $balance;

    // Load the view for this individual.
    $this->support->loadView('manage/account-details', $data);
  }





  /**
   * Sets up the data and calls the view for managing a specific transaction.  transactions
   * can be against an account or not.
   * @param  int $transId     A numerical transaction id.
   */
  public function transaction($transId) {
    log_message('debug', 'CONTROLLER: Manage | transaction()');

    /* Check the action from the post data */
    $action = $this->input->post('action');
    $key = $this->input->post('key');
    if ($action == 'void-transaction' && $key == 'RL6J7QWaUsZqYwTC') {
      log_message('debug', 'CONTROLLER: Manage | We have been asked to void transaction ' . $transId);

      // Punch it.
      $voidingTransId = $this->Transaction->voidTransaction($transId);

      // Log something.
      if ($voidingTransId) {
        log_message('debug', 'CONTROLLER: Manage | Transaction ' . $transId . ' has been voided by transaction ' . $voidingTransId);
      }

      $account = $this->Transaction->getTransaction($transId)->account_id;

      redirect('manage/accountDetails/' . $account);
    }

    // Get some data about this transaction.
    $data['transaction'] = $this->Transaction->getTransaction($transId);
    $data['items_sold'] = $this->Transaction->getItemsSoldInTransaction($transId);
    $data['payment_type'] = $this->PaymentType->getPaymentTypes();

    /* Get some other data depending on stuff */
    $transaction = $data['transaction'];
    if (!is_null($transaction->account_id)) {
      $data['account'] = $this->Account->getAccount($transaction->account_id);
    }

    // Ship this stuff off to the view.
    $this->support->loadView('manage/transaction-details', $data);
  }








  /**
   * A more generic function to allow me to cause change to an account from the UI.
   */
  public function updateAccount($personId) {
    log_message('info', 'CONTROLLER: Manage | updateAccount()');

    // Get the action query string.
    $action = $this->input->get('action');

    if ($action == 'update-populi-remote-balance') {
      // If the action is to update the Populi balance, then do that.
      $this->MealPlan->updatePopuliAccountBalance($personId);
    } else if ($action == 'update-coffeeshop-tab-remote-balance') {
      // We are updating Populi with the current coffeeshop tab balance.
      // Get the current balance out of the database.
      $account = $this->Account->getAccount($personId);
      // Attempt to update Populi - if successful, this will also update the cached value.
      $this->CoffeeshopTab->setBalance($personId, $account->coffeeshop_balance);      
    }

    if ($action == 'get-populi-remote-meal-plan') {
      // If the action is to sync remote meal plan data.
      $this->Sync->updateLocalAccount($personId, true);
    }

    // Redirect to the account details page.
    redirect('manage/accountDetails/' . $personId);
  }


















  public function featured() {
		// Go get the full inventory data from the model and get the list of categories.
		$inventory = $this->InventoryTable->getItems();
		$cats = $this->InventoryCategory->getCategories();
    $featuredItems = $this->InventoryTable->getFeaturedItems();

		// Build the data array.
		$data = array(
			'inventory' 	  => $inventory,
			'categories'	  => $cats,
      'featuredItems' => $featuredItems,
		);

    $this->load->view('manage/featured', $data);
  }




  /**
   * Responds to a button the account details view and updates the account balance to reflect the calculated
   * balance based on actual transactions.
   * @param  [int] $personId        A numberical person id.
   * @param  [float] $newBalance    The value that the balance will be overwritten to.
   */
  public function oneTimeBalanceUpdate($personId, $newBalance) {
    log_message('debug', 'CONTROLLER: Manage | oneTimeBalanceUpdate().');

    // Ask the model to overwrite the balance.
    $this->Account->overwriteBalance($personId, $newBalance);

    redirect('manage/accountdetails/' . $personId);
  }





  public function addTransaction() {
    log_message('debug', 'CONTROLLER: Manage | addTransaction()');

    // Gather POST data.
    $personId     = $this->input->post('person_id');
    $amount       = abs($this->input->post('amount'));
    $transDate    = $this->input->post('transaction_date');
    $transType    = (int)$this->input->post('transaction_type');
    $payType      = (int)$this->input->post('payment_type');
    $memo         = $this->input->post('memo');

    // Tell the log about the data we retrieved.
    log_message('debug', 'CONTROLLER: Manage | Data: ' . $personId . ' ' . $amount . ' ' . $transDate . ' ' . $transType . ' ' . $payType . ' ' . $memo);

    // Add the transaction.
    $object = (object)array(
      'total'             => $amount,
      'payment_type'      => $payType,
      'storefront'        => $_COOKIE['storefront'],
      'date'              => $transDate,
      'transaction_type'  => $transType,
      'account_id'        => $personId,
      'memo'              => $memo,
    );
    $result = $this->Transaction->addTransaction($object);

    // Update the account balance.
    if ($result) {
      if (STOREFRONT === 'CAFETERIA') $this->Account->updateBalance($personId, $amount, $transType);
      if (STOREFRONT === 'COFFEESHOP') $this->Account->updateCoffeeShopTab($personId, $amount, $transType);
    }

    // Redirect to the account details page.
    redirect('manage/accountDetails/' . $personId);
  }






  public function zeroAccount() {
    log_message('debug', 'CONTROLLER: Manage | debitAccount()');

    // Gather POST data.
    $personId     = $this->input->post('person_id');

    // Ask the model to do the work.
    $this->Account->zeroCafeAccount($personId);
  }




  public function transferBalance() {
    log_message('debug', 'CONTROLLER: Manage | transferBalance() START');

    // Gather POST data.
    $personId         = $this->input->post('person_id');
    $amount           = $this->input->post('amount');
    $transactionDate  = $this->input->post('transaction_date');
    $recipientAccount = $this->input->post('recipient_account');

    log_message('debug', 'CONTROLLER: Manage | Data: ' . $personId . ' ' . $amount . ' ' . $recipientAccount . ' ' . $transactionDate);

    // Add a transaction showing a credit to the recipient account.
    $memo = 'Transfer from ' . $personId;

    // Prep and add the first transaction.
    $object = (object)array(
      'total'             => $amount,
      'payment_type'      => 1,
      'storefront'        => $_COOKIE['storefront'],
      'date'              => $transactionDate,
      'transaction_type'  => 1,
      'account_id'        => $recipientAccount,
      'memo'              => $memo,  
    );
    $t1 = $this->Transaction->addTransaction($object);
    // If add the first transaction was successful, update the balance.
    if ($t1) $this->Account->updateBalance($recipientAccount, $amount, 1);

    // Add a transaction showing a debit to the sender account.  Second transaction.
    $object = (object)array(
      'total'             => $amount,
      'payment_type'      => 1,
      'storefront'        => $_COOKIE['storefront'],
      'date'              => $transactionDate,
      'transaction_type'  => 0,
      'account_id'        => $personId,
      'memo'              => $memo,  
    );
    $memo = 'Transfer to ' . $recipientAccount;
    $t2 = $this->Transaction->addTransaction($object);
    // If add the second transaction was successful, update the balance.
    if ($t2) $this->Account->updateBalance($personId, $amount, 0);

    // Log a message related to the outcome.
    if ($t1 && $t2) {
      log_message('debug', 'CONTROLLER: Manage | Both transactions related to transfering funds from account ' . $personId . ' to ' . $recipientAccount . ' posted successfully.  END');
    } else {
      log_message('debug', 'CONTROLLER: Manage | Something bad happened when were attempted to transfer funds from ' . $personId . ' to ' . $recipientAccount . '.  END');
    }

    // Redirect to the account details page.
    redirect('manage/accountDetails/' . $personId);
  }



  /**
   * Change the inventory item for a feature item slot.
   * @param  [type] $id     The feature item slot that will be changed.
   * @param  [type] $itemId The inventory_id of the item to change to.
   */
  public function changeFeaturedItem($id, $itemId = null) {
    // If this is not an editable item.
    if ($id < 1 || $id > 8) {
      redirect('/manage/featured');
    }

		// If no item id was passed, assume that we are getting this value from POST data.
    if ($itemId == null) {
      $itemId = $this->input->post('items-list-complete');
      log_message('debug', 'The ID of the new featured item will be: ' . $itemId);
    }

    // Make sure this item is in the inventory.
    $checkThis = $this->InventoryTable->getItem($itemId);
    if ($checkThis == null) {
      redirect('/manage/featured');
    }

    // Update the database
    $this->InventoryTable->setFeaturedItem($id, $itemId);

    // redirect
    redirect('/manage/featured');
  }






  public function moveFeaturedItemUp($id) {
    log_message('debug', 'moveFeaturedItemUp(), ID passed is: ' . $id);

    // Make sure the id is one of the acceptable id's.
    if ($id >= 2 && $id <= 8) {
      log_message('debug', 'The ID is an acceptable value.');
    } else {
      log_message('error', 'The ID is NOT an acceptable value.');
      redirect('/manage/featured');
    }

    // Move the item up.
    $this->InventoryTable->moveFeaturedItem($id, $id - 1);

    // redirect
    redirect('/manage/featured');
  }


  public function moveFeaturedItemDown($id) {
    log_message('debug', 'moveFeaturedItemDown(), ID passed is: ' . $id);

    // Make sure the id is one of the acceptable id's.
    if ($id >= 1 && $id <= 7) {
      log_message('debug', 'The ID is an acceptable value.');
    } else {
      log_message('error', 'The ID is NOT an acceptable value.');
      redirect('/manage/featured');
    }

    // Move the item up.
    $this->InventoryTable->moveFeaturedItem($id, $id + 1);

    // redirect
    redirect('/manage/featured');
  }









  /**
   * Call the update method to get new meal plan data from Populi.
   */
  public function updateLocalAccounts() {
    // Update supporting data.
    $this->Sync->updateSupportingData();
    // Update local account data.
    $this->Sync->updateLocalAccounts();
    redirect('/manage/accounts');
  }

  /**
   * Calls the update method in the Sync controller to update the balances in Populi.
   */
  public function updatePopuliAccountBalances() {
    $this->Sync->updatePopuliAccountBalances();
    redirect('/manage/accounts');
  }
  public function updatePopuliCoffeeshopTabBalances() {
    $this->Sync->updatePopuliCoffeeshopTabBalances();
    redirect('/manage/accounts');
  }


  /**
   * syncAccounts Syncs meal plan accounts between Populi and the Heritage Cafe.
   */
  public function syncAccounts() {
    $this->load->model('Sync');
    log_message('debug', '-----> CONTROLLER: Manage->syncAccounts()');

    $this->Sync->fullAccountSync();

    // Redirect.
    redirect('/manage/accounts');
  }
}
