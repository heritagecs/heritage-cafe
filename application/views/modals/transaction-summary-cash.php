<!-- Cash modal -->
<div id="modal-transaction-summary" class="modal">
	<div id="modal-cash" class="modal-dialog modal-cash">
		<section>
			<header class="section-header"><h3>Transaction Summary - Cash</h3></header>
			<div class="section-body">
				<ul>
					<li><span class="modal-label">Cash Received</span><span id="modal-cash-received" class="modal-price price">20</span></li>
					<li><span class="modal-label">Change Required</span><span id="modal-change-required" class="modal-price price">4.35</span></li>
				</ul>
				<a href="<?php site_url(); ?>/store" id="modal-ok" class="btn btn-primary">Ok</a>
			</div>
		</section>
	</div>
</div><!-- END #modal-transaction-summary-->
