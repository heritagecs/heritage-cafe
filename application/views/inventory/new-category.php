<?php $this->load->view('parts/header'); ?>

<!-- Start of the individual view -->
	<div id="view-manage" class="site-content-inner view-manage">
    <?php $this->load->view('nav/manage'); ?>

		<h2>Inventory</h2>

    <h3>Create New Category</h3>

		<form id="create-new-category" action="<?php base_url(); ?>/save/inventory" method="post">
			<div class="form-input-area">
				<label class="form-label" for="category-name">Category Name</label>
				<input id="category-name" class="required validation-invalid" name="category_name" type="text"></input>

				<label class="form-label" for="category-description">Description</label>
				<textarea id="category-description" name="category_description"></textarea>
			</div>

			<div class="form-footer">
				<input id="save-category-button" class="btn-submit btn-primary" name="commit" value="Save Category" type="submit"></input>
				<a class="btn-submit btn" href="<?php base_url(); ?>/inventory">Cancel</a>
			</div>
		</form>
  </div><!-- #view-manage DIV -->

<!-- End of the individual view -->

<?php $this->load->view('parts/footer'); ?>
