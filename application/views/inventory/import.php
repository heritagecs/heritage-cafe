<?php $this->load->view('parts/header'); ?>

<!-- Start of the individual view -->
	<div id="view-manage" class="site-content-inner view-manage">
    <?php $this->load->view('nav/manage'); ?>

		<h2>Inventory</h2>

    <h3>Import Items from CSV</h3>

    <p>Paste comma separated text from a CSV file into the box below, or write new data.
    The list should be in the order noted below.</p>

    <p><code>item description, category, price, tax code</code></p>

		<form id="import-csv" action="<?php base_url(); ?>/save/inventory" method="post">
      <div class="form-input-area">
        <label class="form-label" for="import-csv">CSV Data</label>
				<textarea id="import-csv" name="import_csv"></textarea>
      </div>

      <div class="form-footer">
        <input id="import-csv-button" class="btn-submit btn-primary" name="commit" value="Import" type="submit"></input>
        <a class="btn-submit btn" href="<?php base_url(); ?>/inventory">Cancel</a>
      </div>
		</form>

  </div><!-- #view-manage DIV -->

<!-- End of the individual view -->

<?php $this->load->view('parts/footer'); ?>
