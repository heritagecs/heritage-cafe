<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Model for the Users api controller.  Gets users from the database
 */

class TaxCode extends CI_Model {

	public function __construct() {

		// Call the parent constructor
		parent::__construct();
	}




	/**
	 * [getTaxCodes Returns an array of tax codes objects]
	 * @param  [type] $getActiveTaxCodes [description]
	 * @return [type]                    [description]
	 */
	public function getTaxCodes() {
		// Build the query.
		$this->db->select('id, label, code');
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('tax_code');

		$result = $query->result();

		return $result;
	}


	/**
	 * [getTaxCode accepts the label/name of a tax code and returns the numberic code]
	 * @param  [type] $label [the label or name of the tax code]
	 * @return [type]        [the numberic id of the code]
	 */
	public function getTaxCodeId($label) {
		//Build the query.
		$this->db->select('code');
		$this->db->where('label', $label);
		$query = $this->db->get('tax_codes');

		// Put the result of the query into a variable.
		$code = $query->row()->code;

		// Return the tax code value.
		return $code;
	}



}
