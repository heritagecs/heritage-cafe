<div class="hcs-login-form">
  <header>
    <div class="hcs-logo-in-shield">
    	<img src="<?php echo base_url(); ?>assets/img/heritage-logo-square-simple-color.svg" />
    </div>

    <h1>Food Services</h1>
  </header>

  <main>

  <?php
  echo form_open('auth/login');
  echo form_label('Username', 'login-username');
  echo form_input('username', '', 'id="login-username" onfocus="this.value=\'\'" autofocus');
  echo form_label('Password', 'login-password');
  echo form_password('password', '', 'id="login-password" onfocus="this.value=\'\'"');
  echo form_submit('submit', 'Login', 'class="btn btn-primary"');
  ?>

  </main>
</div><!-- End of .hcs-login-form -->
