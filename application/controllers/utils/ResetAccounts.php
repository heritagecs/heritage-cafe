<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ResetAccounts extends CI_Controller {

  public function __construct() {
    // Call the parent constructor
		parent::__construct();

    // Load up the appropriate models.
    $this->load->model('User');
    $this->load->model('Role');
    $this->load->model('MealPlan');
    $this->load->model('PopuliApi');
    $this->load->model('Account');
    $this->load->model('Transaction');
    $this->load->model('Sync');
    $this->load->model('InventoryTable');
    $this->load->model('Sales');
    $this->load->model('MealPlan');
    $this->load->model('AcademicTerms');
    $this->load->model('PaymentType');

    // Check if the user is logged in.
		$this->User->is_logged_in();

    // Check if the user is an admin or manager.
    if (!$this->Role->validateAccess(array('admin'))) {
      redirect('auth');
    }
  }

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
    log_message('debug', '-----> CONTROLLER: Unit->index().');
    $this->load->view('parts/header-test');
 
    // Three keys
    $key1 = 'saEu8KCjY3gZ3WQmumTq6K5E';
    $key2 = 'zAyAfSVDpmHddQkPWBabvybZRSxcXsTmD8D8BddRepp54qKk';
    $key3 = 'as9LfEkakVKwQpLaqYGaXfEc';

    // ?uid=saEu8KCjY3gZ3WQmumTq6K5E&key=1035b50e8433bfcea5ade1760922cf37a6838e90&random=c470ea838d5b55730df909c9f4fd6558d77996de

    // Placeholder for the validation, and get the inputs from GET
    $valid = false;
    $key1Input = $this->input->get('uid');
    $key2Input = $this->input->get('key');
    $key3Input = $this->input->get('random');
    
    // If the first key matches and thesha1 values of the 2nd and 3rd keys match, then we have validation and we can run the zeroing function.    
    if (
      $key1 == $key1Input &&
      sha1($key2) == $key2Input &&
      sha1($key3) == $key3Input
    ) $valid = true;

    // Set a couple of counters we want to track and a result tracker.
    $count = 0;
    $countSuccess = 0;
    $result = false;

    // Get the list of accounts sorted by person_id.
    $data = $this->Account->getAccounts('person_id', 'ASC', false);   
    log_message('debug', 'Resetting ' . sizeof($data) . ' cafeteria accounts.');

    if ($valid) {
      /*
      Loop over each person in the getAccount record.
       */
      foreach ($data as $account) {
        // Increment the counter and setup some useful variables.
        $count++;
        $fullName = $account->first . ' ' . $account->last;
        $personId = $account->person_id;
  
        /* Process the transaction and get the result */
        log_message('debug', $count . '. Resetting the cafeteria account for ' . $fullName . ' (' . $personId . ').');
        $result = $this->Account->zeroCafeAccount($personId);
  
        // Add some more entries to the log based on the result and put something in the UI as well.
        if ($result) {
          $countSuccess++;
          $message = 'SUCCESS';
        } else {
          $message = 'FAILURE';
        }
        log_message('debug', '  ' . $message);
        echo '<p>' . $count . '. Resetting the cafeteria account for ' . $fullName . ' (' . $personId . ').  ' . $message . '.  The balance was $' . $account->mp_balance .'.</p>';
      }
    } else {
      log_message('debug', 'Account resetting DISABLED as the processing keys are INVALID.');
    }

    // Finish up.
    log_message('debug', 'In attempting to reset ' . $count . ' accounts, we successfully reset ' . $countSuccess . '.');

		$this->load->view('parts/footer');
	}
}
