<?php
/**
 * A PHP Class for processing meal plans and their balances for student accounts.
 */


class CoffeeshopTab extends CI_Model {

	public function __construct() {
		// Call the parent constructor
		parent::__construct();

		// Load the appropriate models
		$this->load->model('populi/PopuliConnect');
		$this->load->model('populi/PopuliApi');
		$this->load->model('populi/PopuliApiExtended');
    }
    



    public function getAccounts($order_by = 'last', $order = 'ACS') {
        log_message('debug', 'MODEL: CoffeeshopTab | getAccounts()');
        
        // Make sure were only query for accounts with a balance in the Coffeeshop_balance field.
        $this->db->where('coffeeshop_balance <>', 0);
        // Query the data for this specific item's record.
        $this->db->order_by($order_by, $order);
        // If we are ordering by first name, also order by last name.
        if ($order_by == 'first') $this->db->order_by('last', $order);
        // Get the results.
        $query = $this->db->get('account');
    
        // Put the results into an array.
        $result = $query->result();
    
        // Return the array
        return $result;
    }




    

    /**
     * Push the tab balance to Populi in the custom field.
     */
    public function setBalance($personId, $value) {
        log_message('info', 'MODEL: CoffeeshopTab | setBalance()');

        // Go get the custom field id.
		$customFieldId = $this->PopuliApiExtended->getCustomFieldId('The Bean Tab');

        // Try the query.        
        $try = $this->PopuliApi->setCustomField($personId, $customFieldId, $value);
        if ($try) {
            log_message('debug', 'MODEL: CoffeeshopTab | SUCCESS updating Coffee Shop Tab balance in Populi for ' . $personId . ' to ' . $value);
            // We successfully updated Populi, update our local balance too.
            $this->setCachedBalance($personId, $value);
            return true;
        } else {
            log_message('debug', 'MODEL: CoffeeshopTab | FAILED updating Coffee Shop Tab balance in Populi for ' . $personId);
            return false;
        }
    }
    /**
     * Update the locally cached balance.
     */
    public function setCachedBalance($personId, $value) {
        log_message('info', 'MODEL: CoffeeshopTab | setCachedBalance()');
        // Perform the update.
        $this->db->where('person_id', $personId);
        $try = $this->db->update('account', array('coffeeshop_balance_populi' => $value));
        // Return the result of the update.
        return $try;
    }














}