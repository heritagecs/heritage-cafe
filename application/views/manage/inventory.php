<!-- Start of the individual view -->
<div id="view-inventory" class="site-content-inner view-inventory">

<?php
$show_inactive = (boolean)$this->input->get('show_inactive');

echo '<h1>Inventory';
if ($show_inactive) {
	echo ' - Inactive';
}
echo '</h1>';

?>

	<a class="btn btn-primary" href="<?php base_url(); ?>/inventory/newitem">Add New Item</a>
  	<a class="btn" href="<?php base_url(); ?>/inventory/newcategory">Add New Category</a>
	<a class="btn" href="<?php base_url(); ?>/inventory/import">Import from CSV...</a>
	
<?php
	if ($show_inactive) {
		echo '<a href="' . base_url() . 'inventory">Show active items</a>';
	} else {
		echo '<a href="' . base_url() . 'inventory?show_inactive=true">Show inactive items</a>';
	}

	// See if there is a query string.	
	$show_inactive = (boolean)$this->input->get('show_inactive');

	// Process through each category
	foreach ($categories as $cat) {
		// Setup variables
		$catId		= $cat->id;
		$name 		= $cat->category;
		$desc		= $cat->description;

		// Open the category.
		echo '<article class="manage-category">';
		// Open a list item for the category.
		echo '<section class="manage-category-header">';
		echo '<h2>' . $name . '</h2>';
		echo '<span class="edit-category-link">( ';
		//echo '<a href="' . base_url() . 'inventory/editcategory/' . $catId . '">';
		echo '<a href="javascript: void(0)" class="a-strikethrough">';
		echo 'Edit</a> )</span>';
		echo '<span class="manage-category-description">' . $desc . '</span>';
		echo '</section>';

		// A section for the items.
		echo '<section class="manage-category-items">';

		// Fill in the items for this category.
		// Process through the inventory table's data.
		foreach ($inventory as $row) {
			// Setup variables for this row.
			$itemId	= $row->id;
			$itemCategoryId = $row->category_id;
			$itemName		= $row->description;
			$tax 		= $row->tax_code;
			$price	= number_format((float)$row->price, 2, '.', '');

			// If the category is equal to the item's category
			// Create a new list item and put an unordered list inside.
			if ($catId == $itemCategoryId) {

				echo '<ul>';
				echo '<li><a class="btn btn-item-edit"';
				echo ' href="' . base_url() . 'inventory/edititem/' . $itemId . '">Edit</a></li>';
				echo '<li>' . $itemName . '</li>';
				echo '<li class="price">$' . $price . '</li>';
				echo '</ul>';
			}
		}
		echo '</section>'; // close class="manage-category-items".
		echo '</article>'; // Close class="manage-category".
	}




	?>


</div><!-- #view-manage DIV -->

<!-- End of the individual view -->
