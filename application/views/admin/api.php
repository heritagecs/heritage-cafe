<!-- Start of the individual view -->
<?php
// Get the boolean from the session variable.
$is_admin = $this->session->is_admin;

// Check if this person is an admin.
if (isset($is_admin) && $is_admin != false) {



?>

<div id="" class="site-content-inner">

<h2>API</h2>

<?php
echo '<section class="settings-section">';
echo '<h6>Connect to Populi</h6>';

// Create a substring of the apitoken to show in the UI.  I.e. do not expose the whole thing.
$apiTokenViewable = substr($apiToken, 0, 10) . '......' . substr($apiToken, strlen($apiToken) - 10, 10);

if($apiToken) {
  echo '<p style="word-wrap: break-word;">Current API Token: <code>' . $apiTokenViewable . '</code></p>';
} else {
  echo '<p style="color: #b30737">An API key has not be set.  The API key must be set in order for communication between Heritage and Populi to be successful.  In order for this application to talk to Populi, please enter a set of credentials for a Populi account that has the Registrar role.<p>';
}

include('form-update-api-token.php');

echo '</section><!-- END of .settings-section -->';

?>

</div><!-- .site-content-inner DIV -->

<?php
} else {
	echo '<p>You are not an admin</p>';
}
?>
