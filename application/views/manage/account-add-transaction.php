<div class="panel-short-form">
<?php

// Open the form.
echo form_open('manage/addTransaction', 'id="form-add-transaction" class="form-short form-add-transaction hidden" novalidate');

// Amount field and label.
echo form_label('Amount', 'form-add-transaction-amount');
$extra = array(
  'id'        => 'form-add-transaction-amount',
  'pattern'   => '\d+(\.\d{1,2})?',
  'required'  => 'required',

);
echo form_input('amount', '', $extra);
echo '<span class="input-error-message"></span>';

// Date picker
echo form_label('Date', 'form-add-transaction-date');
echo '<span class="text-small">yyyy-mm-dd or yyyy/mm/dd | </span>';
echo '<input id="form-add-transaction-date" name="transaction_date" value="' . date('Y-m-d') . '"';
echo 'pattern="^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$" type="date" onfocus="this.select();"/>';
echo '<span class="input-error-message"></span>';

// Transaction Type.  Defaulted to a credit transaction.
echo form_label('Transaction Type');
echo form_radio('transaction_type', 0, false, array('id' => 'form-transaction-type-debit'));
echo form_label('Debit (removes funds)', 'form-transaction-type-debit');
echo form_radio('transaction_type', 1, true, array('id' => 'form-transaction-type-credit'));
echo form_label('Credit (adds funs)', 'form-transaction-type-credit');

// Memo
echo form_label('Memo', 'form-add-transaction-memo');
echo form_input('memo', '');

// Payment type radio
echo form_label('Payment Type', 'form-add-transaction-payment-type');
echo '<select id="form-add-transaction-payment-type" name="payment_type">';
foreach ($paymentTypes as $type) {
  // Don't show the On Account or Bean Tab payment types.
  if  ($type->id == 1 || $type->id == 5) continue;
  // Display the payment type.
  echo '<option value="' . $type->id . '"';
  if ($type->id == 4) echo ' selected';
  echo '>' . $type->label . '</option>';
}
echo '</select>';

// The hidden account id field.
echo form_hidden('person_id', $account->person_id);

// Submit.
echo form_submit('submit', 'Save', 'class="btn-primary"');
echo form_button('btn-add-transaction-cancel', 'Cancel', 'class="btn-add-transaction-cancel" onclick="toggleFormAddTransaction()"');


// Close the form.
echo form_close();

?>
</div>
