<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width-device-width, initial-scale=1" />

	<title>Heritage Cafeteria</title>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />
</head>

<body>
<div id="page" class="site">

	<div id="container" class="site-content">

<!-- End of Header Section -->
