<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Model for the Users api controller.  Gets users from the database
 */

class InventoryTable extends CI_Model {

	public function __construct() {
		$this->load->model('InventoryCategory');

		// Call the parent constructor
		parent::__construct();
	}



	/**
	 * [getItem function returns an object with properties about a specific item.]
	 * @param  [type] $id [the item's id]
	 * @return [type]     [an object of properties containing the item's data, or NULL]
	 */
	public function getItem($id) {
		// The list of fields to query.
		$fields = 'inventory.id, inventory.description, inventory.tax_code, inventory.price,';
		$fields .= 'inventory.is_active, inventory.category_id, inventory_category.category,';
		$fields .= 'inventory.created_date, inventory.modified_date,';
		$fields .= 'tax_code.code, tax_code.label';

		// Query the data for this specific item's record.
		$this->db->select($fields);
		$this->db->from('inventory');
		$this->db->join('inventory_category', 'inventory_category.id = inventory.category_id');
		$this->db->join('tax_code', 'tax_code.code = inventory.tax_code');
		$this->db->where('inventory.id', $id);
		$query = $this->db->get();

		// Put the first row into a results variable.
		$result = $query->row();

		// Verify that we found the item we were looking for.
		if (isset($result)) {
			$item = $result;
			log_message('debug', 'MODEL: InventoryTable | An item was found in inventory matching the ID: ' . $item->id);
		} else {
			$item = null;
			log_message('error', 'MODEL: InventoryTable | There was an error locating the item matching ID: ' . $id);
		}

		// Return the array
		return $item;
	}







	/**
	 * [getItems Queries for all active items from the database]
	 * @return [array] [returns an array of rows of items]
	 */
	public function getItems($activeStatus = null) {
		// The list of fields to query.
		$fields = 'inventory.id, inventory.description, inventory.tax_code, inventory.price,';
		$fields .= 'inventory.is_active, inventory.category_id, inventory_category.category';

		// Query the database for all active inventory items.
		// Put the query into an array.
		$this->db->select($fields);
		$this->db->from('inventory');
		if ($activeStatus === 0) $this->db->where('inventory.is_active', 0);
		if ($activeStatus === 1) $this->db->where('inventory.is_active', 1);
		$this->db->join('inventory_category', 'inventory_category.id = inventory.category_id');
		$this->db->order_by('inventory.description', 'ASC');
		$query = $this->db->get();

		// Put the result into an array.
		$result = $query->result();

		// Return the results.
		return $result;
	}



	/**
	 * Retrieve items from the database baseds on category id and return the results as a json object.
	 */
	public function getItemsByCategoryId($id, $is_active = null) {
		// The list of fields to query.
		$fields = 'inventory.id, inventory.description, inventory.tax_code, inventory.price,';
		$fields .= 'inventory.is_active, inventory.category_id, inventory_category.category';

		// Query the database for all active inventory items.
		// Put the query into an array.
		$this->db->select($fields);
		$this->db->from('inventory');
		if ($is_active === 0) $this->db->where('inventory.is_active', 0);
		if ($is_active === 1) $this->db->where('inventory.is_active', 1);
		$this->db->where('inventory.category_id', $id);
		$this->db->join('inventory_category', 'inventory_category.id = inventory.category_id');
		$this->db->order_by('inventory.description', 'ASC');
		$query = $this->db->get();

		// Put the result into an array.
		$result = $query->result();

		// Return the results.
		return $result;
	}








	/**
	 * [addNewItem Inserts a new item into the database using form input.]
	 * @param [type] $name        [description]
	 * @param [type] $category    [description]
	 * @param [type] $description [description]
	 * @param [type] $taxCode     [description]
	 * @param [type] $price       [description]
	 */
	public function addNewItem($category, $description, $taxCode, $price) {
		// Get the date and time.
		$dateTime = getSqlDateTime();

		// Figure out the category ID.
		$categoryId = $this->InventoryCategory->getCategoryId($category);
		if ($categoryId == -1) {
			// If there was no category, create one, and then re-query for the new id.
			$this->InventoryCategory->addNewCategory($category);
			$categoryId = $this->InventoryCategory->getCategoryId($category);
		}

		// Build an array of data.
		$data = array(
			'category_id'		=> $categoryId,
			'description'		=> $description,
			'tax_code'			=> $taxCode,
			'price'					=> $price,
			'created_date'	=> $dateTime,
		);

		// Insert the data into the database.
		$this->db->insert('inventory', $data);
	}



	/**
	 * Updates an inventory items properties in the database.
	 * @param  [object] $item				A object containing items properties.  id, categoryName, description, taxCode, price.
	 */
	public function updateItem($item) {
		log_message('info', 'MODEL: InventoryTable | updateItem()');

		// Make sure the item in question exists in the database and also leave a trail in the logs.
		$existingItem = $this->InventoryTable->getItem($item->id);
		if ($existingItem != null) {
			log_message('debug', 'MODEL: InventoryTable | Existing Item: ' . json_encode($existingItem));
			log_message('debug', 'MODEL: InventoryTable | Updating Item: ' . json_encode($item));
		} else {
			log_message('error', 'MODEL: InventoryTable | In an attempt to update item ' . $item->id . ' we could not locate it in the database.');
			return false;
		}

		// Figure out the category ID.
		$categoryId = $this->InventoryCategory->getCategoryId($item->category);

		if (!$categoryId) {
			// If there was no category, create one, and then re-query for the new id.
			$this->InventoryCategory->addNewCategory($item->category);
			$categoryId = $this->InventoryCategory->getCategoryId($item->category);
		}

		// Build an array of data.
		$data = array(
			'category_id'	=> $categoryId,
			'description'	=> $item->description,
			'tax_code'		=> $item->taxCode,
			'price'			=> $item->price,
			'is_active'		=> $item->is_active,
		);

		// Insert the data into the database.
		$this->db->where('id', $item->id);
		$this->db->update('inventory', $data);

		// Get the item back.
		$this->db->where('id', $item->id);
		$this->db->where('category_id', $categoryId);
		$this->db->where('description', $item->description);
		$this->db->where('tax_code', $item->taxCode);
		$this->db->where('price', $item->price);
		$row = $this->db->get('inventory')->row();

		// Check that the row has data.
    if ($row) {
			log_message('debug', 'MODEL: InventoryTable | The item ' . $item->description . ' (' . $item->id . ') was updated successfully.');
			return true;
		} else {
			log_message('debug', 'MODEL: InventoryTable | Failed updating item ' . $item->description . ' (' . $item->id . ').');
			return false;
		}
	}







	/**
	 * @return [type] Returns an array of item objects.
	 */
	public function getFeaturedItems() {
		// The list of fields to query.
		$fields = 'inventory.id, inventory.description, inventory.is_active,';
		$fields .= 'items_featured.id, items_featured.inventory_id';

		// Put the query into an array.
		$this->db->select($fields);
		$this->db->from('items_featured');
		$this->db->join('inventory', 'inventory.id = items_featured.inventory_id');
		$this->db->order_by('items_featured.id', 'ASC');
		$query = $this->db->get();

		// Put the result into an array.
		$result = $query->result();

		// Return the results.
		return $result;
	}







	/**
	 * @return [type] Returns a single featured item object
	 */
	public function getFeaturedItem($id) {
		log_message('debug', 'getFeaturedItem($id), ID is ' . $id);
		// Put the query into an array.
		$query = $this->db->get_where('items_featured', array('id' => $id));

		// Put the result into an array.
		$result = $query->row();

		// Return the results.
		return $result;
	}














	/**
	 * Updates the featured item to a new inventory item id.
	 * @param [type] $id     [The ID of the featured item to be updated, between 1 and 8.]
	 * @param [type] $itemId [The ID of the inventory item to use.]
	 */
	public function setFeaturedItem($id, $itemId) {
		log_message('debug', 'setFeaturedItem()');

		// Sanity check on incoming data.
		if ($id < 1 || $id > 8) {
      log_message('debug', 'The featured item id is not within the expected limits.  It must be a number between 1 and 8.');
			return;
    } else {
			log_message('debug', 'Editing an item');
		}

		// Make sure there is an item in inventory with this item id.
		$checkThis = $this->getItem($itemId);
		if ($checkThis == null) {
			return;
		}

		// Update the data in the database.
		$this->db->where('id', $id);
		$this->db->update('items_featured', array('inventory_id' => $itemId));

		// Check that the update worked.
		$query = $this->db->get_where('items_featured', array('id' => $id));
		$result = $query->row();

		if ($itemId != $result->inventory_id) {
			log_message('error', 'There was an error updating the featured item with ID: '. $id);
		} else {
			log_message('debug', 'The featured item with ID: ' . $id . ' was updated successfully to item no ' . $itemId);
		}
	}




	/**
	 * Moves the featured item into the new slot specified.
	 * @param  [type] $id  [description]
	 * @param  [type] $dir [description]
	 * @return [type]      [description]
	 */
	public function moveFeaturedItem($pos, $newPos) {
		log_message('debug', 'moveFeaturedItem($pos, $newPos), moving item in position ' . $pos . ' to position ' . $newPos);

    // Make sure the id is one of the acceptable id's.
    if ($pos >= 1 && $pos <= 8 && $newPos >= 1 && $newPos <= 8) {
      log_message('debug', 'The position is an acceptable value.');
    } else {
      log_message('error', 'The position is NOT an acceptable value.');
			return;
    }

		// Get data for each of the items
		$item1 = $this->getFeaturedItem($pos);
		$item2 = $this->getFeaturedItem($newPos);

		log_message('debug', 'item1: ' . json_encode($item1));
		log_message('debug', 'item2: ' . json_encode($item2));

		// Put the first item into the second item's position
		$this->db->where('id', $item2->id);
		$this->db->update('items_featured', array('inventory_id' => $item1->inventory_id));

		// Put the second item into the first item's position
		$this->db->where('id', $item1->id);
		$this->db->update('items_featured', array('inventory_id' => $item2->inventory_id));
	}








	/**
	 * Creates a select box for the entire inventory and is sorted by categories.
	 * @return [type] [description]
	 */
	public function createInventoryDropDown() {
		$inventory = $this->InventoryTable->getItems();
		$categories = $this->InventoryCategory->getCategories();

		// Create a select box that contains all the items group by category.
		$inventoryDropDown = '<select name="items-list-complete">';
		$inventoryDropDown .= '<option disabled selected value> -- Select from Inventory -- </option>';
		// Process through each category
		foreach ($categories as $cat) {
			// Setup variables
			$catId		= $cat->id;
			$name 		= $cat->category;
			$desc			= $cat->description;

			// Create a div for this category.
			$inventoryDropDown .= '<optgroup label="' . $name . '">';
			foreach ($inventory as $item) {
				if ($catId == $item->category_id) {
					$inventoryDropDown .= '<option id="' . $item->id . '" ';
					// If this is the store, add the event handlers.
					if (strpos(current_url(), 'store') != false) {
						$inventoryDropDown .= 'onclick="addItemToCheckout(' . $item->id . ')"';
					}
					$inventoryDropDown .= ' value=' . $item->id . '>';
					$inventoryDropDown .= $item->description . '</option>';
				}
			}
			$inventoryDropDown .= '</optgroup>';
		}
		$inventoryDropDown .= '</select>';

		echo $inventoryDropDown;
	}





}
