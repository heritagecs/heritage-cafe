<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Constructor
	 */
	public function __construct() {
		// Call the parent constructor
		parent::__construct();

		// Load up the appropriate models.
		$this->load->model('User');
		$this->load->model('Role');
		$this->load->model('Account');
		$this->load->model('Transaction');
		$this->load->model('InventoryTable');
		$this->load->model('InventoryCategory');
		$this->load->model('Sales');
    	$this->load->model('Sync');
		$this->load->model('CoffeeshopTab');
		$this->load->model('Util');

		// Check if the user is logged in.
		$this->User->is_logged_in();

		// If this user is not an admin, don't let them in.
		if (!$this->Role->validateAccess(array('admin'))) {
			redirect('store');
		}
	}


	/**
	 * Index Page for this controller.
	 *
	 */
	public function index() {
		redirect('admin/settings');
	}





	public function settings() {
		log_message('debug', 'CONTROLLER: Admin | settings()');
		
		// Get settings data.
		$data['apiToken'] = $this->Settings->getSettingValue('api_token');
		$data['lastFullSync'] = $this->Sync->getLastFullSyncTimestamp();

		// Check for an action in the post data.
		if ($this->input->post('action')) {
			$action = $this->input->post('action');
			log_message('debug', 'CONTROLLER: Admin | An action was specified for Settings: ' . $action);
		}

		// Based on the action, do something.
		if (isset($action)) {
			switch($action) {
				case 'update-api-token':
					// If we are updating the api token, get the username and password the post variables.
					$apiUsername = $this->input->post('api_username');
					$apiPassword = $this->input->post('api_password');
					// Get a new API token from Populi.
					$apiToken = $this->PopuliConnect->getApiToken($apiUsername, $apiPassword);
					// Put the api token into the database.
					if($apiToken) $this->Settings->setSettingValue('api_token', $apiToken);
					break;
			}
			// Redirect back to this page
			redirect('admin/settings');
		}




		$this->load->view('parts/header');
		$this->load->view('nav/admin');
		$this->load->view('admin/settings', $data);
		$this->load->view('parts/footer');

	}





  public function users() {
	$this->load->view('parts/header');
    $this->load->view('nav/admin');
    $this->load->view('admin/users');
    $this->load->view('parts/footer');
  }

  public function util() {
	// A results array to pass to the front end.
	$data = [];

	$action = $this->input->get('action');
	if (!$action) $action = $this->input->post('action');
	$key = $this->input->get('key');
	if (!$key) $key = $this->input->post('key');

	// If the key supplied was correct.
	if ($key = getAdminUtilityKey()) {
		// What was the action.
		if ($action == 'sync-supporting-data') {
			$this->Sync->updateSupportingData();
			redirect('admin/util');
		} else if ($action == 'zero-staff-accounts') {
			// Check that the user confirmed their intent here.
			if ($this->input->post('confirmation') === 'ZEROACCOUNTS') {		
				log_message('debug', 'CONTROLLER: Admin | START Zero staff account balances.');
				// Get a list of staff account numbers
				$accounts = $this->Account->getAccountsByType('STAFF');

				// Loop over the list of accounts
				// Call the Zero function, passing it a new memo
				$count = 0;
				foreach ($accounts as $account) {
					$pId = $account->person_id;
					// Try to Zero out the account
					$try = $this->Account->zeroCafeAccount($pId, null, 'Zero out balance of staff account no. ' . $pId);
					// Log the result
					if ($try) {
						log_message('debug', 'CONTROLLER: Admin | Zero out balance for ' . $pId . ' successful.');
						$count++;
					}
					if (!$try) log_message('error', 'CONTROLLER: Admin | Zero out balance for ' . $pId . ' FAILED.');
				}
				log_message('debug', 'CONTROLLER: Admin | SUMMARY: Reset balances for ' . $count . ' staff accounts.');
			}
		} else if ($action == 'zero-coffeeshop-balance') {
			if ($this->input->post('confirmation') === 'ZEROCOFFEESHOPTABS') {
				log_message('debug', 'CONTROLLER: Admin | Zero out coffeeshop balances confirmed by user.');
				// Punchc it Chewie!
				$try = $this->Util->zeroCoffeeshopBalancesForAllAccounts();
				// Log it.
				($try->result) ? $w = 'SUCCESS' : $w = 'FAIL';
				log_message('debug', 'CONTROLLER: Admin | Zero out coffeeshop balances ' . $w);
				// Send the result object to the front end.
				$data['zero_coffeeshop_balances_result'] = $try;
			}
		}
	}

	
	$this->load->view('parts/header');
    $this->load->view('nav/admin');
    $this->load->view('admin/util', $data);
    $this->load->view('parts/footer');
  }

}
