<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Model {

  public function __construct() {
    // Call the parent constructor
		parent::__construct();

    // Load up the appropriate models.
    $this->load->model('User');
    $this->load->model('MealPlan');
    $this->load->model('populi/PopuliApi');
    $this->load->model('populi/PopuliApiCache');
    $this->load->model('AcademicTerms');
    $this->load->model('Transaction');
  }






















  /**
   * [isAccount Determines if there is already a local account for a particular person]
   * @param  [type]  $personId [description]
   * @return boolean           [Return true if there is a local account.]
   */
  public function isAccount($personId) {
    // variables
    $isAccount = false;

    // Query the database for a record for this person.
    $query = $this->db->get_where('account', array('person_id' => $personId));
    $row = $query->row();

    // If the person exists, is account is true.
    if (isset($row)) {
      $isAccount = true;
    }

    // Return the reslt.
    return $isAccount;
  }



  /**
   * Gets and returns information for a specific account.
   * @param  [type] $personId The person ID record for the account in question.
   * @return [type]           A json object containing the person data.
   */
  public function getAccount($personId) {
    log_message('info', 'MODEL: Account | getAccount()');

    // Query the database for a record for this person.
    $query = $this->db->get_where('account', array('person_id' => $personId));
    $row = $query->row();

    // If the person exists, get the data, otherwise return false.
    ($query->row()) ? $row = $query->row() : $row = false;
		if (!$row) log_message('error', 'MODEL: Account | An account with ID ' . $personId . ' does not exist.');
		return $row;
  }










  /**
   * Query the database for a list of all accounts and their relevant data.
   * @param  string $order_by Accepts the names of the fields in the Accounts table.
   *                          id, person_id, first, last, mp_name, mp_balance, mp_term_id.
   *                          Default is 'last'.
   * @param  string $order    ASC or DESC.  Default is ACS.
   * @return An array of objects containing data from all the accounts sorted by last name.
   */
  public function getAccounts($order_by = 'last', $order = 'ACS', $isActive = true) {
    log_message('info', 'MODEL: Account | getAccounts()');

    // The list of fields to query.
    $fields = '';
    $fields .= '';
    
    // Query the data for this specific item's record.
    if ($isActive === true) $this->db->where('is_active', 1);
    $this->db->order_by($order_by, $order);
    // If we are ordering by first name, also order by last name.
    if ($order_by == 'first') $this->db->order_by('last', $order);
    // Get the results.
    $query = $this->db->get('account');

    // Put the results into an array.
    $result = $query->result();

    // Return the array
    return $result;
  }

  /**
   * Get the accounts by type.
   */
  public function getAccountsByType($type = 'CREDIT', $status = 'ACTIVE') {
    log_message('debug', 'MODEL: Account | getAccountsByType()');

    // Set up the query.
    $this->db->where('type', strtoupper($type));
    if ($status === 'ACTIVE') $this->db->where('is_active', true);
    if ($status === 'INACTIVE') $this->db->where('is_active', false);
    $this->db->order_by('first', 'ASC');
    $query = $this->db->get('account');

    // Return the array of results.
    return $query->result();
  }







  /**
   * Goes to the database to get the accounts for folks who are on meal plans.
   * @return An array of person objects
   */
  public function getAccountsMealPlan($isMpAdded = true) {
    // Query the data for this specific item's record.
    if ($isMpAdded) {
      $this->db->where('is_mp_added', 1);
    } else {
      $this->db->where('is_mp_added', 0);
    }
    $this->db->order_by('last', 'ACS');
    $query = $this->db->get('account');

    // Put the results into an array.
    $result = $query->result();

    // Return the array
    return $result;
  }







  public function setMetaData($id) {
    log_message('info', 'MODEL: Account | setNames');

    // Get the person data from myHeritage
    $person = $this->PopuliApi->getPerson($id);

    // Update the local database with the person's current name.
    $data = array(
      'first'   => $person->first,
      'last'    => $person->last,
    );

    // Update the record.
    $this->db->where('person_id', $id);
    $result = $this->db->update('account', $data);

    // Log the result.
    $fullName = $person->first . ' ' . $person->last;
    ($result) ? $message = 'was successful' : $message = 'failed';
    log_message('debug', 'MODEL: Account | Updating the account meta data for ' . $id . ' (' . $fullName . ') ' . $message . '.');

    // Return the result.
    return $result;
  }












































  /**
   * Create a basic local account based on the person Id from the Populi database.
   * @param [type] The person_id.  Defaults to 0 dollar account.
   * @return [int]          0 = failure, 1 = account already exists, 2 = new account created.
   */
  public function createBasicAccount($personId, $balance = 0) {
		// Check that the ID is numberic.
		if (!is_int($personId)) {
			log_message('info', 'MODEL: Account | createBasicAccount(). The parameter, personId, is not an integer.');
			return 0;
		} else {
      log_message('info', 'MODEL: Account | createBasicAccount(). $personId = ' . $personId . ', $balance = ' . md5(strval($balance)));
    }

    // Check if there is already an account for this person id.
    if ($this->isAccount($personId)) {
      log_message('info', 'MODEL: Account | An account for person_id: ' . $personId . ' already exists in the local database.');
      return 1;
    } else {
      // Go get the person's first and last name from Populi.
      $person = $this->PopuliApi->getPerson($personId);
      $fullName = $person->first . ' ' . $person->last;
      log_message('debug', 'MODEL: Account | Creating a local account for ' . $fullName . '.');

      // Prepare the data
      $data = array(
        'person_id'           => $personId,
        'first'               => $person->first,
        'last'                => $person->last,
        'mp_name'             => 'None',
        'mp_balance'          => $balance,
        'type'                => 'CREDIT',
        'mp_balance_populi'   => null,
        'mp_starting_balance' => null,
        'mp_term_id'          => null,
        'mp_term_name'        => null,
        'is_mp_added'         => false,
        'created_date'        => getSqlDateTime(),
      );

      // Insert the record into the Accounts table.
      $this->db->insert('account', $data);

      return 2;
    }
  }










  /**
   * Tests to see if the cafeteria account is active or not.
   * @param  [int]  $personId     A numerical person Id.
   * @return boolean              The result of the test.  True is active.
   */
  public function isActive($personId) {
    log_message('info', 'MODEL: Account| isActive()');

    // Query the database for a record for this person.
    $query = $this->db->get_where('account', array('person_id' => $personId));
    $row = $query->row();

    if ($row->is_active) {
      return true;
    } else {
      return false;
    }
  }


	public function setIsActive($personId, $active = true) {
		log_message('info', 'MODEL: Account | setIsActive()');

		// If there is a record for this person, update it.
		if ($this->Account->getAccount($personId)) {
			$this->db->where('person_id', $personId);
			$this->db->update('account', array('is_active' => $active));
			// Verify the update was successful.
			if ($this->Account->getAccount($personId)->is_active == $active) {
				log_message('info', 'MODEL: Account | The account status (is_active) for person ' . $personId . ' was updated SUCCESSFULLY.');
			} else {
				log_message('error', 'MODEL: Account | The update to the account status (is_active) for ' . $personId . ' FAILED.');
			}
		}
	}





  /**
   * Get the account type for this account out of the database
   * 
   * Account types
   * 
   * CREDIT - Credit Account
   * MEAL_PLAN - Heritage Meal Plan
   * PRE_PAID - Lily Ruth Pre-Paid
   * STAFF - Heritage Staff and Faculty
   */
  public function getType($personId) {
    log_message('info', 'MODEL: Account | getAccountType()');

    // Query the database for a record for this person.
    $query = $this->db->get_where('account', array('person_id' => $personId));

    // If the person exists, get the account type data, otherwise return false.
		if (!$query->row()) log_message('error', 'MODEL: Account | An account with ID ' . $personId . ' does not exist.');
    ($query->row()) ? $accountType = $query->row()->type : $accountType = false;

    // If the value is null, we assume this is a credit account
    if ($accountType === null) $accountType = 'CREDIT';

    // Return the account type.
		return strtoupper($accountType);
  }
  public function setType($personId, $type = 'CREDIT') {
    log_message('info', 'MODEL: Account | setAccountType()');

    // Get the person record
    $currentType = $this->getType($personId);
    if ($currentType == strtoupper($type)) {
      log_message('info', 'MODEL: Account | Account type for ' . $personId . ' already set correctly');
      return true;
    }

    // Put a new value into the database.
    $this->db->where('person_id', $personId);
    $result = $this->db->update('account', array('type' => strtoupper($type)));

    // Check the result.
    if ($result) {
      log_message('debug', 'MODEL: Account | Account type for ' . $personId . ' updated to ' . $type);
      return true;
    } else {
      log_message('error', 'MODEL: Account | Account type update for ' . $personId . ' FAILED');
      return false;
    }
  }

  public function determineType($personId) {
    log_message('info', 'MODEL: Account | determineType()');
    
    /* If the person is in the meal plan list, then the type is MEAL_PLAN */
    $mealPlanAccounts = $this->MealPlan->getPeopleOnMealPlans();
    foreach ($mealPlanAccounts as $mp) {
      if ((int)$mp->person_id == (int)$personId) {
        log_message('info', 'MODEL: Account | Determined ' . $personId . ' has an account type of MEAL_PLAN');
        return 'MEAL_PLAN';
      }
    }

    /* Look in the Staff list */
    $staff = $this->PopuliApiCache->getRoleMembers('Staff');
    foreach ($staff as $s) {
      if ($s == (int)$personId) {
        log_message('info', 'MODEL: Account | Determined ' . $personId . ' has an account type of STAFF');
        return 'STAFF';
      }
    }

    /* Look in the Faculty list */
    $staff = $this->PopuliApiCache->getRoleMembers('Faculty');
    foreach ($staff as $s) {
      if ($s == (int)$personId) {
        log_message('info', 'MODEL: Account | Determined ' . $personId . ' has an account type of STAFF');
        return 'STAFF';
      }
    }

    // If this is not a meal plan and not a staff account, if the balance is greater than 0, it must be a pre-paid account.
    $account = $this->getAccount($personId);
    if ((float)$account->mp_balance > 0) {
      log_message('debug', 'MODEL: Account | Determined ' . $personId . ' has an account type of PRE_PAID');
      return 'PRE_PAID';
    }

    // If none of the above, its a credit account
    log_message('info', 'MODEL: Account | Determined ' . $personId . ' has an account type of CREDIT');
    return 'CREDIT';
  }

  // Set the account types for all currently active accounts.
  public function setTypes($local = true) {
    /*
      Loop over the accounts and set their types appropriately.  This will catch all account types
      MEAL_PLAN, STAFF, PRE_PAID, and CREDIT.
    */
    if ($local) {
      $localAccounts = $this->Account->getAccounts();
      foreach ($localAccounts as $row) {
        $accounts[] = (int)$row->person_id;
      }
    } else {
      $accounts = $this->PopuliApiExtended->getActiveAccounts();
    }

    // Loop over the active accounts and determine which of them should be pre_paid accounts.
    foreach ($accounts as $pId) {
      // Determine and set the type.
      $type = $this->Account->determineType($pId);
      $this->Account->setType($pId, $type);
    }
  }





  /**
   * Returns the balance for the account.
   * @param  [type] $personId [description]
   * @return [type]           [description]
   */
  public function getBalance($personId) {
    // Query the database
    $this->db->where('person_id', $personId);
    $query = $this->db->get('account', 'mp_balance');

    // Get just the balance.
    return $query->row()->mp_balance;
  }








  public function updateBalance($personId, $amount, $transactionType = 0) {
    // First query the database for the balance of this person's account.
    $currentBalance = $this->getBalance($personId);

    // Calculate the new balance.
    // Handle a debit (0), or credit(1)
    if ($transactionType == 0) {
      $newBalance = $currentBalance - $amount;
      $operator = ' - ';
      log_message('debug', 'MODEL: Account | Debiting the account.');
    } else if ($transactionType == 1) {
      $newBalance = $currentBalance + $amount;
      $operator = ' + ';
      log_message('debug', 'MODEL: Account | Crediting the account.');
    }
    log_message('debug', 'MODEL: Account | updateBalance calculation.  ' . $currentBalance . $operator . $amount . ' = ' . $newBalance);

    // Ensure the number has the proper format before insertting it.
    number_format($newBalance, 2);

    // Build the data.
    $data = array(
      'mp_balance'  => $newBalance,
    );

    // Perform the update.
    $this->db->where('person_id', $personId);
    $this->db->update('account', $data);

    // Double check that the new balance was updated.
    if ($newBalance == $this->getBalance($personId)) {
      log_message('debug', 'MODEL: Account | Account balance updated successfully for account id ' . $personId . ' (Old Balance: $' . $currentBalance . ' | New Balance: $' . $newBalance . ')');
      return true;
    } else {
      log_message('error', 'MODEL: Account | Account balance update FAILED for account id ' . $personId . '.');
      return false;
    }

  }


  public function updateCoffeeShopTab($personId, $amount, $transType = 0) {
    // First, get the current coffee shop balance.
    $oldBalance = $this->getCoffeeShopTab($personId);

    // Calculate the new balance
    if ($transType == 0) {
      $newBalance = $oldBalance - $amount;
      log_message('debug', 'MODEL: Account | updateCoffeeShopTab calculation. DEBIT   ' . $oldBalance . ' - ' . $amount . ' = ' . $newBalance);
    } else {
      $newBalance = $oldBalance + $amount;
      log_message('debug', 'MODEL: Account | updateCoffeeShopTab calculation. CREDIT   ' . $oldBalance . ' + ' . $amount . ' = ' . $newBalance);
    }

    // Ensure the number has the proper format before insertting it.
    number_format($newBalance, 2);

    // Perform the update.
    $this->db->where('person_id', $personId);
    $try = $this->db->update('account', array('coffeeshop_balance'  => $newBalance));

    if ($try) {
      log_message('debug', 'MODEL: Account | Coffee Shop tab updated successfully.  Account id ' . $personId . ' (Old Balance: $' . $oldBalance . ' | New Balance: $' . $newBalance . ')');
      return true;
    } else {
      log_message('error', 'MODEL: Account | Coffee Shop tab updated FAILED.  Account id ' . $personId);
      return false;
    }

  }




















  /**
   * Completely overwrites the stored balance in an account with a new value.
   * @param  [type] $personId [description]
   * @param  [type] $amount   [description]
   */
  public function overwriteBalance($personId, $amount) {
    // First query the database for the balance of this person's account.
    $storedBalance = $this->Account->getBalance($personId);

    // Format the number with number format before storing it in the database
    number_format($amount, 2);

    // Perform the update.
    log_message('debug', 'MODEL: Account | Overwritting balance for account id ' . $personId . ' (Previous Value: $' . $storedBalance . ' | New Balance: $' . $amount . ')');
    $this->db->where('person_id', $personId);
    $this->db->update('account', array('mp_balance' => $amount));
  }





  /** This function is deprecated. */
  public function zeroCafeAccount($personId, $date = null, $memo = null) {
    log_message('debug', 'MODEL: Account | zeroCafeAccount().');

    $balance = $this->Account->getBalance($personId);

    // If the balance is already 0, don't do anything.
    if ($balance == 0) {
      $skip = true;
      log_message('debug', 'MODEL: Account | The account ' . $personId . ' is already Zero.');
      return true;
    } else if ($balance > 0) {
      // If the balance is greater than 0, we are going to debit the account.
      $transactionType = 0;
    } else {
      // If the balance is less than 0, we will credit the account.
      $balance = $balance * -1;
      $transactionType = 1;
    }

    // If the caller has specified a date value, use that.
    if (!$date) {
      $date = getSqlDateTime();
    } else {
      $date = date($date . ' h:i:s');
    }

    // Add a transaction.
    if ($memo == null) $memo = 'EoY - zero out account balance.';
    $object = (object)array(
      'total'             => $balance,
      'payment_type'      => 1,
      'storefront'        => 'CAFETERIA',
      'date'              => $date,
      'transaction_type'  => $transactionType,
      'account_id'        => $personId,
      'memo'              => $memo,
    );
    $isTransaction = $this->Transaction->addTransaction($object);
    $tId = $isTransaction;

    // Wipe out the balance.
    $this->Account->overwriteBalance($personId, 0);

    if ($isTransaction) {
      return (int)$tId;
    } else {
      return false;
    }
  }

  public function zeroCoffeeshopBalance($personId, $date = null, $memo = null) {
    log_message('debug', 'MODEL: Account | ZeroCoffeeshopBalance()');

    $balance = $this->Account->getCoffeeShopTab($personId);

    // If the balance is already zero return.
    if ($balance == 0) {
      log_message('info', 'MODEL: Account | Account ' . $personId . ' is already Zero.');
      return true;
    }
    // Determine the transaction type (debit or credit) that we need to make.
    ($balance > 0) ? $transactionType = 0 : $transactionType = 1;
    if ($transactionType == 1) $balance = $balance * -1;

    // Prep the date.  If none, then get now, if a date was submitted, prep it.
    (!$date) ? $date = getSqlDateTime() : $date = date($date . ' h:i:s');

    // Prep the memo based on input.
    if ($memo == null) $memo = 'Zero out The Bean Tab balance.';

    // Add a transaction
    $object = (object)array(
      'total'             => $balance,
      'payment_type'      => 1,
      'storefront'        => 'COFFEESHOP',
      'date'              => $date,
      'transaction_type'  => $transactionType,
      'account_id'        => $personId,
      'memo'              => $memo,
    );
    $is_transaction = $this->Transaction->addTransaction($object);

    // Wipe out the balance.
    log_message('debug', 'MODEL: Account | Overwritting Coffeeshop balance for account id ' . $personId . ' (Previous Value: $' . $balance . ' | New Balance: $0)');
    $this->db->where('person_id', $personId);
    $this->db->update('account', array('coffeeshop_balance' => 0));

    // Return
    if ($is_transaction) return true;
    return $memo;
  }






  /**
   * Adds funds to a cafe account.  Creates the transaction and updates the balance.
   * @param  Int $accountId    A numerical account id (person Id)
   * @param  Float $amount     The amount of funds we are adding.
   * @return boolean           Success or failure.
   */
  public function creditCafeAccount($accountId, $amount, $memo = null) {
    $object = (object)array(
      'total'             => $amount,
      'payment_type'      => 1,
      'storefront'        => 'CAFETERIA',
      'transaction_type'  => 1,
      'account_id'        => $accountId,
      'memo'              => $memo,
    );  
    // Add a transaction that adds the value of the funds.
    $result = $this->Transaction->addTransaction($object);
    // Update the balance.
    if ($result) $this->Account->updateBalance($accountId, $amount, 1);
    // Return the result.
    return $result;
  }

  public function debitCafeAccount($accountId, $amount, $memo = null) {
    $object = (object)array(
      'total'             => $amount,
      'payment_type'      => 1,
      'storefront'        => 'CAFETERIA',
      'transaction_type'  => 0,
      'account_id'        => $accountId,
      'memo'              => $memo,
    );
    // Add a transaction that removes the value of $amount
    $result = $this->Transaction->addTransaction($object);
    // Update the balance.
    if ($result) $this->Account->updateBalance($accountId, $amount, 0);
    // Return the result.
    return $result;
  }


































  /**
   * Gets the history for this account using ther person_id as a parameter.
   * @param  [type] $personId [description]
   * @return [type]           An array of objects.  Returns an empty array if no transaction
   *                             history was found for the account in question.
   */
  public function getAccountHistory($personId, $storefront = null) {
    // Create an empty array.
    $result = array();

    log_message('debug', '====== ' . $storefront);

    // Look in the transactions table for transactions for this person.
    $this->db->where('account_id', $personId);
    if ($storefront) $this->db->where('storefront', $storefront);
    $this->db->order_by('date', 'ASC');
    $query = $this->db->get('transaction');

    // Put the results into an array.
    $result = $query->result();

    // Return the results.
    return $result;
  }



	/**
	 * Retreives the name of the meal plan in the local database.
	 * @param  [type] $personId
	 * @return [type]           The name of the meal plan.
	 */
	public function getMealPlanName($personId) {
    // Query the database
    $this->db->where('person_id', $personId);
    $query = $this->db->get('account', 'mp_name');

    // Get just the meal plan name.
    return $query->row()->mp_name;
	}




  /**
   * Returns the id of the term associated with this person's meal plan.
   * @param  [type] $personId Int of the person's id.
   * @return [type]           An int of the term id associated with the student's meal plan.
   */
  public function getMealPlanTermId($personId) {
    // Query the database
    $this->db->where('person_id', $personId);
    $query = $this->db->get('account', 'mp_term_id');

    // Get just the balance.
    return $query->row()->mp_term_id;
  }
























  /**
   * Add meal plan data to the local database for this person id.
   * @param [type] $person.  Object containing personId, termId, mpId, mpName.
   * @return [boolean]        Returns true is the account balance was changed and fall if the balance was not altered.
   */
  public function addMealPlanData($input) {
    log_message('info', 'MODEL: Account->addMealPlanData() | PARAMETERS: $person = ' . json_encode($input));

    // Get the date and time.
    $dateTime = getSqlDateTime();

    // Setup common data.
    $personId     = $input->person_id;
    $mpId         = $input->mp_id;
    $mpName       = $input->mp_name;
    $mpTermId     = $input->term_id;
    $mpTermName   = $this->PopuliApiCache->getCachedTerm($mpTermId)->name;
    $balanceChange = false;

    // Get information about the available meal plans.
    $mealPlan = $this->MealPlan->getCachedMealPlan($mpId);

    // If this person is not already in the laddMealPlanDataocal database.
    if (!$this->isAccount($personId)) {
      // Create the account.
      $this->Account->createBasicAccount($personId);
    }

    // Get the person data from the local database.
    $person = $this->getAccount($personId);

    // Setup further variables based on new input.
    $fullName           = $person->first . ' ' . $person->last;
    $fullNameUp         = strtoupper($fullName);
    $mpName             = $mealPlan->name;
    $mpBalancePopuli    = number_format((double)$this->MealPlan->getBalance($personId), 2);
    $mpStartingBalance  = (float)$mealPlan->amount;
    $accountStartingBalance = $mpStartingBalance;

    // Prepare the data
    $data = array(
      'mp_name'             => $mpName,
      'mp_starting_balance' => $mpStartingBalance,
      'type'                => 'MEAL_PLAN',
      'mp_term_id'          => $mpTermId,
      'mp_term_name'        => $mpTermName,
      'is_mp_added'         => true,
    );

    log_message('info', 'MODEL: Account | Person data: ' . json_encode($data));

    // Get the balance for this person from our local database.
    $currentBalance = $this->Account->getBalance($personId);

    /*
      Begin heuristics checking to determine what to do with the data from Populi in certain situations.
      1. The student is completely new and has never had a meal plan before.
      2. The student has had a meal plan in a past semester.
      3. Balance carries forward in the winter, but not into the spring or fall.
     */
    $isBalanceChanged = false;
    $localTermId = $this->Account->getMealPlanTermId($personId);
    log_message('debug', 'MODEL: Account | ' . $fullNameUp . ' (' . $personId . ')');


    // If the meal plan's term ID is different than the one we have recorded locally,
    // then this is either a new student or an existing student with a new meal plan.
    if ($mpTermId != $localTermId) {

      // If the local term is null, then this is a new student.
      (!$localTermId) ? $message = 'is now on a meal plan' : $message = 'purchased a NEW meal plan for a NEW term';
      log_message('debug', 'MODEL: Account | ' . $personId . ' ' . $message . '.  Term ID: ' . $mpTermId . '.');


      // The balance carries over if the current semester is winter, or if the previous balance is negative.
      if (!$localTermId) {
        /*
          This is the first meal plan for this cafe account.
         */
        log_message('debug', 'MODEL: Account | ++ Welcome to our new students!  Adding a meal plan to account ' . $personId . '.');
        $result = $this->Account->creditCafeAccount($personId, $mpStartingBalance, 'Adding new meal plan');
        if ($result) $isBalanceChanged = true;
      } else if ($this->MealPlan->isCurrentSemesterWinter()) {
        /*
          This is the winter semester.  As such, since this is a new meal plan, we must carry the balance forward.
          So we will issue a credit to the account, which will render a transaction and update the balance.
         */
        log_message('debug', 'MODEL: Account | ++ We are in the winter semester.  So, the new meal plan for ' . $personId . ' will be added to the previous balance.');
        $result = $this->Account->creditCafeAccount($personId, $mpStartingBalance, 'Adding winter meal plan');
        if ($result) $isBalanceChanged = true;
      } else if ($currentBalance < 0) {
        /*
          The current balance of the cafe account is negative.
          So, the balance must carry forward regardless of the semester.
         */
        log_message('debug', 'MODEL: Account | ++ The previous balance for ' . $personId . ' is negative.  So, we will add the new meal plan to the previous balance of $' . $currentBalance . '.');
        $result = $this->Account->creditCafeAccount($personId, $mpStartingBalance, 'Adding new meal plan to previous balance');
        if ($result) $isBalanceChanged = true;
      } else {
        /*
          The semester is not winter and there is a positive balance so the balance doesn't carry forward.
          This block will execute at the beginning of the fall semester.
          Credit the cafe account by the amount of the meal plan.
         */
        log_message('debug', 'MODEL: Account | -- We have entered the fall or spring semester and/or the balance is positive.  So, the previous balance ($' . $currentBalance . ') for ' . $personId . ' does not carry over.');
        $this->Account->zeroCafeAccount($personId);
        $result = $this->Account->creditCafeAccount($personId, $mpStartingBalance, 'Adding new meal plan - zeroing balance');
        if ($result) $isBalanceChanged = true;
      }


    } else {
      // Get the current meal plan info from the local database.
      $localMpName = $this->Account->getMealPlanName($personId);

      // Since the local term id of the meal plan is the same as the one supplied by Populi then we know that this
      // student has already paid for a meal plan for this semester.
      log_message('debug', 'MODEL: Account | ' . $personId . ' already has a meal plan (' . $localMpName . ') in the local database for this semester (' . $localTermId . ').');
      $isBalanceChanged = false;

      // Check if the meal plan itself is a different one than the one in Populi.
      if ($mpName != $localMpName) {
        /*
          The meal plan itself has changed, so let's update the balance.
          This would happen only if a meal plan was assigned, but an invoice had not been created and then the meal plan
          was changed.  Once the meal plan has been invoiced for a semester, it cannot be changed in Populi so we wouldn't even get here.
         */
        log_message('debug', 'MODEL: Account | ++ The meal plan for ' . $fullNameUp . ' has changed.  Updating available funds to reflect the new meal plan. ' . $localMpName . ' => ' . $mpName);
        // I first need to debit the account for the amount of the previous meal plan.
        $localMpStartingBalance = $this->Account->getAccount($personId)->mp_starting_balance;
        $this->Account->debitCafeAccount($personId, $localMpStartingBalance, 'Meal Plan change: removing previous');
        // Add the new meal plan amount.
        $result = $this->Account->creditCafeAccount($personId, $mpStartingBalance, 'Meal plan change: adding new');
        if ($result) $isBalanceChanged = true;
      }
    }

    // Show the old balance and the new balance regardless of the situation.
    if ($isBalanceChanged) {
      $newBalance = $this->Account->getBalance($personId);
      log_message('debug', 'MODEL: Account | There was a balance change for cafe account ' . $personId . '.  $' . $currentBalance . ' => $' . $newBalance);
      $result = true;
    } else {
      log_message('debug', 'MODEL: Account | The balance for ' . $personId . ' has not changed for term ' . $localTermId . '.');
      $result = false;
    }

    // Perform the update using the updated values.
    $this->db->where('person_id', $personId);
    $this->db->update('account', $data);

    // Send the result back.
    return $result;
  }



  /* See if this person has a meal plan in the main register */
  public function hasMealPlan($personId) {
    log_message('debug', 'MODEL: Account | hasMealPlan()');

    if ($this->Account->getType($personId) == 'MEAL_PLAN') {
      return true;
    } else {
      return false;
    }
  }
  /* See if this person has a meal plan in the cache data set */
  public function hasMealPlanCache($personId) {
    log_message('info', 'MODEL: Account | hasMealPlanCache()');

    $cachedMealPlanAccounts = $this->MealPlan->getPeopleOnMealPlans();

    // Loop over the list of cached meal plans and see if this person id is in the list.
    foreach ($cachedMealPlanAccounts as $cached) {
      // If the person IDs match, they are in the list
      if ($cached->person_id == $personId) {
        log_message('info', 'MODEL: Account | ' . $personId . ' has a meal plan in the cache.');
        return true;
      }
    }

    log_message('debug', 'MODEL: Account | ' . $personId . ' does not have a meal plan in the cache.');
    // If we made it this far, we didn't find anything.
    return false;
  }





  /**
   * Removes the meal plan data from the account.  We would use this in a case where a student is no longer on a meal plan.
   * @return [int]        0 = error, 1 = removed meta data, 2 = meta data doesn't exist.
   */
  public function removeMealPlan($personId) {
    log_message('debug', 'MODEL: Account | removeMealPlan()');

    // Get the account.
    $account = $this->Account->getAccount($personId);

    // Prepare the data that will be used to update an account to a credit account.
    $data = array(
      'mp_name'             => 'None',
      'mp_starting_balance' => null,
      'type'                => 'CREDIT',
      'mp_term_id'          => null,
      'mp_term_name'        => null,
    );

    // Remove the meal plan meta data from the account register.
    $this->db->where('person_id', $personId);
    $result = $this->db->update('account', $data);
    
    // Check that the meal plan was removed successfully.
    if ($result) {
      log_message('debug', 'MODEL: Account | Successfully removed meal plan meta data from ' . $personId);
      return true;
    } else {
      log_message('error', 'MODEL: Account | There was an error in attempting to remove meal plan meta data from ' . $personId);
      return false;
    }
  }





















  /**
   * Goes through the transactions for the person in question and reports back what the total debits, credits, and final balance are.  Balances are never carried over from year to year.
   * @param  [int] $personId    A numerical person id.
   * @return [object]           A account audit object containing a boolean value for if the account is reconcile to the stored balance, total debits, total credits, calculated balance, and balance showing on account.
   */
  public function auditAccountTransactionsByAcademicYear($personId) {
    log_message('debug', 'MODEL: Account | auditAccountTransactionsByAcademicYear()');

    // Get pertinent data on this student from the database.
    $account = $this->Account->getAccount($personId);
    $accountHistory = $this->Account->getAccountHistory($personId);
    $fullName = $account->first . ' ' . $account->last;

    // Generate the balance for each transaction in accountHistory.
    // Set the debits, credits, and initial balance to 0.
    $balance = 0;
    $debits = 0;
    $credits = 0;
    $count = 0;
    log_message('debug', 'MODEL: Account | Calculating the balance for each transaction ('. sizeof($accountHistory) . ') in accountHistory for ' . $fullName . ' (' . $account->person_id . ').');

    // Loop over the account history, calcuclating the balance after each transaction.
    foreach ($accountHistory as $trans) {
      // Increment the counter.
      $count++;

      // Get the term that this perticular transaction is in.
      $trans->term = $this->AcademicTerms->getTermByDate(nice_date($trans->date));

      // Adjust the balance depending on if this is a debit or credit.
      if ($trans->transaction_type == 0) {
        // Add this transactions to the debits column.
        $debits += $trans->total;
        // Lower the balance as money is flowing out of the account.
        $balance -= $trans->total;
        // The balance at the end of this transaction is whatever the balance now is.
        $trans->account_balance = $balance;
      } else {
        // Add this transaction to the credits column.
        $credits += $trans->total;
        // Increase the balance as money is flowing into the account.
        $balance += $trans->total;
        // The balance at the end of this transaction is whatever the balance now is.
        $trans->account_balance = $balance;
      }
    }

    // Round the balance to two decimal places.
    $balance = round($balance, 2);

    // Audit the balance.
    if ($credits-$debits == $balance) {
      log_message('debug', 'MODEL: Account | The balance audit is reconciled against itself.');
      $debits = $debits * -1;
    }

    // Audit the stored balance.
    if ($balance == $account->mp_balance) {
      log_message('debug', 'MODEL: Account | The Transaction Audit is reconciled with the stored balance (' . $account->mp_balance . ').');
      $reconciled = true;
    } else {
      log_message('debug', 'MODEL: Account | The stored balance (' . $account->mp_balance . ') is NOT reconciled with the calculated balance.');
      $reconciled = false;
    }

    log_message('debug', 'MODEL: Account | ' . $account->person_id . ' Debits: ' . $debits . ', Credits: ' . $credits . ', Final balance: ' . $balance);
    log_message('debug', 'MODEL: Account | ' . $fullName . ' has completed ' . $count . ' transactions.');


    // Prepare the audit object to be returned.
    $auditData = array(
      'reconciled'    => $reconciled,
      'debits'        => $debits,
      'credits'       => $credits,
      'balance'       => $balance,
      'storedBalance' => $account->mp_balance,
    );

    // Return the information as an object.
    return (object)$auditData;
  }










































  public function getCoffeeShopTab($personId) {
    // Query the database
    $this->db->where('person_id', $personId);
    $query = $this->db->get('account', 'coffeeshop_balance');

    // Get just the balance.
    return $query->row()->coffeeshop_balance;
  }
  public function setCoffeeShopTab($personId, $balance) {
    // Put a new value into the database.
    $this->db->where('id', $personId);
    $result = $this->db->update('account', array('coffeeshop_balance' => $balance));

    if ($result) {
      log_message('debug', 'Coffee shop tab for ' . $personId . ' to ' . $balance);
      return true;
    } else {
      log_message('error', 'Coffee shop tab update for ' . $personId . ' FAILED.');
      return false;
    }

  }











}
