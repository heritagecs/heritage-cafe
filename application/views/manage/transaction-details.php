<?php $this->load->view('nav/manage'); ?>

<!-- Start of the individual view -->

<div id="" class="site-content-inner">

<?php
  echo '<h1>Transaction Details</h1>';

  // Breadcrumbs
  if (!is_null($transaction->account_id)) {
    // Setup some useful account variables.
    $accountId = $transaction->account_id;
    $fullName = $account->first . ' ' . $account->last;

    echo '<p>';
    echo 'Back to <a href="' . base_url('manage/accounts') . '">Accounts</a>';
    echo ' | ';
    echo anchor('manage/accountDetails/' . $accountId, $fullName);
    echo ' | ';
    echo $transaction->id;
    echo '</p>';
  }


  echo '<h6>Transaction ' . $transaction->id . '</h6>';

  /*
    Create a little receipt of the transaction.
   */

  // Prepare an HTML report of the transaction.
  $transactionType = $transaction->transaction_type;

  // Build the receipt HTML
  $receipt = '';
  $receipt .= '<section class="transaction-receipt">';
  if ($transaction->is_voided) $receipt .= '<div class="watermark-is-voided"><span>VOID</span></div>';
  $receipt .= '<main>';
  $receipt .= '<h3>Cafeteria Receipt</h3>';

  $receipt .= '<header>';
  $receipt .= '<p>Food Services | Heritage College and Seminary</p>';
  $receipt .= '<p><span class="">Transaction ' . $transaction->id . '</span>';
  $receipt .= '<span class="price">' . $transaction->date . '</span></p>';
  $receipt .= '</header>';
  switch($transactionType) {
    case 0:
      // This was a debit transaction.

      // If there were items to this transaction.
      if ($items_sold) {
        $receipt .= '<ul class="unstyled-list">';
        // Add each item to the receipt
        foreach ($items_sold as $item) {
          $item = $this->InventoryTable->getItem($item->inventory_id);

          $receipt .= '<li>';
          $receipt .= '<span>' . $item->id . '</span>';
          $receipt .= '<span>' . $item->description . '</span>';
          $receipt .= '<span>' . $item->price . '</span>';
          $receipt .= '</li>';
        }
        $receipt .= '<li><span>Subtotal</span><span>' . $transaction->subtotal . '</span></li>';
        $receipt .= '<li><span>GST</span><span>' . $transaction->tax . '</span></li>';
        $receipt .= '<li><span>Total</span><span>' . $transaction->total . '</span></li>';
        $receipt .= '</ul>';
      }

      break;
    case 1:
      $receipt = '<p>This was a credit transaction.</p>';
      break;
  }
  $receipt .= '</main></section>';

  echo $receipt;


  /* Actions */

  // This is going to be the void transaction button.  Only show it if the transaction was a debit (0).
  if ($transactionType == 0 && !$transaction->is_voided) {
    $action = 'void-transaction';
    $post_data = array(
      'action'  => $action,
      'key'     => 'RL6J7QWaUsZqYwTC',
    );

    echo '<section class="transaction-actions">';
    $uri = base_url() . 'manage/transaction/' . $transaction->id;
    echo form_open($uri, '', $post_data);
    echo form_submit(str_replace('-', '_', $action), ucwords(str_replace('-', ' ', $action)), 'class="btn btn-top-margin-narrow"');
    echo form_close();
    echo '</section>';
  }



  ?>

</div><!-- END of .site-content-inner -->

<!-- Load the manage.js script -->
<script src="<?php echo base_url(); ?>assets/js/manage.js"></script>
