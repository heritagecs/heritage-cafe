<!-- Start of the individual view -->

<div id="" class="site-content-inner">

<?php
echo '<h2>Utilities</h2>';

// Generate a button that allows me to sync and cache the basic supporting data of the system.
$key = getAdminUtilityKey();
$uri = 'admin/util?action=sync-supporting-data&key=' . $key;
echo anchor($uri, 'Sync Supporting Data', 'class="btn btn-small"');


// Zero out Staff account balances
$key = getAdminUtilityKey();
$uri = 'admin/util';
echo '<section style="margin-top: 3em;">';
echo '<h3>Zero Staff Account Balances</h3>';
echo '<p>A method for which an administrator can zero out the balances of all Heritage Staff accounts.';
echo '<p>Enter the word ZEROACCOUNTS in this text box before pressing the Zero Staff Account Balances button in order to confirm your intent to initiate this action.</p>';
echo form_open($uri);
echo form_hidden('action', 'zero-staff-accounts');
echo form_hidden('key', $key);
echo form_input('confirmation');
echo form_submit('submit', 'Zero Staff Account Balances', 'class="btn-small"');
echo form_close();
echo '</section>';

// Zero out Coffeeshop balance
$key = getAdminUtilityKey();
$uri = 'admin/util';

echo '<section style="margin-top: 3em;">';
echo '<h3>Zero out The Bean Balances</h3>';
echo '<p>This end-of-school-year utility will create a transaction for every open tab at The Bean cafe.  The transaction ';
echo 'will <em><strong>clear out the balance</strong></em> for each tab account.  This utility should only be run in consultation with the CFO ';
echo 'during the summer months.</p>';
echo '<p>Enter the word ZEROCOFFEESHOPTABS into the text box before running the utility.</p>';

if (isset($zero_coffeeshop_balances_result)) {
    if ($zero_coffeeshop_balances_result->result == true) {
        echo '<p class="hcs-red">All The Bean tab balances have been successfully cleared.</p>';
    } else {
        echo '<p class="hcs-red">Some or all of The Bean tabs were not cleared.  Check the logs.<br />';
        echo 'Out of ' . $zero_coffeeshop_balances_result->count . ' tab accounts, ';
        echo 'only ' . $zero_coffeeshop_balances_result->success . ' of them were cleared.</p>';
    }
}

echo form_open($uri);
echo form_hidden('action', 'zero-coffeeshop-balance');
echo form_hidden('key', $key);
echo form_input('confirmation');
echo form_submit('submit', 'Zero The Bean Tab Balances', 'class="btn-small"');
echo form_close();

echo '</section>';


?>

</div><!-- #view-dashboard DIV -->
