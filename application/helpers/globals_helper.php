<?php defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Returns the nightly maintenance key.  Allows me to hard code this key in source to protect against unwanted runs.
 * @return [type] [description]
 */
function getNightlyMaintenanceKey() {
  return 'PPgsQ8s9Sg7rRM99';
}

function getAdminUtilityKey() {
  return 'yavRUBzGhkrLreSH5U6HUGhA';
}


function getCurrentTermId() {
  return $this->Settings->getSettingValue('current_term_id');
}