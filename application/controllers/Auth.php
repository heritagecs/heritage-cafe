<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct() {
		// Call the parent constructor
		parent::__construct();

		// Load up the appropriate models.
    $this->load->model('User');
	}








  /* Instance Variables */





  /* Instance Methods */
  public function index() {
		log_message('debug', 'CONTROLLER: Auth | index()');

		// Otherwise, send them to the login page.
		$data['content_inner_view'] = 'auth/login-form';
    $this->load->view('parts/template', $data);
  }





  /**
   * [login Responsible for validating users credentials, setting session data
   * and redirecting to the appropriate user area.]
   */
  public function login() {
		log_message('debug', 'CONTROLLER: Auth->login()');

		// Validate the login and get a return value.
    $valid = $this->User->validate();

    // If User's credentials are validated.
    if($valid) {
      $data = array(
        'username'      => trim($this->input->post('username')),
        'is_logged_in'  => true,
      );

      // Set session data.
      log_message('debug', 'CONTROLLER: Auth | Creating session data... ' . json_encode($data));
      $this->session->set_userdata($data);

			// Set the session variable for admin access.
			$valid = $this->User->is_admin();
			if ($valid) {
				$this->session->set_userdata('is_admin', true);
			} else {
				$this->session->set_userdata('is_admin', false);
			}

      // Redirect to the store.
      log_message('debug', 'CONTROLLER: Auth | $_SESSION (id=' . session_id() . '): ' . json_encode($_SESSION));
      log_message('debug', 'CONTROLLER: Auth | Redirecting to the storefront...');
      redirect('store/index');
    } else {
      // If not valid, go to the login form.
      log_message('debug', 'CONTROLLER: Auth | Oops, the user was not validated, returning to the login form.');
      $this->index();
    }


  }





  /**
   * Destroys the session and sends the user back to the login page.
   * @return [type] [description]
   */
  public function logout() {
    // Destroy the session.
    session_destroy();

    // Redirect to the login page.
		redirect('auth');
  }











}
