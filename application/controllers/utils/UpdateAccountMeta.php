<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UpdateAccountMeta extends CI_Controller {

  public function __construct() {
    // Call the parent constructor
		parent::__construct();

    // Load up the appropriate models.
    $this->load->model('User');
    $this->load->model('Role');
    $this->load->model('MealPlan');
    $this->load->model('populi/PopuliApi');
    $this->load->model('Account');
    $this->load->model('Transaction');
    $this->load->model('Sync');
    $this->load->model('InventoryTable');
    $this->load->model('Sales');
    $this->load->model('MealPlan');
    $this->load->model('AcademicTerms');
    $this->load->model('PaymentType');
    $this->load->model('ItemsSold');
    $this->load->model('common/Settings');
  }

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index()
	{
    log_message('debug', '-----> CONTROLLER: UpdateAccountMeta');
    $this->load->view('parts/header-test');

    // Execute the update.
    $this->Sync->updateAccountMetaData();

		$this->load->view('parts/footer');
	}



}
