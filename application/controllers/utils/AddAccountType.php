<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AddAccountType extends CI_Controller {

  public function __construct() {
    // Call the parent constructor
		parent::__construct();

    // Load up the appropriate models.
    $this->load->model('User');
    $this->load->model('Role');
    $this->load->model('MealPlan');
    $this->load->model('populi/PopuliApi');
    $this->load->model('Account');
    $this->load->model('Transaction');
    $this->load->model('Sync');
    $this->load->model('InventoryTable');
    $this->load->model('Sales');
    $this->load->model('MealPlan');
    $this->load->model('AcademicTerms');
    $this->load->model('PaymentType');
    $this->load->model('ItemsSold');
    $this->load->model('common/Settings');
  }

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index() {
        log_message('debug', '-----> CONTROLLER: UpdateAccountMeta');
        $this->load->view('parts/header-test');

        // Only folks from this machine can run this utility.
        $adminIP = '10.0.1.7';

        // Execute the update.
        $accounts = $this->Account->getAccounts();
        $reportData = array(
            'MEAL_PLAN' => 0,
            'PRE_PAID'  => 0,
            'CREDIT'    => 0,
        );

        // If this is an administrator machine (like Russ')
        if ($this->input->ip_address() == $adminIP) {

            foreach ($accounts as $ac) {
                $pId = $ac->person_id;
    
                /*if ($ac->is_mp_added) {
                    // If the account is_mp_added, this is a meal plan
                    $this->Account->setType($pId, 'MEAL_PLAN');
                    $reportData['MEAL_PLAN']++;
                } else if (!$ac->is_mp_added && $ac->mp_balance > 0) {
                    // If the account is no is_mp_added, but their balance is positive, its a prepaid
                    $this->Account->setType($pId, 'PRE_PAID');
                    $reportData['PRE_PAID']++;
                } else {
                    // Otherwise, its a credit account
                    $this->Account->setType($pId, 'CREDIT');
                    $reportData['CREDIT']++;
                }*/
            }
    
    
            
            /*
                Display the data returned by the test function.
                */
            echo 'RETURNED TYPE: ' . gettype($reportData) . '<br />';
            if (is_array($reportData)) {
                echo 'Yep, that was an array... ' . count($reportData) . ' records returned.';
                echo '<br />';
                echo '<br />';
                foreach ($reportData as $d) {
                echo var_dump($d);
                echo '<br />';
                echo '<br />';
                }
            } else {
                echo 'Only one element was returned.';
                echo '<br />';
                echo '<br />';
                var_dump($reportData);
            }

        } else {
            // If this is not an adminstrator machine
            echo 'Utilities disabled.';
        }




		$this->load->view('parts/footer');
	}



}
