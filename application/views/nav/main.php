<?php
$pages = array(
  // url    => display name
  'store'   => 'store',
  'reports' => 'reports',
  'manage'  => 'manage',
);


$is_logged_in = $this->session->is_logged_in;

// Check if the session is set.
if (isset($is_logged_in) || $is_logged_in == true) {
?>

<nav id="hcs-site-navigation" class="hcs-site-navigation hcs-navigation site-navigation" role="navigation">
  <ul class="unstyled-list">

    <?php
    // Loop through the array of page names.
    foreach ($pages as $page => $label) {
      echo '<li>';
      // If this person is not a manager or admin, don't show the Manage link.
      if ($page == 'manage') {
        if (!$this->Role->validateAccess(array('manager', 'admin'))) continue;
      }
      echo '<a href="' . base_url() . $page . '" class="site-link">' . $label . '</a>';
      echo '</li>';
    }
    ?>

  </ul>
</nav>

<?php

} // End of session check.

?>
