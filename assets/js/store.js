/**
 * Javascript file specifically for store use.
 */














/**
 *
 * When the document is loaded, run this stuff.
 *
 */
$( document ).ready( function() {
	console.log( "Document loaded" );

  // Load the default category.
	loadDefaultCategoryItems();

	// Clear the cart.
	clearCart();

	// Setup the payment type handlers.
	handlePaymentType();






	/**
	 * One-Off Event Handlers
	 */

	// Meal Plan Account select box.
	/*

	$('#action-account').click( function(event) {
		// Replace the input field with a button.
		$('#cash-received').replaceWith(cashButtonHtml());
		// Event handler for the cash button.  Reloads this function.
		$("#action-cash").click( function(event) { actionCash(event) });
	});*/

	/* Event handler for the complete transaction button */
	$("#complete-transaction").click( function(event) { completeTransaction(event); });

	/* Modal event handlers */
	$('#modal-ok').click( function(event) {
		deactivateButton('#modal-ok');
	});


});





/**
 * Ask the server for the entire inventory from the database.
 */
function loadInventoryData() {
  $.ajax({
    url: BASE_URL + "Inventory/getinventory",
    success: function(result) {
      console.log(result);
    }
  });
}



















/**
 * Adds the clicked item to the cart.
 */
function addItemToCheckout(id) {
	console.log('addItemToCheckout, ID = ' + id);

  // Get the data for this item ID from the database.
  $.ajax({
    url: BASE_URL + "Inventory/getItem/" + id,
		success: function(result) {
			// Get the json and turn it into an object.
      var item = JSON.parse(result);

      // Create a new array for the items.
      var cartItems = new Array();

      // Add this item to session storage.
			// If there is no session storage
      if (sessionStorage.getItem('cartItems') === null) {
				console.log('Creating new cart.');
				// This is the first item in the array.
        cartItems[0] = item;

        // Put that array into session storage - must store as JSON string.
        sessionStorage.setItem('cartItems', JSON.stringify(cartItems));

      } else {
				console.log('There is already a cart in session storage');

				// Get the array of items from session storage
				cartItems = JSON.parse(sessionStorage.getItem('cartItems'));

				// Push the new item onto the currentCart array.
				cartItems.push(item);

        // Put the new array into session storage - must store as JSON string.
        sessionStorage.setItem('cartItems', JSON.stringify(cartItems));

      }

			// Calculate if the item is taxable.
			var taxable = '';
			if (item.tax_code == 1 || item.tax_code == 3) {
				taxable = 't';
			}

			// Build some html for the item.
      var html = '<li id="li-cart-item-' + item.id + '" class="li-cart-item"';
			html += 'onclick="removeItemFromCart(\'' + item.id + '\')">';
			html += '<div class="item">' + item.description;
      html += '</div><span class="item-taxcode">' + taxable + '</span><span class="price">' + parseFloat(Math.round(item.price * 100) / 100).toFixed(2); + '</span>';
			html += '</li>';

			// Update the totals.
			updateTotals();

      // Append the cart with the new item.
      $('section.items ul').append(html);

			// If the payment type has been set and it is cash, check the input against the total.
			// Disable the complete button if the total is more than the cash.
			cartPaymentType = parseInt(sessionStorage.getItem('cartPaymentType'));
			if (cartPaymentType == 4) {
				// Get the cart total and the value of the input field.
				var cartTotal = parseFloat(sessionStorage.getItem('cartTotal'));
				var input = parseFloat($('#cash-received').val());
				if (cartTotal > input) {
					console.log('addItemToCheckout() | Total is more than the input, disable Complete button.');
					// Reset the payment type.
					resetPaymentType();
				}
			}
    }
  });
}

/**
 * Removes an item from the cart both visibly and in session storage and re-calculates the total.
 * @param  {[type]} id A numberical value representing the position of the item in the array of cartItems.
 */
function removeItemFromCart(id) {
	// Build the element id from the input parameter.
	var elementId = 'li-cart-item-' + id;

	/*
	Remove the item from the cartItems array in session storage.
	 */
	// Get the content in the cart at present.
	var cartItems = JSON.parse(sessionStorage.getItem('cartItems'));
	var newCartItems = new Array();

	// Loop over the cart items
	for (x = 0; x < cartItems.length; x++) {
		// Get the current item.
		var item = cartItems[x];
		// Generate an array of items we're keeping.
		if (item.id != parseInt(id)) {
			newCartItems.push(item);
		} else {
			console.log('removeItemFromCart() | Removing \'' + item.description + '\' from the cart.');
		}
	}

	// Update session storage to reflect the new cart list.
	sessionStorage.setItem('cartItems', JSON.stringify(newCartItems));

	// Remove the item from view.
	document.getElementById(elementId).remove();

	// Update the total to reflect the change.
	updateTotals();
}















/**
 * Dynamically updates the total based on values in the cartItems session storage.
 */
function updateTotals() {
	// Set the variables we'll need for complicated maths.
	var subtotal = 0;
	var tax = 0;
	var total = 0;

	// Get the list of items from sessionStorage.
	var storedItems = JSON.parse(sessionStorage.getItem('cartItems'));

	// Loop through each item.
	$.each(storedItems, function(key, item) {
		// Cast the price to a float.
		price = parseFloat(item.price);

		// Calculate the tax on this item.
		if (item.tax_code == 1 || item.tax_code == 3) {
			console.log('The item ' + item.description + ' is taxable.');
			tax += price * 0.13;
		}

		// Calculate the new subtotal.
		subtotal += price;
	});


	// Calculate the total.
	total = subtotal + tax;

	// Check if the paymentType is set and is cash.  If so, round the total to the nearest nickle ($0.05).
	var cartPaymentType = sessionStorage.getItem('cartPaymentType');
	if (cartPaymentType && cartPaymentType == 4) {
		// Rounding calculations
		var total = Math.round(total * 20) / 20;

		console.log('Round the total to the nearest nickle ($0.05).');
		console.log('Rounded total: ' + total);
	}

	console.log('Subtotal: ' + subtotal.toFixed(2) + ', Tax: ' + tax.toFixed(2) + ', Total: ' + total.toFixed(2));

	// Update session storage.
	sessionStorage.setItem('cartSubtotal', subtotal.toFixed(2));
	sessionStorage.setItem('cartTax', tax.toFixed(2));
	sessionStorage.setItem('cartTotal', total.toFixed(2));

	// Update the UI.
	$('#subtotal').text(subtotal.toFixed(2));
	$('#tax').text(tax.toFixed(2));
	$('#total').text(total.toFixed(2));
}
















function actionCash(event) {
	console.log('actionCash()');

	// Write some html that will be insertted shortly.
	var inputField = '<input name="cash_received" value="" id="cash-received" class="cash-received" autofocus="" type="text">';

	// Replace the Cash button with an input field for the amount.
	$('#action-cash').replaceWith(inputField);
	document.getElementById('cash-received').focus(); // Put the focus on the input field.

	// Reselect the On Account item in the Meal Plan select box.
	$('#action-account').val('-1');

	// Set the payment type to cash and disable the Complete transaction button until
	// the case is greater than the transaction's total.
	setPaymentType(event);
	deactivateButton('#complete-transaction');

	// Loop until the cash entered is greater than the total.
	var cartTotal = parseFloat(sessionStorage.getItem('cartTotal'));
	var input = 0;
	console.log('cartTotal: ' + cartTotal);

	// Listen for the change to the user's input.
	$('#cash-received').keyup(function() {
		// Get the value of the input field
		input = parseFloat($('#cash-received').val());
		console.log('actionCash(): Cash was entered.  input = ' + input);

		// Once the user has entered an appropriate value, enable the complete transaction button.
		if (input >= cartTotal) {
			activateButton('#complete-transaction');
		} else {
			deactivateButton('#complete-transaction');
		}
	});


}

/**
 * What to do when the on account button is clicked.
 * @return {[type]} [description]
 */
function actionOnAccount(event) {
	console.log('actionOnAccount()');

	// Send the event to setPaymentType to set the payment type and account id.
	setPaymentType(event)
}









/**
 * Sets the payment type based on user input to session storage.
 * @param {[type]} event The event for the button that was clicked and called this function.
 */
function setPaymentType(event) {
	var message = "";
	var accountId = -1;

	// Stop the page from reloading.
	event.preventDefault();

	// Recalculate the totals in case we have changed payment types.
	updateTotals();

	// Put the element into a variable.
	id = event.target.id;

	// Go get the cart total.
	var cartTotal = 0;
	cartTotal = sessionStorage.getItem('cartTotal');

	// Depending on which button was clicked.
	switch(id) {
		case 'action-debit':
			message = 'Debit';
			sessionStorage.setItem('cartPaymentType', 2);
			break;
		case 'action-credit':
			message = 'Credit';
			sessionStorage.setItem('cartPaymentType', 3);
			break;
		case 'action-cash':
			message = 'Cash';
			sessionStorage.setItem('cartPaymentType', 4);
			break;
		case 'action-coffeeshop-tab':
			message = 'Coffee shop tab';
			accountId = $("#action-coffeeshop-tab").val();
			console.log('THE ACCOUNT: ' . accountId);
			// Set the session variables.
			sessionStorage.setItem('cartPaymentType', 5);
			sessionStorage.setItem('cartAccountId', accountId);

			// If the cash input field exists, replace it with the cash button.
			if ($('#cash-received').length) {
				$('#cash-received').replaceWith(cashButtonHtml());
				console.log('Replace the input field.');
			}
			
			// If the account is not -1 and there are items in the cart, enable the Complete button.
			if (accountId != -1 && cartTotal > 0) {
				activateButton('#complete-transaction');
				handlePaymentType();
			}
			break;
		default:
			// If this is not one of the others, we assume it's on account.
			message = 'On Account';
			accountId = $("#action-account").val();
			// Set the session variables.
			sessionStorage.setItem('cartPaymentType', 1);
			sessionStorage.setItem('cartAccountId', accountId);

			// If the cash input field exists, replace it with the cash button.
			if ($('#cash-received').length) {
				$('#cash-received').replaceWith(cashButtonHtml());
				console.log('Replace the input field.');
			}

			// If the account is not -1 and there are items in the cart, enable the Complete button.
			if (accountId != -1 && cartTotal > 0) {
				activateButton('#complete-transaction');
				handlePaymentType();
			}
			break;
	}

	//  Build a nice message for the logs.
	var returnMessage = 'setPaymentType(): ' + sessionStorage.getItem('cartPaymentType') + ' (' + message + ')';
	if (accountId != -1) {
		returnMessage += ', AccountId: ' + sessionStorage.getItem('cartAccountId');
	}

	console.log(returnMessage);
}





/**
 * Function to call when you want to setup handlers for the payment type buttons.
 * @return {[type]} [description]
 */
function handlePaymentType() {
	console.log('handlePaymentType()');
	/* Handlers for the payment type buttons */
	$("#payment-actions .payment-type").click( function(event) {
		// Figure out which payment type the user clicked.
		id = event.target.id;
		console.log('A payment type was selected.');

		// Stop the page from reloading.
		event.preventDefault();

		// Figure out which payment button was clicked.
		if (id == 'action-cash') {
			console.log('User selected the Cash payment type.');
			// Process the cash event.
			actionCash(event);
		} else if (id == 'action-debit') {
			console.log('User selected the Debit payment type.');
			// Process the debit event
		} else if (id == 'action-credit') {
			console.log('User selected the Credit payment type.');
			// Process the credit event
		} else if (id == 'action-coffeeshop-tab') {
			console.log('User selected the coffee shop tab payment type.');
			// Attach an event handler to the drop down to watch for changes.
			$("#action-coffeeshop-tab").change( function(event) {
				actionOnAccount(event);
			});			
		} else {
			console.log('User selected the On Account payment type.');
			// Attach an event handler to the drop down to watch for changes.
			$("#action-account").change( function(event) {
				actionOnAccount(event);
			});
		}

	});
}


/**
 * Reset the payment type by clearing the session storage variable and resetting the buttons.
 */
function resetPaymentType() {
	console.log('resetPaymentType()');
	// Disable the complete button.
	deactivateButton('#complete-transaction');

	// Clear the session storage variables for the payment type.
	sessionStorage.removeItem('cartPaymentType');
	sessionStorage.removeItem('cartAccountId');

	// Replace the cash input field with the button.
	$('#cash-received').replaceWith(cashButtonHtml());

	// Reselect the On Account item in the Meal Plan select box.
	$('#action-account').val('-1');

	// Call handlePaymentType again to set up the event listeners.
	handlePaymentType();
}






/**
 * A function that checks all the heristics in order to make a decision to enable
 * the Complete Transaction button.
 * @return {[type]} [description]
 */
function activateCompleteTransactionButton() {
	console.log('activateCompleteTransactionButton()');

	// Get relevant data points to default values.
	var cartTotal = 0;
	var cartPaymentType = 0;
	var cartAccountId = 0;

	// Set the relevant data points to default values.
	cartTotal = sessionStorage.getItem('cartTotal');
	cartPaymentType = sessionStorage.getItem('cartPaymentType');
	cartAccountId = sessionStorage.getItem('cartAccountId');
}


























/**
 * Completes the transaction by posting the transaction to the database and charging
 * the user either by account, cash, credit, or debit.
 * @return {[type]} [description]
 */
function completeTransaction(event) {
	console.log('completeTransaction()');

	// Deactive the complete transaction button
	deactivateButton('#complete-transaction');

	// Stop the page from reloading.
	event.preventDefault();

	// Transaction variables
	cartItems = sessionStorage.getItem('cartItems');
	cartSubtotal = sessionStorage.getItem('cartSubtotal');
	cartTax = sessionStorage.getItem('cartTax');
	cartTotal = sessionStorage.getItem('cartTotal');
	cartPaymentType = sessionStorage.getItem('cartPaymentType');

	// Set the account depending on the payment type
	if (cartPaymentType == 1 || cartPaymentType == 5) {
		cartAccount = sessionStorage.getItem('cartAccountId');
	} else {
		cartAccount = -1;
	}

	// If the payment type is 4 (cash), get the value in the cash-received field and calculate the change.
	if (cartPaymentType == 4) {
		var cashReceived = $('#cash-received').val();
		var changeRequired = cashReceived - cartTotal;
		console.log('Cash info. cashReceived = ' + cashReceived + ', changeNeeded = ' + changeRequired);
	}

	console.log('PaymentType: ' + cartPaymentType);
	console.log('cartAccount: ' + cartAccount);

	// Prepare the data object.
	var data = {
		items: cartItems,
		subtotal: cartSubtotal,
		tax: cartTax,
		total: cartTotal,
		paymentType: cartPaymentType,
		account: cartAccount,
	};

	// Convert the data to a JSON string.
	json = JSON.stringify(data);
	console.log(json);

	// Create Ajax request.
	$.ajax( {
		url: BASE_URL + 'store/completeTransaction',
		type: 'POST',
		data: json,
		contentType: 'application/json',
		success: function(response) {
			console.log('Response from Save/completeTransaction: ' + response);

			// If the transaction type is cash, pop the transaction summary modal.
			if (cartPaymentType == 4) {
				console.log('Payment type is Cash, pop the Transaction Summary dialog.');
				// Set the values of the transaction summary fields accordingly.
				$('#modal-cash-received').text(twoDecimals(cashReceived));
				$('#modal-change-required').text(twoDecimals(changeRequired));

				// Pop the modal.
				document.getElementById('modal-transaction-summary').style.display = 'block';
			} else {
				console.log('Payment type is On Account');
				// Reload the page on success after reading the response.
				window.location = BASE_URL + "store";
			}

		},
		error: function(response) {
			console.log('Response from Save/completeTransaction: ' + response);
		}
	});
}




/**
 * Loads the items from the currently selected category into the main item area.
 * @param  {[type]} id [description]
 * @return {[type]}    [description]
 */
	function loadCurrentCategoryItems(id) {
	// Tell the log what parameters we got.
	console.log('loadCurrentCategoryItems() PARAMETERS: id = ' + id);

	// Get the category name and update the heading.
	$.getJSON(BASE_URL + 'inventory/getCategoryName/' + id, function(category) {
		// Build some html
		html = '<h3 id="store-current-category-title">' + category + '</h3>';
		// Display the category name.
		$('#store-current-category-title').replaceWith(html);
	});


	// Go get the inventory data for this particular category.
	var is_active = 1;
	$.getJSON(BASE_URL + 'inventory/getInventoryByCategory/' + id + '/' + is_active, function(inventory) {
		// Build some HTML with the inventory data.
		html = '<div class="section-body clear">';
		$.each(inventory, function(key, item) {
			html += '<button class="btn btn-item" type="button" ';
			html += 'onclick="addItemToCheckout(' + item.id + ')">';
			html += item.description;
			html += '</button>';
		});
		html += '</div>';

		logForLog = html.substring(0, 19) + '...' + html.substring(html.length - 24, html.length);
		console.log('loadCurrentCategoryItems() RETURN: ' + logForLog);

		// Add the items to the category section.
		$('#store-current-category div.section-body').replaceWith(html);
	});
}



/**
 * This function uses heuristics to load the inventory items for the default category.
 * @return {[type]} [description]
 */
function loadDefaultCategoryItems() {
	var date = new Date();
	var currentHour = date.getHours();
	var loadThisCategory = 6;

	// Get the storefront cookie.
	var STOREFRONT = getCookie('storefront');
	console.log(STOREFRONT);

	// Which storefront
	if (STOREFRONT == 'CAFETERIA') {
		if (currentHour < 10) {
			// Load up Breakfast items.
			loadThisCategory = 3;
		} else if (currentHour >= 11 && currentHour < 13) {
			// Load up on Lunch items (sandwiches).
			loadThisCategory = 7;
		} else if (currentHour >= 16 && currentHour < 18) {
			// Load up supper items.
			loadThisCategory = 7;
		} else {
			// Otherwise, load up snack items.
			loadThisCategory = 6;
		}
	} else if (STOREFRONT == 'COFFEESHOP') {
		// Otherwise, load up The Bean stuff.
		loadThisCategory = 10;
	}

	// Load the appropriate category.
	loadCurrentCategoryItems(loadThisCategory);
}





















/**
 * Clears the cart of any items.
 * @return {[type]} [description]
 */
function clearCart() {
  console.log("Empty the cart.");
	// Deactivate the complete transaction button.
	deactivateButton('#complete-transaction');

	// Replace the input field with the cash button.
	$('#cash-received').replaceWith(cashButtonHtml());

	// Reselect the On Account item in the Meal Plan select box.
	$('#action-account').val('-1');

  // Clear session storage.
  sessionStorage.clear();

  // Empty the cart.
  $('section.items ul').empty();

	// Update the totals.
	updateTotals();

	// Enable event handlers for the paymentType buttons.
	handlePaymentType();
}




function newCart() {
	// Clear the cart.
	clearCart();

	// Reload the page.
	window.location = BASE_URL + 'store/index';
}














function twoDecimals(n) {
	var number = parseFloat(n);

	number = (Math.round(n * 100) / 100).toFixed(2);

	return number;
}








/**
 * Deactivates the button specified.
 * @param  {[type]} selector A text string of the selector for the button you want to
 * deactivate.
 */
function deactivateButton(selector) {
	// Deactive the button till we know the server got the data.
	$(selector).attr('disabled','disabled').addClass('btn-disabled');
}


function activateButton(selector) {
	// Activate the button.
	$(selector).removeAttr('disabled','disabled').removeClass('btn-disabled');
}





/**
 * Returns the HTML for the Cash button.
 * @return {[type]} [description]
 */
function cashButtonHtml() {
	var cashButton = '<button id="action-cash" class="btn btn-pos-payment payment-type">Cash</button>';
	return cashButton;
}






















function getCookie(name) {
	// Split cookie string and get all individual name=value pairs in an array
	var allTheCookies = document.cookie.split(";");
	
	// Loop through the array elements
	for(var i = 0; i < allTheCookies.length; i++) {
		var cookiePair = allTheCookies[i].split("=");
		
		/* Removing whitespace at the beginning of the cookie name
		and compare it with the given string */
		if(name == cookiePair[0].trim()) {
			// Decode the cookie value and return
			return decodeURIComponent(cookiePair[1]);
		}
	}
	
	// Return null if not found
	return null;
}