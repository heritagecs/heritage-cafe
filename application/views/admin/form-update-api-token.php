<?php

($apiToken) ? $buttonMessage = 'Update API Key' : $buttonMessage = 'Create API Key';


// Prepare some hidden attributes for this Populi credentials form.
// Open the form.
$hidden = array(
  'action'  => 'update-api-token',
);
echo form_open('admin/settings', 'class="form-update-api-token"', $hidden);
// The Username field.
echo form_label('Populi Username', 'api-username');
echo form_input(array('name' => 'api_username', 'id' => 'api-username', 'required' => 'required',));
// The password field.
echo form_label('Populi Password', 'api-password');
echo form_password(array('name' => 'api_password', 'id' => 'api-password', 'required' => 'required'));
// The submit button.
echo form_submit('update_api_token', $buttonMessage);
echo form_close();

?>
