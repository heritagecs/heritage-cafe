<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width-device-width, initial-scale=1" />

	<title>Heritage Cafeteria</title>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />
	<script src="https://kit.fontawesome.com/2818996bc8.js" crossorigin="anonymous"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/config.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/script.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/store.js"></script>

	<script src="https://use.typekit.net/sok7lmx.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

</head>

<body>
<div id="page" class="site">
	<header id="masthead" class="hcs-site-header site-header">
		<div class="hcs-site-header-branding">
			<?php
				if($this->session->is_logged_in) {
					// Show the logo and Site Title
					echo '<div class="hcs-site-header-logo-and-title">';
					$this->load->view('parts/logo-header-simple');
					echo '<span class="hcs-site-header-title">Food Services</span>';
					echo '</div>';

					// Show the store front name.
					if (isset($_COOKIE['storefront'])) {
						$sf = $_COOKIE['storefront'];
					} else {
						$sf = 'NOT SET';
					}
					switch ($sf) {
						case 'CAFETERIA':
							$sfn = 'Lily Ruth Catering';
							break;
						case 'COFFEESHOP':
							$sfn = 'The Bean';
							break;
						default:
							$sfn = 'Storefront NOT SET';
					}
					echo '<div class="hcs-site-header-storefront">';
					echo '<span class="hcs-site-header-storefront-name">' . $sfn . '</span>';
					echo '<span class="text-small"><a href="/store/set_storefront">';
					if ($sf) echo '<i class="fal fa-edit"></i></a></span>';
					echo '</div>';

					// Load the user's nav menu.
					$this->load->view('nav/user');
				}
			?>
		</div>



		<?php
		// If logged in, show the menus
		if ($this->session->is_logged_in) {
			$this->load->view('nav/main');
		}
		?>

	</header>



	<div id="container" class="site-content">

<!-- End of Header Section -->
