<?php $this->load->view('parts/header'); ?>
<?php $this->load->view('nav/manage'); ?>
<?php
// Check for the dev flag.
(ENVIRONMENT === 'development') ? $devFlag = true : $devFlag = false;
(ENVIRONMENT === 'testing') ? $testFlag = true : $testFlag = false;

if ($devFlag) {
	$disableButtonClass = ' btn-disabled';
	$disabledHref = 'javascript: void(0)';
}

// Prep the data depending on the storefront
// A little counting.
$mealPlanCount = 0;
$prePaidCount = 0;
$staffCount = 0;
$totalAccountsCount = sizeof($accounts);

if (STOREFRONT === 'CAFETERIA') {
	foreach($accounts as $a) {
		if ($a->type == 'MEAL_PLAN') $mealPlanCount++;
		if ($a->type == 'PRE_PAID') $prePaidCount++;
		if ($a->type == 'STAFF') $staffCount++;
	}
} else if (STOREFRONT === 'COFFEESHOP') {
	$coffeeShopTabsInUse = 0;
	foreach ($accounts as $a) {
		if ($a->coffeeshop_balance != 0) $coffeeShopTabsInUse++;
	}
}

?>
<!-- Start of the individual view -->

	<div id="" class="site-content-inner">

	<?php
	if (STOREFRONT === 'CAFETERIA') {
		echo '<h1>Accounts - Lily Ruth Catering</h1>';
	} else if (STOREFRONT === 'COFFEESHOP') {
		echo '<h1>Accounts - The Bean</h1>';
	}
	?>

		<section class=" manage-populi-sync">
			<?php

			// Build and display the Full Account Sync button.
			($devFlag) ? $href = $disabledHref : $href = site_url() . 'manage/syncAccounts';
			$attr = 'id="update-populi-account-balances" class="btn btn-primary' . $disableButtonClass . '"';
			echo '<a href="' . $href . '" ' . $attr . '>Full Account Sync</a>';
			?>

			<a id="update-populi-account-balances" class="btn" href="<?php echo site_url(); ?>manage/updateLocalAccounts">Update Local Accounts</a>

			<?php
			
			if (STOREFRONT === 'CAFETERIA') {
				// Build and display the button to allow someone to update the account balances in Populi.
				($devFlag) ? $href = $disabledHref : $href = site_url() . 'manage/updatePopuliAccountBalances';
				$attr = 'id="update-populi-account-balances" class="btn' . $disableButtonClass . '"';
				echo '<a href="' . $href . '" ' . $attr . '>Update Populi Account Balances</a>';
			} else if (STOREFRONT === 'COFFEESHOP') {	
				// Build and display a button for updating The Bean tab balances.
				($devFlag) ? $href = $disabledHref : $href = site_url() . 'manage/updatePopuliCoffeeshopTabBalances';
				$attr = 'id="update-update-coffeeshop-tab-remote-balances" class="btn' . $disableButtonClass . '"';
				echo '<a href="' . $href . '" ' . $attr . '>Update Populi Tab Balances for The Bean</a>';
			}
			?>
		</section>

		<p>A Full Account Sync runs daily at 2:00 a.m.<br />
		Last full sync: <code><?php echo $lastFullSync; ?></code></p>

		<section class="local-account-stats">
			<?php
	    // Get the list of meal plan accounts from Populi
			$populiBalance = 0;
			?>
			<ul class="no-bullets">
				<li>Total local accounts: <code><?php echo $totalAccountsCount; ?></code></li>
				<li>Active accounts: <code><?php echo count($accountsActive); ?></code><br /><span class="text-small">(current semester students & staff - most accounts don't appear in the store)</span></li>				
				

				<?php
				// Conditional logic based on the storefront.
				if (STOREFRONT === 'CAFETERIA') {
					echo '<li>Accounts on a Heritage Meal Plan (<i style="font-size: 0.825em;" class="fas fa-utensils"></i>): ';
					echo '<code> ' .$mealPlanCount . '</code></li>';
					echo '<li>Lily Ruth Pre-Paid Accounts (<i style="font-size: 0.825em;" class="fas fa-wine-glass-alt"></i>): ';
					echo '<code>' . $prePaidCount . '</code></li>';
					echo '<li>Staff & Faculty Accounts (<i style="font-size: 0.825em;" class="fas fa-user-friends"></i>): ';
					echo '<code>' . $staffCount . '</code></li>';
				} else if (STOREFRONT === 'COFFEESHOP') {
					echo '<li>Tabs in Use (<i style="font-size: 0.825em;" class="far fa-money-check-edit"></i>): ';
					echo '<code>' . $coffeeShopTabsInUse . '</code></li>';
				}
				?>
			</ul>
			
			<?php



			// A little block that shows the number of active accounts and the filter.
			echo '<section style="display: flex; justify-content: space-between; font-weight: 300;">';

			// A little block that shows the number of active accounts and the filter.
			echo '<span>';
			// Display the current filer
			($filter_type) ? $filterMeta = $filter_type : $filterMeta = 'None';
			echo '<span>Current Filter: <strong>' . $filterMeta . '</strong></span>';
			if ($filter_type) echo '<a class="text-small" style="margin-left: 0.45em;" href="/manage/accounts"><i class="fad fa-times-circle"></i></a>';
			echo '<br />';

			// Select the filter
			echo '<span class="text-small">Select Filter: ';
			if (STOREFRONT === 'CAFETERIA') {
				echo '<a href="/manage/accounts?type=MEAL_PLAN">Heritage Meal Plans</a> | ';
				echo '<a href="/manage/accounts?type=PRE_PAID">Lily Ruth Pre-Paid</a> | ';
				echo '<a href="/manage/accounts?type=STAFF">Staff & Faculty</a>';
			} else if (STOREFRONT === 'COFFEESHOP') {
				echo '<a href="/manage/accounts?type=ONLY_TABS">Only accounts with tabs</a>';			
			}
			echo '</span>';
			// Close the filter container
			echo '</span>';
			
			echo '<span><i class="fa fa-check-circle" aria-hidden="true"></i> indicates an active account.</span>';
			echo '</section>';
			?>

		</section>

    <section class="manage-accounts">
			<?php $this->load->view('manage/accounts-table'); ?>
		</section>
	</div><!-- #view-dashboard DIV -->

<!-- End of the individual view -->

<?php $this->load->view('parts/footer'); ?>
