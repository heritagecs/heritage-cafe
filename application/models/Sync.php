<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sync extends CI_Model {

  public function __construct() {
    // Call the parent constructor
		parent::__construct();

    // Load up the appropriate models.
    $this->load->model('User');
    $this->load->model('MealPlan');
    $this->load->model('populi/PopuliApi');
    $this->load->model('populi/PopuliApiCache');
    $this->load->model('Account');
  }






  public function fullAccountSync() {
    log_message('info', 'MODEL: Sync | fullAccountSync()');

    // Generate a timestamp.
    $beforeSync = now();
    log_message('debug', 'MODEL: Sync | fullAccountSync: Timestamp_START ' . $beforeSync);

    // First update the local accounts from Populi.
    $this->Sync->updateLocalAccounts();

    // Second, pass the new balances back to Populi.
    $this->Sync->updatePopuliAccountBalances();

    // Third, update the coffee shop tab balance to Populi.
    $this->Sync->updatePopuliCoffeeshopTabBalances();

    // Gerenate a new timestamp.
    $afterSync = now();
    log_message('info', 'MODEL: Sync | fullAccountSync: Timestamp_END ' . $afterSync);

    // Calculate the difference.
    $timeToSync = $afterSync - $beforeSync;

    // Update the database.
    $this->updateLastFullSyncTimestamp();

    // Log the difference.
    if ($timeToSync == 1) {
      log_message('debug', 'MODEL: Sync | fullAccountSync took ' . $timeToSync . ' second.' . PHP_EOL);
    } else {
      log_message('debug', 'MODEL: Sync | fullAccountSync took ' . $timeToSync . ' seconds.' . PHP_EOL);
    }
  }






  /**
   * Update the local account for this person based on information in myHeritage.
   */
  public function updateLocalAccount($personId, $updateCache = false) {
    log_message('debug', 'MODEL: Sync | Update the local account.');
    // Do different things depending on the account type.
    $accountType = $this->Account->determineType($personId);


    // Check the Meal Plan records in myHeritage and update as needed.
    // Get a list of everyone on a meal plan.
    $mealPlanList = $this->MealPlan->getPeopleOnMealPlans($updateCache);
    $found = false;
    // Loop over the list of meal plan accounts in the cache.
    foreach ($mealPlanList as $rec) {
      // See if this person is in the list.
      if ($personId == $rec->person_id) {
        log_message('debug', 'MODEL: Sync | Yep, ' . $personId . ' is on a Heritage Meal Plan.');
        // See if the meal plan is new or changed and needs to be updated.
        $this->Account->addMealPlanData($rec);
        $this->Account->setIsActive($rec->person_id); 
        $found = true;
      }
    }
    if (!$found) {
      // Clear out the Meal Plan data if this person is no longer on a Meal Plan.
      log_message('debug', 'MODEL: Sync | No, ' . $personId . ' is not on a Heritage Meal Plan.');
      // Remove the meal plan from the persons account.  Effectively making this a credit andor pre-paid account.
      $this->Account->removeMealPlan($personId);
    }

    // Re-write the type.
    $this->Account->setType($personId, $this->Account->determineType($personId));
    if ($this->Account->getType($personId) == 'STAFF') $this->Account->setIsActive($personId);
    
    // Return some metrics
    return $found;
  }







  /**
   * [updateLocalAccounts Update the local accounts based on information from Populi such
   * as person name, and if they have added a new meal plan.]
   * @return [type] [description]
   */
  public function updateLocalAccounts() {
    log_message('info', 'MODEL: Sync | updateLocalAccounts()');

    // Generate a timestamp.
    $beforeSync = now();
    log_message('debug', 'MODEL: Sync | updateLocalAccounts: Timestamp_START: ' . $beforeSync);

    // Get the list of active accounts from Populi.
    $activeAccounts = $this->PopuliApiExtended->getActiveAccounts();

    /*
    Make sure there is a local account for folks who are have an active account in Populi.
    Active accounts are current students, staff, and anyone with a meal plan.
     */
    $report['countExistingAccounts'] = 0;
    $report['countNewAccounts'] = 0;
    // Merge the two arrays and loop through it.
    foreach ($activeAccounts as $pId) {
      // Create a basic account and set it active.
      $result = $this->Account->createBasicAccount($pId);
      $this->Account->setIsActive($pId);
      // If the result is creation of a new account, count it as added
      if ($result == 1) $report['countExistingAccounts']++;
      if ($result == 2) $report['countNewAccounts']++;
    }


    /*
      Create a local account for all staff members of Heritage College or make sure an existing account is active.
     */
    log_message('debug', 'MODEL: Sync | STAFF Members');
    // Get a list of staff and faculty, add (true) to include adjunct faculty.
    $staff = $this->PopuliApiExtended->getStaffActive();
    // Loop through each staff member.
    foreach ($staff as $row) {
      $personId = intval($row->person_id);

      // Create a basic account if needed and set it active
      $result = $this->Account->createBasicAccount($personId);
      $this->Account->setIsActive($personId);
    }


    /*
    Update the local account with meal plan data for anyone with a Meal Plan.
     */
    log_message('debug', 'MODEL: Sync | MEAL PLAN DATA');
    // This is the current list of folks who are on meal plans.
    $mealPlansCurrent = $this->Account->getAccountsByType('MEAL_PLAN', 'ALL');
    // The list of folks on a meal plan (from the cache - which was updated earlier in the sync)
    $mealPlansNew = $this->MealPlan->getPeopleOnMealPlans();

    /* We need to remove folks who no longer have meal plans first. */ 
    // Any meal plan account that isn't in this list can have their meal plan metadata removed.
    $report['removedMealPlanIds'] = [];
    // A nice counter
    $count = 0;
    // Accounts that are in the current list, that are no longer in the cached list
    foreach ($mealPlansCurrent as $current) {      
      if (!$this->Account->hasMealPlanCache($current->person_id)) {
        $count++;
        log_message('debug', 'MODEL: Sync | ' . $count . '. Remove meal plan data from account ' . $current->person_id);
        // Remove the meal plan data from the account.
        $result = $this->Account->removeMealPlan($personId);
        // Add this Id to the list of removed meal plans.
        if ($result) $report['removedMealPlanIds'][] = $personId;
        // Add a space to the log.
        log_message('debug', 'MODEL: Sync |');
      }
    }

    /* Update meal plan data for meal plan users */
    // Accounts that are in the cached list (i.e. the most recent list from Populi)
    foreach ($mealPlansNew as $row) {
      $count++;
      $pId = $row->person_id;
      log_message('debug', 'MODEL: Sync | ' . $count . '. Update meal plan data for account ' . $pId);
      // See if the meal plan is new or changed and set it as active.
      $this->Account->addMealPlanData($row);
      $this->Account->setIsActive($pId);
      // A little white space
      log_message('debug', 'MODEL: Sync |');
    }




    /*
      Loop over the accounts and set their types appropriately.  This will catch all account types
      MEAL_PLAN, STAFF, PRE_PAID, and CREDIT.
    */
    // Loop over the active accounts and determine which of them should be pre_paid accounts.
    foreach ($activeAccounts as $pId) {
      // Determine and set the type.
      $type = $this->Account->determineType($pId);
      $this->Account->setType($pId, $type);
    }


    /*
      Disable all accounts that are not currently active accounts in Populi.
     */
    $this->db->update('account', array('is_active' => 0));
    // Make all account inactive, then reactivate these accounts.
    $report['countActiveAccounts'] = 0;
    foreach ($activeAccounts as $active) {
      $report['countActiveAccounts']++;
      $this->Account->setIsActive($active);
    }
    log_message('debug', 'MODEL: Sync | Active Populi ACCOUNTS: ' . $report['countActiveAccounts']);


    // Put some important informaton into the logs.  At some point, it would be awesome to create a report
    // out of this data.
    log_message('debug', 'MODEL: Sync | Active Local ACCOUNTS: ' . sizeof($this->Account->getAccounts()));
    log_message('debug', 'MODEL: Sync | MEAL PLAN ACCOUNTS: ' . sizeof($mealPlansNew));
    if ($report['removedMealPlanIds']) log_message('debug', 'MODEL: Sync | ' . implode(',', $report['removedMealPlanIds']) . ' moved from a meal plan to a credit account.');
    if (!$report['removedMealPlanIds']) log_message('debug', 'MODEL: Sync | No one moved from a meal plan to a credit account.');
    log_message('debug', 'MODEL: Sync | STAFF & FACULTY ACCOUNTS: ' . sizeof($this->Account->getAccountsByType('STAFF')));
    log_message('debug', 'MODEL: Sync | PRE PAID ACCOUNTS: ' . sizeof($this->Account->getAccountsByType('PRE_PAID')));
    log_message('debug', 'MODEL: Sync | CREDIT ACCOUNTS: ' . sizeof($this->Account->getAccountsByType('CREDIT')));
    log_message('debug', 'MODEL: Sync | ' . $report['countNewAccounts'] . ' new accounts were created.');

    // Gerenate a new timestamp.
    $afterSync = now();
    log_message('info', 'MODEL: Sync | updateLocalAccounts: Timestamp_END: ' . $afterSync);

    // Calculate the difference.
    $timeToSync = $afterSync - $beforeSync;

    // Log the difference.
    if ($timeToSync == 1) {
      log_message('debug', 'MODEL: Sync | updateLocalAccounts took ' . $timeToSync . ' second.' . PHP_EOL);
    } else {
      log_message('debug', 'MODEL: Sync | updateLocalAccounts took ' . $timeToSync . ' seconds.' . PHP_EOL);
    }



  }











  /**
   * [updatePopuliAccountBalances Update the account balances in Populi based on the account
   * balances in the local database]
   * @return [type] [description]
   */
  public function updatePopuliAccountBalances() {
    log_message('info', 'MODEL: Sync | updatePopuliAccountBalances()');

    // Get the account data from the local database sorting by person_id.
    $accounts = $this->Account->getAccounts('person_id', 'ACS', false);
    log_message('debug', 'MODEL: Sync | Updating Meal Plan Balances in Populi for ' . sizeof($accounts) . ' accounts.');

    // Loop through the accounts, updating the balance in Populi with the balance in
    // our local database.
    foreach ($accounts as $a) {
      $personId = $a->person_id;
      $balance = $a->mp_balance;
      $fullName = $a->first . ' ' . $a->last;

      // We should only update the records if the current balance is different than the Populi balance.
      // Since we keep a cached copy of the Populi balance, we can certainly check.
      $cachedPopuliBalance = $this->MealPlan->getCachedBalance($personId);
      log_message('info', 'MODEL: Sync | ' . $personId . ' stored balance = $' . $balance . ', cached Populi balance = ' . $cachedPopuliBalance);
      if ($balance != $cachedPopuliBalance) {
        log_message('debug', 'MODEL: Sync | Updating meal plan balance in Populi for '. $fullName . ' (' . $personId . ')');
        // Set the balance in Populi.
        $this->MealPlan->setBalance($personId, $balance);
        // Set the cached balance in our local database to this same value.
        $this->MealPlan->setCachedBalance($personId, $balance);
      } else {
        log_message('info', 'MODEL: Sync | The balance for ' . $personId . ' ' . $fullName . ' has not changed, no update required.');
      }
    }
  }

  /**
   * Update the balances of all accounts with Coffeeshop tab balances in Populi.
   */
  public function updatePopuliCoffeeshopTabBalances() {
    log_message('debug', 'MODEL: Sync | updatePopuliCoffeeshopTabBalances()');

    // See if we are in production.
    if (ENVIRONMENT === 'development') {
      log_message('error', 'MODEL: Sync | Oops, we are in development, abort!');
      return false;
    } else {
      log_message('debug', 'MODEL: Sync | Not in development, sync allowed.');
    }

    // Get all account whether active or inactive (some may have flipped their active status, and we want to
    // update the balances for all accounts that have changed regardless of account status).
    $accounts = $this->Account->getAccounts('first', 'ACS', false);

    // Loop over the accounts.  Only updating accounts where the balance is different.
    $count = 0;
    foreach ($accounts as $a) {
      $pId = $a->person_id;
      $bal = $a->coffeeshop_balance;

      // Only update record if the current balance is different than the Populi balance.  Since we cache the coffeeshop balance 
      // into a separate field when we update Populi, we can use that to check - super fast!
      if ($bal == $a->coffeeshop_balance_populi) {
        log_message('info', 'MODEL: Sync | The Coffeeshop Tab balance for ' . $pId . ' has not changed, no update required.');
        continue;
      } else {
        log_message('debug', 'MODEL: Sync | Updating Coffeeshop Tab balance in Populi for ' . $pId);
        // Update Populi.  This will update Populi and update the cached value.
        $try = $this->CoffeeshopTab->setBalance($pId, $a->coffeeshop_balance);
        // Update the counter.
        if ($try) $count++;
      }    
    }
    // Put something in the logs.
    log_message('debug', 'MODEL: Sync | Updated a total of ' . $count . ' Coffee Shop accounts.');
  }




  /**
   * Updates the database with the time that the last sync was performed.
   */
  public function updateLastFullSyncTimestamp() {
		// Get the date and time.
		$dateTime = getSqlDateTime();

    // Update the last_full_sync field in the populi_api table.
		$this->db->update('populi_api', array('last_full_sync' => $dateTime));
  }





  public function getLastFullSyncTimestamp() {
		// Put the query into an array.
		$query = $this->db->get('populi_api');

		// Put the result into an array.
		$result = $query->row();

		// Return just the last full sync.
		return $result->last_full_sync;
  }




  /**
   * A function that kicks off an update for all supporting data such as meal plans
   * and caches this data in the local database.
   * @return [type] [description]
   */
  public function updateSupportingData() {
    log_message('debug', 'MODEL: Sync | updateSupportingData()');
    
    // Cache meal plan data (not for people).
    $this->MealPlan->setCachedMealPlans();
    
    // Cache Academic Terms.
    $this->PopuliApiCache->cacheAcademicTerms();
    
    // Cache the current term id.
    $this->PopuliApiCache->cacheCurrentTermId();

    // Cache person IDs in these roles.
    $this->PopuliApiCache->cacheRoleMembers('Student');
    $this->PopuliApiCache->cacheRoleMembers('Staff');
    $this->PopuliApiCache->cacheRoleMembers('Faculty');

    // Cache Person IDs for people on a Meal Plan.
    $this->PopuliApiCache->cachePeopleOnMealPlans();
  }
















  /**
   * Run through all the active accounts and update the meta data associated with the account such as
   * the person's name.
   */
  public function updateAccountMetaData() {
    // Get the list of active accounts.
    $accounts = $this->Account->getAccounts();
    // Tell thte log what's up.
    log_message('debug', 'MODEL: Sync | Update meta data for all active accounts (' . count($accounts) . ').');
    // Loop over the list.
    foreach ($accounts as $acc) {
      // Cast to an int.
      $pId = (int)$acc->person_id;
      // Update the local accounts with the meta data stored in Populi.
      $this->Account->setMetaData($pId);
    }
  }








}
