<nav id="hcs-secondary-navigation" class="hcs-secondary-navigation hcs-navigation" role="navigation">
  <ul class="unstyled-list">
    <li><a href="<?php base_url(); ?>/reports/accounts">Accounts</a></li>
    <li><a href="<?php base_url(); ?>/reports/sales">Sales</a></li>
    <li><a href="<?php base_url(); ?>/reports/dailycash">Daily Cash Totals</a></li>
  </ul>
</nav>
