<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AcademicTerms extends CI_Model {

  public function __construct() {
    // Call the parent constructor
		parent::__construct();

  }



  /**
   * Retrieves a term's information based on the term id.
   * @param  [int] $termId    A numberical term id.
   * @return [object]         A term object containing data about the specific term.
   */
  public function getTerm($termId) {
    log_message('info', 'MODEL: AcademicTerms | getTerm()');
    // Query the database
    $this->db->where('term_id', $termId);
    $query = $this->db->get('academic_terms');

    // Get just the balance.
    return $query->row();
  }




  /**
   * Retrieves a term's information based on the provided date.
   * @param  [date] $date     A unix timestamp.
   * @return [object]         A term object containing data about the specific term.
   */
  public function getTermByDate($timestamp) {
    log_message('info', 'MODEL: AcademicTerms | getTermByDate()');

    // Remove the time from the date as the date stored in the database does not contain a timestamp.
    $date = date('Y-m-d', $timestamp);

    // Query the database
    $this->db->where('start_date <=', $date);
    $this->db->where('end_date >=', $date);
    $query = $this->db->get('academic_terms');

    // Get just the balance.
    return $query->row();
  }







}
