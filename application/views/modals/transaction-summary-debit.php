<!-- Debit modal -->
<div id="modal-transaction-summary" class="modal">
  <div id="modal-debit" class="modal-dialog modal-debit">
		<section>
			<header class="section-header"><h3>Transaction Summary - Debit</h3></header>
			<div class="section-body">
				<ul>
					<li><span class="modal-label">Cash Received</span><span id="modal-cash-received" class="modal-price price">20</span></li>
				</ul>
				<a href="<?php site_url(); ?>/store" id="modal-ok" class="btn btn-primary">Accepted</a>
			</div>
		</section>
	</div>
</div><!-- END #modal-transaction-summary-->
