<?php $this->load->view('parts/header'); ?>

<?php

if ($item->is_active == 1) {
	$activeStatus = 'checked';
} else {
	$activeStatus = 'unchecked';
}


?>

<!-- Start of the individual view -->
	<div id="view-manage" class="site-content-inner view-manage">
    <?php $this->load->view('nav/manage'); ?>

		<h2>Inventory</h2>

    <h3>Edit Item</h3>

		<form id="edit-item" action="<?php base_url(); ?>/save/item/<?php echo $item->id; ?>" method="post">
			<div class="form-input-area">
  			<label for="item-description">Description</label>
        <input id="item-description" class="required validation-invalid" name="item_description" type="text"
				value="<?php echo $item->description; ?>"></input>

  			<label for="item-price">Price</label>
        <input id="item-price" class="required validation-invalid" name="item_price" type="text"
				value="<?php echo $item->price; ?>"></input>

				<div class="single-checkbox">
					<input id="item-active" class="required validation-invalid" name="item_active"  type="checkbox"
					<?php echo $activeStatus; ?>></input>
					<label for="item-active">Active</label>
				</div>

  			<fieldset class="form-radio">
  				<legend>Tax Code</legend>
  				<ul class="unstyled-list">
						<?php
						// Loop through each of the tax codes.
						foreach ($tax_codes as $t) {
							echo '<li>';
							echo '<input id="item-tax-code' . $t->code . '" name="item_tax_code" ';
							echo 'value="' . $t->code . '" type="radio" ';
							if ($t->label == $item->label) {
								echo 'checked';
							}
							echo '/>';
							echo '<label for="item-tax-code' . $t->code . '">' . $t->label . '</label>';
							echo '</li>';
						}
						?>
  				</ul>
  			</fieldset>

        <label for="item-category">Category</label>
        <select id="item-category" name="item_category">
					<?php
					foreach ($cats as $row) {
						$value = $row->category;
						echo '<option value="' . $value . '" ';
						if ($value == $item->category) {
							// If this the Other category, make it the default.
							echo 'selected';
						}
						echo '>' . $value . '</option>';
					}
					?>
        </select>
      </div>

      <div class="form-footer">
        <input id="save-item-button" class="btn-submit btn-primary" name="commit" value="Save Item" type="submit"></input>
        <a class="btn-submit btn" href="<?php base_url(); ?>/inventory">Cancel</a>
      </div>
		</form><!-- #edit-item FORM complete -->

  </div><!-- #view-manage DIV -->

<!-- End of the individual view -->

<?php $this->load->view('parts/footer'); ?>
