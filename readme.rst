###################
Heritage Cafeteria
###################

Cafeteria Management System for the Heritage College and Seminary cafeteria.  Most importantly, this solution interfaces with our college management database (https://heritage.populiweb.com) to gather a list of current semester students, staff and faculty, and their current meal plan.  It also updates the student's balance in Populi on the Student tab in their profile.

The application is built on the Codeigniter 3.0 PHP application framework using an MVC (model-view-controller) architecture.  https://codeigniter.com/.

How to get going.
- Use the create_database_schema.sql file to create a new MySql database with the appropriate tables.
- Update the config.php file under application/config based on your settings.
- Create a database.php file under application/config and populate it with settings for your database.  Look at the Codeigniter [documentation](https://www.codeigniter.com/user_guide/general/welcome.html) for an example of the database.php file.
